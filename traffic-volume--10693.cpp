// Ricardo Nieto Fuentes
// nifr91@gmail.com 
//
// ============================================================================
//
// Traffic Volume --- 10693 
// 3.000 seconds
//
// In the picture below you can see a street. It has infinite number of cars
// ont it. The distance between any two consecutive cars os 'd', length of 
// each car is 'L' and the velocity of each car is 'v'. The volume of cars
// through a road means the number of cars passing through a road in a specific
// amount of time. When the velocity is constant, 'd' must be minimum of 
// the volume of cars passing through the road to be maximal. In our model 
// when the velocity of all the cars is 'v' then the minimum possible value of
// 'v**2/(2*f)' (the more the car velocity the more distance you need to 
// bring down your velocity to 0. Here f is the deceleration due to break.
//
//
//       **    d         **    d         **
//  ******* <-----> ******* <-----> *******
//   *  *            *  *            *  *  
//  <----->
//     L
//
//
// Keeping this model in mind and given the values of L and f your job is to
// find the value of 'v' for which the volume of traffic through the road is 
// maximal 
//
// Input : 
//
// The input file contains several lines of input. Each line of input 
// contains two integers L (0 < L <= 100) and f (0 < f <= 10000). The unit of 
// L is meter and the unit of f is meter/second**2 . The input is terminated by
// a single line whose value of L and f is zero.
//
// Output: 
//
// For each line of input except the last one produce one line of output. Each
// line contains two floating point number 'v' and 'volume' separated by
// a single space. Here 'v' is the velocity for which traffic flow is maximal 
// and volume is the maximum number of vehicles (of course its a fraction) 
// passing through the read in an hour. These two floating points should 
// have eight digits after the decimal. Error less than 1e-5 will be ignored.
//
// ===========================================================================
//
// Sample Input:
//
// |5 3
// |0 0
//
// Sample Output: 
//
// |5.47722558 1971.80120702
//
// ===========================================================================

// Solución ------------------------------------------------------------------
// 
// La velocidad será sqrt(2 l f)
// y el volumen será (v*2600.0)/2l)
// ---------------------------------------------------------------------------

#include<iostream>
#include<iomanip>
#include<cmath>
using std::cout; 
using std::cin;
using std::endl;
using std::setprecision;
using std::fixed;

int main(int num_args, char ** args){ 

  int l = 0; 
  int f = 0; 

  while(cin >> l >> f) {
    if (!l || !f){break; } 
    double v = sqrt(2*l*f);  
    double vol=(v*3600.0)/(2.0*l);
    cout << fixed << setprecision(9) << v << " " << vol << endl; 
  } 
  return 0; 
} 
