// Ricardo Nieto Fuentes
// nifr91@gmail.com
//
// ============================================================================
//
// Prime Ring Problem --- 524
// 3.000 seconds
//
// A ring is composed of 'n' (even number) circles as shown in diagram. Put 
// natural numbers 1,2,...,n into each circle separately , and the sum of 
// the numbers in two adjacent circles should be prime. 
//
//       1------4 -----3
//       |             |
//       6------5------2
//
// Note: the number of first circle always be 1. 
//
// Input: 
//
// n (0 < n <= 16) 
//
// Output : 
//
// The output format is shown as sample below. Each row represents a series of
// circle numbers in the ring beginning from 1 clockwisely and anticlockwisely. 
// The order of numbers must satisfy the above requirements. 
//
// You are to write a program that completes above process. 
//
// ===========================================================================
//
// Sample input : 
//
// |6
// |8
//
// Sample Output: 
//
// |Case 1: 
// |1 4 3 2 5 6
// |1 6 5 2 3 4
// |
// |Case 2:
// |1 2 3 8 5 6 7 4
// |1 2 5 8 3 4 7 6
//
// ============================================================================

#include<iostream>
using namespace std; 

// Solución -------------------------------------------------------------------
// 
// Ir construyendo la solución y cuando se detecta que una solución parcial 
// no cumple la propiedad abandonar la búsqueda. 
// Para esto tener un vector dinámico que contendrá el anillo y un vector 
// booleano que indica si hemos utilizado el valor o no. 
//
// En cada llamada recursiva se va a agregando un nuevo valor para formar el 
// anillo 
//
// 1           | 1111111111
// 1->2        | 0111111111
// ...
// 1->2->4  x  | 0011111111
// 1->2->5     | 0001111111
// 
// y cuando se detecta que no es una solución valida se abandona la 
// construcción.
//
// ----------------------------------------------------------------------------


// Vector para tener los valores primos
bool isprime[41] = {}; 

// Estructura tipo vector 
struct array{
  int buff[20] ={};
  int size = 0;
  int push(int n);
  int pop();
  int & at(int index);
  void to_cout();
  array(){};
} ;

// Función que genera los anillos detectando si soluciones parciales son 
// invalidas
void solve(array & ring, array & opts){ 

  // Caso base el anillo tiene el tamaño especificado
  if (ring.size == opts.size){
    if (isprime[ring.at(0) + ring.at(-1)]){ ring.to_cout();} 
    return ; 
  } 

  // Probar todas las posibles combinaciones para la posición
  for(int i=1; i < opts.size ; i+=1){
    // Solo números que no han sido utilizados 
    if(!opts.at(i)){ continue; } 
    // Marcamos y asignamos 
    opts.at(i) = 0;
    ring.push(i+1);
    // Verificar si la solución parcial es válida
    auto ispartialsol = isprime[ring.at(-1) + ring.at(-2)];
    // Si es una solución válida se continua la construcción
    if(ispartialsol){solve(ring,opts);}
    // Se desmarca y elimina el valor probado
    ring.pop();
    opts.at(i) = 1;
  } 
} 

// ===========================================================================
//
int main(int num_args, char ** args){ 

  // Criba para marcar los números primos 
  isprime[0] = false; 
  isprime[1] = false;
  for(int i=2; i < 41; i+=1){isprime[i] = true;} 
  for(int i=2; i < 41; i+=1){ 
    if(!isprime[i]){ continue; } 
    for(int j=i+1; j < 41; j+=1){ 
      isprime[j] = ((j%i)==0) ? false : isprime[j];
    } 
  } 

  // Leer el número 
  int n = 0; 
  int case_num = 1;
  while (cin >> n ){
    // Generar los números 
    array opts;
    for(int i=0; i < n; i+=1){ opts.push(1); }
    // Inicializar el anillo 
    array ring;
    ring.push(1);

    // Calcular y desplegar la información
    if (case_num != 1){ cout << endl; } 
    cout << "Case " << case_num << ":" <<endl;
    solve(ring,opts);
    case_num += 1;
    } 
  return 0;
}

// Función para desplegar la información 
void array::to_cout(){ 
  for(int i=0; i < this->size-1; i+=1){ cout << this->buff[i] << " "; } 
  cout << this->buff[this->size-1] << endl;
} 

// Acceder a los valores 
int & array::at(int index){ 
  if (index < 0){index = this->size + index;} 
  index = (index % this->size); 
  return this->buff[index]; 
} 
 
// Agregar al final del arreglo 
int array::push(int val){
  this->buff[this->size] = val; 
  this->size += 1;
  return val; 
} 

// Eliminar el último elemento del arreglo 
int array::pop(void){ 
  this->size -= 1;
  return this->buff[this->size];
} 
