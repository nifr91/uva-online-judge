// Ricardo Nieto Fuentes
// nifr91@gmail.com 
//
// ============================================================================
//
// How Many Knights --- 696 
//
// The knight is a piece used in chess, a game played on a board with squares
// arranged un rows and columns. A knight attacks pieces that are either(a) two
// rows and one column away from its position, or (b) one row and two columns
// away from its position. The following diagram illustrates this. The square
// marked N represents the position of the knight, and the squares marked 'x'
// indicate the squares that are under attack. 
//
// [ ][x][ ][x][ ]
// [x][ ][ ][ ][x]      
// [ ][ ][N][ ][ ]      
// [x][ ][ ][ ][x]      
// [ ][x][ ][x][ ]      
//
// Int this problem you are to determine the largest number of knights that 
// can be placed on a board with 'M' rows and 'N' columns so that no knights is
// attacking any other. 'M' and 'N' will each be no large than 500. 
//
// Input 
//
// The input consist of pairs of integers giving values for M and N, followed 
// by a pair of zeroes. 
//
// Output 
//
// For each input pair display the number fo rows and columns in the board, and 
// the number of knights that can be appropriately placed. 
//
// ============================================================================
//
// Sample Input 
//
// |2 3
// |5 5
// |4 7
// |0 0
//
// Sample Output 
//
// |4 knights may be placed on a 2 row 3 column board.
// |13 knights may be placed on a 5 row 5 column board.
// |14 knights may be placed on a 4 row 7 column board.
//
//=============================================================================

#include<iostream> 
using namespace std; 

// Existen tres casos
//
// * El número de filas o columnas es 1

//   [k][k][k][k]...[k] 
//
//   en este caso notemos que el valor es el número de casillas pues se 
//   pueden colocar un caballo en cada casilla sin que se ataquen. 
//
// * El número de filas o columnas es 2
//
//   [k][k][x][x][k][k][x][x][k][k]
//   [k][k][x][x][k][k][x][x][k][k]
//
//   en este caso la forma mas eficiente de colocar los caballos es en 
//   grupos de 4 por ello el número de caballos que se pueden colocar será 
//   
//   (c/4 * 4)   +  caballos que pueden ser agregados en el residuo
//    ---
//     v
//   número de grupos que podemos colocar en dada por las columnas 
//
//   Si el  residuo es 0 entonces  podemos colocar exactamente el número 
//   de grupos * 4 caballos. 
// 
//      1  2  3  4 
//   1 [k][k][x][x]
//   2 [k][k][x][x]
//
//      1  2  3  4  5  6  7  8
//   1 [k][k][x][x][k][k][x][x]
//   2 [k][k][x][x][k][k][x][x]
//             
//   Si el residuo es 1 entonces podemos agregar 1 columna de caballos 
//
//      1  2  3  4  5 
//   1 [k][k][x][x][k]
//   2 [k][k][x][x][k]
//                 ---
//                  ^
//
//   Si el residuo es 2 o 3 entonces podemos agregar la segunda columna
//
//      1  2  3  4  5  6  7 
//   1 [k][k][x][x][k][k][x]
//   2 [k][k][x][x][k][k][x]
//                    ------
//                     ^
//  * Si r > 2 entonces es posible colocar un caballo en todas las posiciones 
//    del mismo color del color que tenga más casillas
//
//     1  2  3
//  1 [k][x][k] 
//  2 [x][k][x] 
//  3 [k][x][k] 
//
//     1  2  3  4
//  1 [k][x][k][x] 
//  2 [x][k][x][k] 
//  3 [k][x][k][x] 
//
//     1  2  3  4
//  1 [k][x][k][x] 
//  2 [x][k][x][k] 
//  3 [k][x][k][x] 
//  4 [x][k][x][k] 

//

int f(int x){ 
  if(x == 1){return 2;} 
  if(x == 2 || x==3){return 4;} 
  return 0; 
} 

int main (int argc, char ** argv){ 
  int rows,cols; 
  while (cin >> rows >> cols){ 
    if ((rows == 0)&&(cols == 0)){break;} 
    
    int r = (rows < cols) ? rows : cols;
    int c = (rows < cols) ? cols : rows; 
    int n = r*c;

    if((r == 1)){ n = n; } 
    else if((r == 2)){ n = ((c/4)*4) + f(c%4);} 
    else{ n = ((n % 2) == 0) ? n/2 : (n/2)+1;} 

    cout 
      << n 
      << " knights may be placed on a " 
      << rows 
      << " row " 
      << cols << " column board." << endl;
  } 
  return 0;
} 
