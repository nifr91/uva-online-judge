// Ricardo Nieto Fuentes
// nifr91@gmail.com
//
// ============================================================================
//
// Slogan Learning of Princes --- 12592
// 1.000 seconds
//
// Prince Macaw is flying here and there and searching for food. Her Princess is
// waiting for him at home. But he stopped when he was flying near Shahbag. He
// saw millions of people gathered there for only one demand- death sentence
// of war crimes. They were giving slogans. If your are not familiar with 
// slogans let me tell you, slogan has two parts. One part of the slogan is 
// yelled by the leader and the rest part is chorused from the mass people. 
// For example, when the leader says -- "tomar amar thikana" then other 
// say "padma meghna jomuna". Some of the slogans are listed below: 
//
// |ko te kader molla -> tui rajakar tui rajakar
// |tumo ke ami ke    -> garo chakma bangali
//
// Prince also joined with this mass people. When it was too late, Princess 
// got worried about Prince. So she also went out. After some while, she found
// Prince on a tree at Shahbag telling slogans in strong voice. Instead of 
// getting mad, Princess asked Prince what are the slogans. So Prince told her 
// a few slogans, first line and corresponding second line. So Princess got 
// prepared. Given the list of slogans and also the first lines Princess going
// to hear, print out the second line of the slogans.
//
// Input: 
//
// First line contains number of slogans, 'N' (0 < N <= 20) Prince is going to
// teach Princess. Hence follows 'N' pairs of lines. First line of the pair
// contains first line of a slogan and the second line of the pair is
// corresponding second line of the slogan. Then another positive integer 'Q'
// will be given (Q <= 100). Hence follows Q lines. Each line contains the
// first line of a slogan. You may assume that the slogans will contain lower
// case English alphabet and/or space. Both the lines of slogan will contain at
// leas one alphabet. No two first lines in the princess slogan list will be
// the same. Each slogan will be at most 100 characters long. All of the query
// first line will be in Princess List. 
//
// Output: 
//
// For each of the first line of the slogan, you need to print out 
// corresponding second line. 
//
// ============================================================================
//
// Sample Input : 
//
// |3
// |ko te kader molla
// |tui rajakar tio rajakar
// |tumi ke ami ke
// |garo chakma bangali
// |jalo re jalo
// |agun jalo
// |2
// |jalo re jalo
// |ko te kader molla
// 
// Sample Output: 
//
// |agun jalo
// |tui rajakar tui rajakar
//
// ============================================================================


// Solución -------------------------------------------------------------------
//
// Usar un diccionario para almacenar las primeras lineas y utilizarlo para 
// dar respuesta 
//
// ----------------------------------------------------------------------------

#include<iostream>

// Estructura para el mapa 
struct node{ 
  int id; std::string k; std::string v; node * l; node * r; node * p;
  node(std::string _k,std::string _v,node * _l = nullptr, node *_r = nullptr, node * _p = nullptr):
    k(_k),v(_v),l(_l),r(_r),p(_p){ id = rand(); }
};

// Estructura para regresar dos nodos en el mapa
struct node_pair{ 
  node * l; node * r; node_pair(node * _l, node * _r):l(_l),r(_r){}
};


// Mapa std::string => std::string
struct nmap{ 
  node * root = nullptr; 

  node_pair split(node * root, std::string & key, bool leq = true){
    node * tl = nullptr; 
    node * tr = nullptr; 
    if(!root){ return node_pair(tl,tr);} 
    bool comp = (leq) ? (key <= root->k) : (key < root->k); 
    if (comp){
      tr = root; 
      auto tpair = split(tr->l,key,leq); 
      tr->l = tpair.r;
      tl = tpair.l;
    }
    else{ 
      tl = root;
      auto tpair = split(tl->r,key,leq); 
      tl->r = tpair.l;
      tr = tpair.r;
    } 
    return node_pair(tl,tr);
  } 

  node * merge(node * tl, node * tr){ 
    if(!tl || !tr){ return ((tl) ? tl : tr);}
    if(tl->id > tr->id){ 
      tl->r = merge(tl->r,tr);
      return tl; 
    } 
    else { 
      tr->l = merge(tl,tr->l); 
      return tr; 
    } 
  }

  node * find(std::string & key,node * root){ 
    if(!root || (root->k == key)){return root;} 
    if(key < root->k){return find(key,root->l); } 
    return find(key,root->r);
  } 

  void insert(std::string & key, std::string & val){ 
    node * n = new node(key,val);
    auto npair = split(this->root,key); 
    auto l = npair.l;
    auto m = npair.r;
    npair = split(m,key,false);
    m = npair.l; 
    auto r = npair.r;
    this->root = merge(merge(l,n),r);
  } 
};

int main(int num_args, char ** args){ 

  // Obtener el número de slogans
  int num_slogans = 0; 
  std::cin >> num_slogans; 
  std::cin.ignore(); 

  // Obtener los slogans 
  nmap map; 
  for(int i=0; i < num_slogans; i+=1){ 
    std::string first = ""; 
    std::getline(std::cin, first); 
    std::string second = ""; 
    std::getline(std::cin, second); 
    map.insert(first,second); 
  }

  // Obtener el número de preguntas 
  int num_queries = 0; 
  std::cin >> num_queries; 
  std::cin.ignore(); 

  // Procesar cada pregunta 
  for(int i=0; i < num_queries; i+=1){ 
    std::string slogan = ""; 
    std::getline(std::cin, slogan); 

    auto e = map.find(slogan,map.root) ;
    if(e){std::cout << (e->v) << std::endl;}
    else{std::cout << "error" << std::endl;} 
  } 

  return 0; 
} 
