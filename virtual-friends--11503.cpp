// Ricardo Nieto Fuentes
// nifr91@gmail.com
//
// ============================================================================
//
// uva.onlinejudge.org 
//
// Virtual Friends --- 11503
//
// These days, you can do all sorts of things online. For example you can use 
// various websites to make virtual friends. For some people growing their 
// social network (their friends, their friends' friends, their friends' 
// friends' friends, and so on), has become an addictive hobby. Just as some 
// people collect stamps, other people collect virtual friends. 
//
// Your task is to observe the interactions on such a website and keep track of 
// the size of each person's network.
//
// Assume that every friendship is mutual. If Fred is Barney's friend, then 
// Barney is also Fred's friend. 
//
//
// Input: 
//
// The first line of input contains one integer specifying the number of test 
// cases to follow. Each test case begins with a line containing an integer 'F'
// , the number of friendships formed, which is no more than 100_000. Each of 
// the following 'F' lines contains the names of two people who have just 
// become friends, separated by a space. A name is a string of 1 to 20 letters
// (uppercase or lowercase). 
//
// Output: 
//
// Whenever a friendship os formed, print a line containing one integer, the 
// number of people in the social network of the two people who have just 
// become friends. 
//
// ============================================================================
//
// Sample Input ---------------------------------------------------------------
//
// |1
// |3
// |Fred Barney
// |Barney Betty
// |Betty Wilma 
//
// Sample Output ---------------------------------------------------------------
//
// |2
// |3
// |4
//

#include <iostream>
#include <map> 
using namespace std; 

// Clase union find disjoint set 
//
template <class T> class ufds{ 
 
  // Clase privada de nodo, contiene un valor, un apuntador al padre
  // el rango del nodo y el número de nodos hijos.
  class node{ 
    public: 
    T val; 
    node * parent; 
    int cnt; 
    int rnk; 
    node(T val){
      this->val = val;
      this->parent = this; 
      this->cnt = 1;
      this->rnk = 0; 
    } 
  };

  // Utilizamos un mapa para almacenar los nodos 
  map<T,node*> kmap;

public:

  // Constructor 
  ufds(){ this->kmap = map<T,node*>(); } 
  // Destructor, liberamos la memoria de los nodos
  ~ufds(){ for(auto & pair : this->kmap){ delete pair.second; } } 

  // Para insertar un nuevo nodo, se genera el nodo y se agrega al mapa
  void insert(T val){
    node * n = new node(val); 
    this->kmap.insert(pair<T,node*>(val,n));
  } 

  // Regresa true si esta en el conjunto y false si no esta, empleamos 
  // la función count del mapa. 
  bool inset(T val){ return(this->kmap.count(val) != 0);  } 

  // Regresa el nodo padre del conjunto (el representante), aplica la 
  // compresión de camino, donde se reduce la altura del árbol a 1. 
  node * find(T val){ 
    auto node = this->kmap.find(val)->second;
    if(node->parent != node){ node->parent = find(node->parent->val);} 
    return (node->parent); 
  }

  // Combinar dos conjuntos, regresa el número de nodos en el nuevo conjunto
  // se emplea el 'rank' para disminuir el coste computacional.  
  int merge(T v1, T v2){ 
    // Obtener el representante 
    auto n1 = this->find(v1);
    auto n2 = this->find(v2); 

    // los dos valores pertenecen al mismo conjunto si tienen el mismo 
    // representante.
    if(n1 == n2){ return n1->cnt; } 

    // Si los dos conjuntos tienen el mismo rango se combinan y se aumenta el 
    // rango, si no, se elije el que tenga menor rango como padre.
    node * p = NULL;  
    node * c = NULL; 
    if (n1->rnk == n2->rnk){ 
      p = n1; 
      c = n2; 
      n1->rnk += 1; 
    }else if ( n1->rnk > n2->rnk){ 
      p = n1; 
      c = n2; 
    }else{ 
      p = n2; 
      c = n1; 
    } 

    // El hijo (menor rango) tiene como padre al de mayor rango (para 
    // no aumentar la altura). También se actualiza el número de nodos 
    // en el conjunto.
    c->parent = p; 
    p->cnt += c->cnt; 

    return p->cnt; 
  } 
};


// Driver =====================================================================

int main(int argc, char ** argv){ 
    
  int test_cases; 
  cin >> test_cases; 

  for(int i=0; i < test_cases; i+=1){ 
    auto set = ufds<string>(); 

    int friendships; 
    cin >> friendships; 
    
    for(int k=0; k < friendships; k+=1){ 
      string f1,f2; 
      cin >> f1 >> f2; 
      // Revisar si ya están en el conjunto; 
      if (!set.inset(f1)){ set.insert(f1); } 
      if (!set.inset(f2)){ set.insert(f2); }
      
      cout << set.merge(f1,f2) << endl; 
    } 
  } 

  return (0); 
} 
