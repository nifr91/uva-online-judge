// Ricardo Nieto Fuentes
// nifr91@gmail.com
//
// ===========================================================================
//
// Citizen attention offices
// 3.000 seconds
//
// The Mayor of a city wants to improve the attentions of the local government 
// to the citizens. For that, five offices to attend to consults will be open 
// in the city. The Mayor thinks that the attentions would be better if the 
// offices are close to where the people live. To decide where to open the 
// offices the city has been divided in 25 areas, in a square: 
//
// [00][01][02][03][04]
// [05][06][07][08][09]
// [10][11][12][13][14]
// [15][16][17][18][19]
// [20][21][22][23][24]
//
// and the quantity of people living in each square is counted, this assigning 
// to each area the number (an integer number) of thousands of people living 
// there. For example: 
//
// [0][0][2][3][1]
// [5][2][2][2][0]
// [1][1][2][3][0]
// [0][0][2][1][5]
// [2][1][2][0][4]
//
// the number of people in each area is less or equal to 10 million. For each
// area, the minimum distance to the five offices is obtained as follows: 
// the distance from one area to an office is obtained as the number of
// movements to go from the area to the area where the office will be installed
// (the movements are in horizontal and vertical; for example, from (1,1) to 
// (2,3) three movements are necessary). 
// 
// And we want to minimize the sum of the minimum distances from all the areas,
// but having into account the quantity of people living in each area. Thus,
// for each area the distance to an office is multiplied by the value
// associated to the area. 
//
// Input:
//
// The input begins with a line where the number of test cases 't' is indicated.
// The data for each test case apear in successive lines. In each test case
// the first line contains the number of areas in the city with non null 
// population 'n' and in each one of the 'n' following lines three numbers 
// appear, the first and the second indicate the row and the column of the area
// (the are numbered from 0 to 4 and the third the number of thousands of 
// people living in the area. 
//
// Output: 
//
// The output consists of a line for each problem, with five numbers with a
// space between each two numbers, and no space after the las number. The
// numbers in the solutions are written in increasing order. If there is more
// than one optimum solution, you should output the solution with lowest first
// value. If the first value coincides, you must output the solution with
// lowest second value, and so on. 
//
// ============================================================================
//
// Sample Input
//
// |4
// |1
// |2 2 1
// |4
// |0 0 1
// |4 4 1
// |0 4 1
// |4 0 1
// |5
// |0 0 1
// |1 1 1
// |2 2 1
// |3 3 1
// |4 4 1
// |7
// |4 2 2
// |3 3 1
// |2 4 3
// |2 1 1
// |1 3 4
// |1 2 2
// |1 0 1
//
// Sample Output
//
// |0 1 2 3 12
// |0 1 4 20 24
// |0 6 12 18 24
// |5 7 8 14 22
//
// =============================================================================

#include<iostream>
using namespace std; 

// ----------------------------------------------------------------------------
// Solución Propuesta
//
// El problema pide encontrar la forma de colocar las 5 oficinas tal que 
// se minimice la suma de las distancias de las casillas. La distancia de 
// cada casilla es la distancia mínima (movimientos) de la casilla a 
// la oficina más cercana. 
//
// Como las oficinas son iguales podemos enumerar las combinaciones 
//
// |C(25,5) = 53_130 
//
// (5 oficinas en 25 posibles posiciones)  y para cada posible
// combinación calcular la distancia mínima a una oficina y la suma de todas
// esas distancias. 
//
// La solución es la combinación (a,b,c,d,e) tal que la suma es mínima, como 
// se pide que si existen más soluciones se de mas pequeña lexicográficamente 
// se generan en ese orden las posibles combinaciones. 
//
//-----------------------------------------------------------------------------


// Función para calcular la distancia 
int distance(int office, int city, int k){ 
  int ro = office / 5; 
  int co = office % 5; 
  int r  = city / 5; 
  int c  = city % 5; 
  return (abs(r - ro) + abs(c - co))*k; 
} 

// Driver ---------------------------------------------------------------------

int main(int argc, char ** argv){ 

  // Obtener el número de casos 
  int test_cases; 
  cin >> test_cases; 

  // Matriz que representa la ciudad.
  int city[25][2]; 

  // Para cada caso 
  for(int t=0; t < test_cases; t+=1){ 

    // Obtener el número de casillas que tienen población
    int non_null_pop; 
    cin >> non_null_pop;

    // Obtenemos el valor de cada casilla;
    for(int n=0; n < non_null_pop; n+=1){ 
      int r,c,p; 
      cin >> r >> c >> p; 
      city[n][0] = (r*5)+c; 
      city[n][1] = p; 
    } 

    // Solución 
    int sol[5] = {0,1,2,3,4};
    int min_sum = 1 << 30; 

    // Generar cada combinación en orden lexicográfico
    for(int a=0; a < 25; a+=1){ 
      for(int b=a+1; b < 25; b+=1){ 
        for(int c=b+1; c < 25; c+=1){ 
          for(int d=c+1; d < 25; d+=1){ 
            for(int e=d+1; e < 25; e+=1){ 

              // Posiciones de las oficinas
              int office[5] = {a,b,c,d,e}; 

              // Obtener la suma mínima para la configuración de oficinas
              int sum  = 0; 
              for(int n=0; n < non_null_pop; n+=1){ 
                int dist = 1 << 30; 
                for(int i=0; i < 5; i+=1){ 
                  dist = min(dist,distance(city[n][0],office[i],city[n][1]));
                } 
                sum += dist; 
              } 
              // Almacenar la nueva mejor configuración
              if(sum < min_sum){ 
                for(int i=0; i < 5; i+=1){ sol[i] = office[i]; } 
                min_sum = sum; 
              } 
            } 
          } 
        } 
      } 
    } 

    // Mostrar el resultado 
    for(int i=0; i < 5; i+=1){ cout << sol[i] << ((i < 4) ? " " : ""); } 
    cout << endl; 
  } 
  return 0; 
} 
