// Ricardo Nieto Fuentes
// nifr91@gmail.com 
//
// ============================================================================
//
// Mobile Casanova --- 12085
//
// Mobile Phone industry is the fastest growing industry in Wonderland. The 
// three prominent mobile service providers of Wonderland are Coktel, Anglolink
// and Grinding Phone. All Three operators provide very cheap rate packages 
// after 12:00 AM. The name of their packages are "The Thief Talk" ("Chorer
// Alap" in Bangla), "Insomnia" and "The Vampire Chat" respectively. These 
// cheap packages have inspired many people to disturb others at night. 
//
// One such gone to ashtray guy is Arif whose activities begin after 12:00 AM. 
// He dials a number randomly and writes down it in his notebook if he likes it
// . Later he dials those numbers written in his notebook frequently. As names
// are not important to hum so his note book contains only numbers, and no 
// names. But in a few days his notebook becomes filled with many numbers. So 
// now he wants to reorganize the numbers in a new notebook. The reorganizing 
// process is described below: 
//
// Arifs notebook contains many numbers so there may exist some consecutive 
// numbers. Therefore he wants to save space by writing the consecutive numbers
// as a range. For example if there are three numbers 
// 01711322396
// 01711322397
// 01711322398
// then he will write them as 
// 01711322397-8. 
// It means that the smallest number in the sequence is the first part of the
// range followed by a hyphen and then only the rightmost digits of the largest
// number that are different than the first number. A phone number can have 
// any numerical value within 100 and 2_000_000_000 (inclusive) and is 
// always preceded by only a single zero. So 01123 is a valid phone number but
// 00123, 089,1234,02000000001 are invalid phone numbers. 
//
// You will be given up to 100_000 sorted phone (As you cant sort numbers) 
// numbers and your job is to write a program that does the job for Arif. 
//
// Input
// 
// The input file contains 110 sets of inputs. But only about 10% of them are 
// large inputs. The description of each set is given below: 
//
// Each set starts with an integer 'N' ( 0 <= N <= 100_000) which denotes how 
// many phone numbers are there in this set. Each of the next 'N' lines 
// contains a valid phone number. The phone numbers will be sorted in ascending
// order according to their numerical values. You can assume that all input 
// phone numbers will be valid and distinct. You can also assume that all phone
// numbers in a single set will have equal length. 
//
// Input is terminated by a set where the value of N = 0. This set need not to 
// be processed. 
//
//
// Output 
//
// For each set of inputs produce to or more lines of outputs. The description
// of output for each set is given below: 
//
// First line contains the serial of output. Next lines contains the phone 
// numbers printed according to the rule mentioned in the problem statement. 
// The numbers which are not part of a consecutive sequence are printed exactly
// as the input and the consecutive number sequences are printed as range. 
// Print a blank line after the output for each set of input. Look at the 
// output for sample input for details. Remember that it needs at least two 
// consecutive numbers to forma consecutive sequence. 
//
// ============================================================================
//
// Sample Input 
//
// |3
// |01711322396
// |01711322397
// |01711322398
// |7
// |01187239192
// |01711322396
// |01711322397
// |01711322398
// |01711322399
// |01711322400
// |01711389821
// |0
//
// Sample Output 
//
// |Case 1:
// |01711322396-8
// |
// |Case 2:
// |01187239192
// |01711322396-400
// |01711389821
//
// ============================================================================

#include<iostream> 
using namespace std; 


// Función para imprimir los números consecutivos en forma de rango 
void print_number(int first_num, int last_num){ 
  // Convertir a string ambos números
  string first = to_string(first_num); 
  string last  = to_string(last_num);

  // Puntero que indica la última posición en la que son iguales (-1 son 
  // completamente distintas). 
  int ptr = -1;
  for(int i=0; i < (int)first.size(); i+=1){ 
    if (first[i] == last[i]){ ptr = i;}
    else{ break;}
  }

  // Imprimir el número  0xxxxxx-yyy , donde xxxx es completo e yyy es a 
  // partir de la posición en la que son diferentes
  cout << "0" ; 
  for(int i=0; i < (int)first.size(); i+=1){cout << first[i];} 
  cout << "-" ; 
  for(int i=ptr+1; i < (int)last.size(); i+=1){ cout << last[i]; } 
  cout << endl; 
} 

int main(int argc, char ** argv){ 
  int n; 
  int testcase = 1;

  while(cin >> n){ 
    if(!n){break;}
 

    // Leer el primer número 
    int first_num; 
    cin >> first_num; 
    int last_num = first_num;

    cout << "Case " << testcase <<  ":" << endl;

    // Para cada número 
    for (int i=1; i < n; i+=1){ 
      // Leer el número 
      int num;
      cin >> num;
      
      // Si están en secuencia solo almacenamos el último valor de la secuencia
      if ((num - last_num) == 1){last_num = num;}
      // Si no están en secuencia, primero se imprime el último número o 
      // la secuencia si existe y se actualizan los números;
      else{
        if(last_num == first_num){
          cout << "0" << last_num << endl;
        }else { print_number(first_num,last_num); } 
        last_num = first_num = num;
      } 
    }
    // Imprimir el último número 
    if(last_num != first_num){ print_number(first_num,last_num);} 
    else{ cout << "0" << last_num << endl; } 

    cout << endl;
    testcase += 1; 
  } 
  return (0); 
} 
