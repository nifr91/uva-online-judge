// Ricardo Nieto Fuentes
// nifr91@gmail.com
//
// ============================================================================
//
// The dominoes solitaire --- 10503
// 3.000 seconds
//
// A man used to play dominoes with some friends in a small town. Some families
// have left the town and at present the only residents in the town are the man 
// and his wife. This man would like to continue playing dominoes, but his wife
// does not like playing. He has invented a game to play alone. Two pieces are
// picked out and are put at the two extremes of a row with a number (n) of
// spaces. After that, other pieces (m) are picked out to fill the spaces. The 
// number of pieces is greater than or equal to the number of spaces (m >= n), 
// and the number of pieces is less than or equal to 14 (m <= 14). The spaces
// are filled by putting one piece in each space, according to the rules of
// dominoes: the number of adjacent dots on two different dominoes must coincide.
// Pieces with repeated values are placed in the same way as the other pieces
// and not in right angles. 
//
// The problem consists in the design of a program which, given a number of 
// spaces (n) , a number of pieces (m), the two initial pieces (i1,i2) and 
// (d1,d2) and the m pieces (p1,q1),(p2,q2),...(pm,qn), decides if it is 
// possible to fill the n spaces between the two initial pieces using the 
// m pieces and with the rules of dominoes. For example, with n=3, m=4, 
// initial pieces (0,1),(1,2),(2,2),(2,3),(3,4),(5,6),(2,2) and (3,2) the 
// answer is YES, because there is a solution (0,1),(1,2),(2,2),(2,3),(3,4).
// With n=2, m=4, pieces in the extremes (0,1) and (3,4) and (1,4),(4,4),(3,2)
// and (5,6), the answer is NO.
//
// Input : 
//
// The problem will consists of a series of problems, with each problem
// described in a series of lines: in the first line the number of spaces (n)
// is indicated, in the second line the number of pieces (m) used to fill the
// spaces, in the next line the piece to be placed on the left, with the two
// values in the piece separated by a space, and in the same way that the
// number appears in a row, in the following lines the piece to be placed on the
// right, with the two values in the piece separated by a space and in the same
// way that the numbers appear in the row, and the other m appear in
// consecutive lines, one in each line, with the two values separated by a
// space. Different problems appear in the input successively without
// separation, and the input finishes when '0' appears as the number of spaces.
//
// Output: 
//
// For each problem in the input a line is written, with YES if the problem has
// solution and "NO" if it has no solution.
//
// ===========================================================================
//
// Sample Input:
//
// |3
// |4
// |0 1
// |3 4
// |2 1
// |5 6
// |2 2
// |3 2
// |2
// |4
// |0 1
// |3 4
// |1 4
// |4 4
// |3 2
// |5 6
// |0
//
// Sample Output: 
//
// |YES
// |NO
//
// ============================================================================

#include<iostream>
using namespace std; 


// Solution --------------------------------------------------------------------
//
// -----------------------------------------------------------------------------


// Representación de una pieza
struct piece{ 
  int l=0; 
  int r=0; 
  piece(){}
  piece(int _l,int _r):l(_l),r(_r){}
};

// Arreglo dinámico 
template<typename T> struct nary{
  T buff[100];
  int size = 0; 
  T & at(int index){ 
    if(index < 0){ index = this->size + index; } 
    return this->buff[index]; 
  } 

  T push(T val){ 
    this->at(this->size) = val; 
    this->size += 1; 
    return val;
  } 

  T pop(){ 
    this->size -= 1;
    return this->at(this->size);
  }
};


// Para verificar si tenemos una solución válida se tiene que verificar 
// que las últimas dos piezas coincidan [x.y][y.z]
bool ispartialsol(nary<piece> & sol,nary<piece> & pieces,nary<bool> & used,int spaces){ 
  // La solución vacía es valida 
  if(sol.size == 0){ return true; } 
  // Si solo tiene una se verifica con la primer pieza
  if(sol.size == 1){ return (sol.at(0).l == pieces.at(0).r); } 
  // [.]...[.x][x.y][y.x] ? 
  return (sol.at(-1).l == sol.at(-2).r);
} 


bool solve(nary<piece> & sol , nary<piece> & pary, nary<bool> & uary,int spaces){ 

  // Si no es una solución podamos
  if(!ispartialsol(sol,pary,uary,spaces)){return false;} 

  // caso base 
  if (sol.size == spaces){
    return (sol.at(-1).r == pary.at(-1).l); 
  } 

  // Para cada posible pieza de domino se intenta colocar en la solución 
  for(int i=1; i < pary.size-1; i+=1){ 
    // Si la pieza ya fue utilizada pasamos a la siguiente 
    if(uary.at(i)){continue;} 
    piece p = pary.at(i); 

    // Intentamos colocar la pieza en la forma l.r
    uary.at(i) = true; 
    sol.push(piece(p.l,p.r));
    if(solve(sol,pary,uary,spaces)){return true;}  
    sol.pop(); 

    // Intentamos colocar la pieza en la forma r.l
    sol.push(piece(p.r,p.l));
    if(solve(sol,pary,uary,spaces)){return true;} 
    sol.pop(); 
    uary.at(i) = false; 
  } 

  return false; 
} 

// Driver ====================================================================

int main(int num_args, char ** args){ 
  
  int spaces = 0; 
  while(cin >> spaces){ 
    if(!spaces){ break;} 

    // Leer los datos 
    int num_pieces = 0; 
    cin >> num_pieces; 
    piece left_piece; 
    cin >> left_piece.l >> left_piece.r; 
    piece right_piece; 
    cin >> right_piece.l >> right_piece.r; 
    nary<piece> pieces; 
    pieces.push(left_piece);
    for(int i=0; i < num_pieces; i+=1){
      int r = 0; 
      int l = 0; 
      cin >> l >> r;
      pieces.push(piece(l,r)); 
    } 
    pieces.push(right_piece);

    // Inicializar marcas
    nary<bool> used; 
    for(int i=0; i < pieces.size; i+=1){used.push(false);}
    used.at(0) = true;
    used.at(-1) = true;

    // Resolver 
    nary<piece>  sol;  
    bool res = solve(sol,pieces,used,spaces); 

    // Desplegar resultado 
    cout << ((res) ? "YES" : "NO") << endl;
  } 

  return 0; 
} 


