// Ricardo Nieto Fuentes
// nifr91@gmail.com 
//
// ===========================================================================
//
// N+NOD(N) --- 11876
// 2.000 seconds 
//
// Consider an integer sequence N where, 
// N_0 = 1
// N_i = N_u + NOD(N_i-1) for i > 0
//
// Here NOD(x) = number of divisors of x
// So the first few terms of this sequence are 1 2 4 7 9 12 18 ... 
//
// Given two integers A and B, find out the number of integers in the above
// sequence that lies within the range [A,B].
//
// Input: 
//
// The first line of input is an integer T(1 < 100_000) that indicates the 
// number of test cases. Each case contains two integers, A followed by 
// B (1 <= A <= B <= 1_000_000).
//
// Output: 
//
// For each case, output the case number first followed by the required result
//
// ============================================================================
//
// Sample Input: 
//
// |3
// |1 18
// |1 100
// |3000 4000
//
// Sample Output:
//
// |Case 1: 7
// |Case 2: 20
// |Case 3: 87
//
// ============================================================================


// Solución -------------------------------------------------------------------
// 
// Generar la secuencia de números y emplear una búsqueda binaria para cada
// caso para encontrar el intervalo de A = a[i] y B = a[j] en el arreglo, el
// número de elementos será j-i.
//
// Es necesario realizar de forma eficiente el cálculo de los divisores 
// para ello se realiza una descomposición por primos
//
// ----------------------------------------------------------------------------

#include<iostream>
#include<cmath>

// Vector con funciones tipo pila
template <typename T> 
struct nvect{ 
  T buffer[1000001] = {};
  int size = 0; 
  // Agregar elemento al final 
  T push(T val){ 
    this->buffer[this->size] = val; 
    this->size +=1; 
    return val; 
  } 
  // Eliminar último elemento añadido 
  T pop( ){ 
    this->size -= 1; 
    return this->buffer[this->size]; 
  } 
  // Acceder a un elemento 
  T & operator[](int index){
    if(index < 0){index = this->size + index;} 
    return this->buffer[index];
  } 
}; 


// Calcular el número de divisores
int divcount(int n,nvect<int> & primes){ 

  // Cuenta total de divisores
  int total = 1; 

  // Para cada primo se prueba cuantas veces divide a n
  for(int i=0; i < primes.size; i+=1){ 
    int p = primes[i];
    if(p*p > n){break;}
    if((n%p == 0)){ 
      int count = 0; 
      while((n%p) == 0){ 
        n = n / p; 
        count += 1; 
      } 
      total = total * (count + 1); 
    } 
  }
  if(n != 1){total *= 2;} 
  return total; 
} 

// ============================================================================

int main(int num_args, char **args){ 

  // Calcular los números primos 
  int nmax = 1000011;
  bool * isprime = new bool[nmax]; 
  nvect<int>primes; 
  for(int i=2; i < nmax; i+=1){isprime[i] = true; }  
  isprime[0] = false; 
  isprime[1] = false; 

  for(int i=2; i*i < nmax; i+=1){ 
    if(!isprime[i]){ continue; } 
    for(int j=i*2; j<nmax; j+=i){isprime[j] = false;}
    primes.push(i); 
  } 

  // Calcular los números 
  int * nums = new int[nmax];
  nums[0] = 1;

  int len = 1;
  for(int i=1; i < nmax; i+=1){ 
    nums[i] = nums[i-1] + divcount(nums[i-1],primes); 
    len += 1; 
    if(nums[i] >= nmax){ break; } 
  }

  // Leer las preguntas 
  int testcases = 0; 
  std::cin >> testcases; 
  for(int t=0; t < testcases; t+=1){ 
    int a = 0; 
    int b = 0;
    std::cin >> a >> b ;

    // Encontrar el límite inferior
    int l =0; 
    int r =len-1; 
    while(l < r){ 
      int m = (l+r)/2;
      if(nums[m] < a){ l = m+1; } 
      else{ r = m; } 
    } 
    int left = l; 

    // Encontrar el límite superior 
    l = 0; 
    r = len-1; 
    while(l < r){ 
      int m=(l+r)/2; 
      if(nums[m] < b){ l = m+1; } 
      else{r=m;} 
    }
    int right = l; 
    if(nums[l] == b){ right += 1;}

    // Desplegar la información 
    std::cout << "Case " << (t+1) << ": " << (right-left) << std::endl; ; 
  } 


  
  return 0; 
} 
