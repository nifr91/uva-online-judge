// Ricardo Nieto Fuentes
// nifr91@gmail.com
//
// ============================================================================
//
// The path in the colored field --- 10102
// 3.000 seconds
//
// The square field consist of MXM cells. Each cell is colored in one of three 
// colors (1,2,3). The initial state is chosen in one fo the cells of color 1. 
// In each step one allowed to move one cell up,down, left or right remamining
// inside the field. 
// 
// You are to define the minimal amount of steps one should make to get a 
// cell of color 3 independent on the initial state. 
//
// Note that the field contains at least one cell of color 1 and at least one
// cell of color 3. 
//
// Input: 
//
// The input consist of several input blocks. The first line of each block 
// contains integer M, the size of the field. Then There are M lines with 
// colors of the cells. 
//
// Output:
//
// For each input block the output should consist of one line with the 
// integer, the minimal amount of steps one should make to get a cell of color
// 5 independent on the initial state. 
//
// ============================================================================
//
// Sample Input
//
// |4
// |1223
// |2123
// |2213
// |3212
// |2
// |12
// |33
//
// Sample Output
//
// |3
// |1
//
// ============================================================================

#include<iostream>
#include<vector>
using namespace std; 

//-----------------------------------------------------------------------------
// Solución propuesta
// 
// La distancia de una celda a otra 
//
// a 1 2 3
// 1 2 3 4 
// 2 3 4 5
// 3 4 5 b
//
// es la distancia |(a.r - b.r)| + |(a.c - b.c)|
//
// Al leer la matriz podemos almacenar las posiciones de los 1s y de los 3s 
// y después para cada 1, calcular la distancia mínima a un 3s, y regresar
// la máxima distancia de los 1s a los 3s. 
//
// ----------------------------------------------------------------------------


// Máximo valor entre dos números
int max(int a, int b){ return (a < b)? b : a; } 
// Mínimo valor entre dos números 
int min(int a, int b){ return (a < b)? a : b; } 

// Coordenada r,c
struct npair{ 
  int r=0; 
  int c=0; 
  npair(){}
  npair(int _r, int _c):r(_r),c(_c){}
};

// Arreglo que nos permite agregar (push) y eliminar (clear) los objetos en el
struct vect{ 
  npair * buff; 
  int size = 0; 
  int buff_size = 1; 
  vect(){this->buff = new npair[this->buff_size];} 
  ~vect(){delete [] this->buff;}
  npair push(npair val){ 
    this->resize(); 
    this->buff[this->size] = val; 
    this->size += 1; 
    return val;
  } 
  npair pop(void){ 
    this->size -=1; 
    return buff[this->size]; 
  }
  void resize(void){ 
    if(this->size == this->buff_size){ 
      this->buff_size *= 2; 
      auto new_buff = new npair[this->buff_size]; 
      for(int i=0; i < this->size; i+=1){ new_buff[i] = this->buff[i]; }
      delete[] this->buff;
      this->buff = new_buff; 
    } 
  } 
  npair operator[](int index){return this->buff[index];}
  void clear(void){ this->size = 0; } 
};


// Driver 
int main(int argc, char ** argv){ 

  int m; 
  auto ones = vect();
  auto threes = vect(); 
  while(cin >> m){ 
    // Eliminamos los 1s y los 3s
    ones.clear();
    threes.clear();

    // Leemos la siguiente matriz, se agregan las coordenadas al vector 
    // correspondiente
    for(int i=0; i < m; i+=1){ 
      for(int j=0; j < m; j+=1){ 
        char chr; 
        cin >> chr; 
        int val = chr - '0'; 
        if (val == 1){ ones.push(npair(i,j));} 
        if (val == 3){ threes.push(npair(i,j));} 
      }
    }

    // Comparamos para cada uno, la distancia mínima entre el 1 y todos los 3s, 
    // almacenando la distancia máxima de las distancias. 
    int dist = 0; 
    for(int i=0; i < (int)ones.size; i+=1){ 
      auto one = ones[i];
      int one_dist = 1 << 20;
      for(int j=0; j < (int)threes.size; j+=1){ 
        auto three = threes[j];
        one_dist = min(one_dist,abs(one.r - three.r) + abs(one.c - three.c)); 
      } 
      dist = max(dist,one_dist); 
    } 

    // Regresar el valor 
    cout << dist << endl; 
  } 
  return 0; 
} 
