// Ricardo Nieto Fuentes
// nifr91@gmail.com
//
// ============================================================================
// 
// We have heard a lot recently about the Y2K problem. According to the doom
// sayers, planes will fall out the skies, business will crash and the world
// will enter a major depression as the bugs in software and hardware bite
// hard.  While this panic is a very satisfactory state of affairs for the
// computing profession, since it is leading to lots of lucrative jobs, it will
// have a tendency to bring the profession into disrepute when almost no
// problems occur in 1/1/00. To help avoid this unseemly behaviour on  any
// future occasion, you must write a program which will give the correct date
// for (almost) any number of future days - in particular, it must correctly
// predict the date 'N' days ahead of any given date, where 'N' os a number
// less than 1_000_000_000 and the given date is any date before the year
// 3_000. 
//
// Remember that in the standard Gregorian calendar we use there are 365 days
// in a year, except for leap years which there are 366. Leap years are all
// years divisible by 4 and not divisible by 100, except for those divisible by
// 400. Thus 1900 was not a leap year, 1904,1908,...1996 were leap years, 2000
// will be a leap year, 2100 will not be a leap year, etc. The number of days
// in each month in a normal year is: 31,28,31,30,31,30,31,31,30,31,30,31; in a
// leap year, the second month has 29 days. 
//
// Input 
//
// Input will consist of lines containing four numbers, separated by one or 
// more spaces. The first number on each line will be the number of days you 
// have to predict (0 .. 999_999_999), followed by the date in the format 
// 'DD MM YYYY' where 'DD' is the day of the month (1..31), 'MM' is the 
// month (1..12) and 'YYYY' is the year (1998 .. 2999), all inclusive. The 
// input will be terminated by a line containing four 0's. 
//
// Output 
//
// For each line of input, one output line should be produced. This line should
// contain the date which is required number of days ahead of the input date, 
// written in the same format as the input dates.
//
// ===========================================================================
//
// Sample input 
//
// |1 31 12 2999
// |40 1 2 2004
// |60 31 12 1999
// |60 31 12 2999
// |146097 31 12 1999
// |999999 1 1 2000
// |0 0 0 0
//
// Sample output 
//
// |1 1 3000 
// |12 3 2004
// |29 2 2000
// |1 3 3000 
// |31 12 2399
// |27 11 4737
//

#include<iostream>
using namespace std;


// Estructura que nos permite controlar la fecha 
struct date{ 
  int d; 
  int m; 
  int y;
  int days_in_year = 365; 
  int days_in_month[13] = {0,31,28,31,30,31,30,31,31,30,31,30,31};

  // Modificamos el número de días dependiendo  de si es un año bisiesto 
  void update_leap_year_leap(){ 
    if (this->is_leap_year()){ 
      days_in_month[2] = 29;
      days_in_year = 366; 
    }else{ 
      days_in_month[2] = 28; 
      days_in_year = 365; 
    }
  }

  // Movemos la fecha al siguiente año 
  int next_year(){ 
    auto days_advanced = this->remaining_days_in_year() + 1;   
    this->d = 1; 
    this->m = 1; 
    this->y += 1; 
    this->update_leap_year_leap(); 
    return days_advanced;
  } 

  // Movemos la fecha al siguiente mes
  int next_month(){
    if(this->m == 12){return (this->next_year()); } 
    int days_advanced = this->remaining_days_in_month() + 1;
    this->d = 1; 
    this->m += 1;
    return days_advanced; 
  }

  // Avanzamos n días 
  void advance_days(long long n){
    // Hasta que avancemos todos los días
    while(n > 0){ 
      // Si podemos avanzar un año 
      if (n > this->remaining_days_in_year()){ n -= this->next_year(); }
      // Si podemos avanzar un mes
      else if ( n > this->remaining_days_in_month()){ n -= this->next_month();}
      // Si podemos avanzar los días dentro del mes
      else { this->d += n; n = 0;} 
    }
  } 

  // Calcular el número de días restantes en el mes
  int remaining_days_in_month(){ 
    return (this->days_in_month[this->m] - this->d);  
  } 

  // Calcular el número de días restantes en el año 
  int remaining_days_in_year(){ 
    int sum = 0; 
    for(int i=1; i < m; i+=1){sum += days_in_month[i];} 
    sum += d;
    return (days_in_year - sum); 
  }

  // Ver si es un año bisiesto 
  bool is_leap_year(){ 
    return ((((this->y % 4) == 0) && (( this->y % 100) != 0)) || ((this->y % 400) == 0)); 
  }
 
  // Imprimir el resultado 
  string to_s(){ 
    string s = ""; 
    s += to_string(this->d); 
    s += " "; 
    s += to_string(this->m); 
    s += " "; 
    s += to_string(this->y); 
    return s; 
  } 
}; 

// Main Program ===============================================================

int main(int argc, char ** argv){ 
  long long n; 
  date d; 

  while(true){ 
    // Leer los datos 
    cin >> n >> d.d >> d.m >> d.y; 
    // Terminar si se llega al final del archivo 
    if ((n == d.d) && (d.m == d.y) && (d.d == d.m) && (n == 0)){ break; } 
    
    // Verificar si estamos en año bisiesto 
    d.update_leap_year_leap(); 

    // Avanzar n días
    d.advance_days(n); 
    
    // Imprimir el resultado 
    cout << d.to_s() << endl; 
  } 

  return(0); 
} 
