// Ricardo Nieto Fuentes
// nifr91@gmail.com
//
// ============================================================================
//
// Anagrams by Stack --- 732
// 3.000 seconds 
//
// How can anagrams result from sequences of stack operations? There are two
// sequences of stack operators which can convert TROT to TORT: 
//
// [
// i i i i o o o o
// i o i i o o i o
// ]
//
// where 'i' stands for push and 'o' stands for pop. Your program should, 
// given pairs of words produce sequences of stack operations which convert
// the first word to the second. 
//
// Input: 
//
// The input will consist of several lines of input. The first line of each 
// pair of input line is to be considered as a source word (which does not
// include the end-of-line character). The second line (again, not 
// including the end-of-line character) of each pair is a target word.
//
// Output: 
//
// For each input pair, your program should produce a sorted list of valid 
// sequences of 'i' and 'o' which produce the target word from the source 
// word. Each list should be delimited by 
// |[ 
// |] 
// and the sequences should be printed in "dictionary order". Within each 
// sequence, each 'i' and 'o' is followed by a single space and each 
// sequence is terminated by a new line. 
//
// Process 
//   A stack is a data storage and retrieval structure permitting two operations:
//     * push - to insert an item 
//     * pop  - to retrieve the most recently pushed item
//
//   we will use the symbol 'i' (int) for push and 'o' (out) for pop operations
//   for an initially empty stack of characters. Given an input word, some 
//   sequences of push and pop operations are valid in that every character of 
//   the word is both pushed and popped, and furthermore, no attempt is ever 
//   made to pop the empty stack. For example in the word FOO is input, then
//   the squence: 
//
//     i i o i o o -- is valid
//     i i o       -- is not valid (its too short)
//     i i o o o i -- is not valid (theres an illegal pop of an empty stack
//
//   Valid sequences yield rearrangements of the letters in an input word. 
//   For example, the input word FOO and the sequence 'i i o i o o' produce
//   the anagram OOF. So also would the sequence 'i i i o o o'. You are to 
//   write a program to input pairs of words and output all the 
//   valid sequences of 'i' and 'o' which will produce the second member of each
//   pair from the first. 
// 
// =============================================================================
//
// Sample Input
//
// |madam
// |adamm
// |bahama
// |bahama
// |long
// |short
// |eric
// |rice
//
// Sample Output
//
// |[
// |i i i i o o o i o o
// |i i i i o o o o i o
// |i i o i o i o i o o 
// |i i o i o i o o i o
// |]
// |[
// |i o i i i o o i i o o o
// |i o i i i o o o i o i o
// |i o i o i o i i i o o o
// |i o i o i o i o i o i o
// |]
// |[
// |]
// |[
// |i i o i o i o o
// |]
//
// ============================================================================


// Solution -------------------------------------------------------------------
// 
// Generar de forma recursiva las posibles soluciones, eliminando aquellas 
// ramas que llevarían a una solución invalida. 
//
// ----------------------------------------------------------------------------

#include<iostream>
using std::cout; 
using std::cin; 


// Estructura con pila 
template<typename T> struct stack{ 
  T buff[10000] = {}; 
  int size = 0; 

  // Agregar un elemento al final 
  T push(T val){ 
    this->buff[this->size] = val; 
    this->size +=1; 
    return val ; 
  } 

  // Eliminar un elemento 
  T pop(){
    this->size -= 1;
    return this->buff[this->size];  
  } 
 
  // Acceder a un elemento 
  T operator[](int index){return this->buff[index];} 
}; 

// Determinar si es una solución parcial
bool ispartialsol(stack<char> & sol, std::string & ts){ 
  for(int i=0; i < sol.size; i+=1){ if(sol[i] != ts[i]){ return false;} } 
  return true; 
} 

// Resolver de forma recursiva
void solve(stack<char> & seq, stack<char> & s, stack<char> & aux, std::string & fs, std::string & ts,int pos){ 

  // Se aborta esta rama si no es posible llegar a una solución parcial 
  if (!ispartialsol(s,ts)){ return; } 

  // Caso base
  if((s.size == (int)ts.size())){
    bool issol = true; 
    for(int i=0; i < s.size; i+=1){ 
      if(s[i] != ts[i]){
        issol = false; 
        break; 
      } 
    } 

    if(issol){
      std::cout << seq[0] ; 
      for(int i=1; i < seq.size; i+=1){std::cout <<  " " << seq[i];} 
      std::cout << std::endl; 
      return;
    } 
  } 

  // Intentamos agregar 
  if(pos < (int)fs.size()){
    aux.push(fs[pos]);
    seq.push('i');
    solve(seq,s,aux,fs,ts,pos+1);
    seq.pop();  
    aux.pop();
  } 

  // Intentamos eliminar 
  if(aux.size > 0){ 
    s.push(aux.pop());
    seq.push('o');
    solve(seq,s,aux,fs,ts,pos);
    seq.pop();
    aux.push(s.pop());
  } 
} 

// Abortar si no va a ser posible realizar el anagrama
bool ispossible(std::string & fs,std::string & ts){
  if (fs.size() != ts.size()){ return false; } 
  int count[250] = {};
  for(int i=0; i < (int)fs.size(); i+=1){count[fs[i]-'a'] += 1;} 
  for(int i=0; i < (int)fs.size(); i+=1){count[ts[i]-'a'] -= 1;}
  for(int i=0; i < 250; i+=1){ if(count[i] != 0){ return false; }} 
  return true; 
} 

// ============================================================================

int main(int num_args, char ** args){ 
  std::string fs = ""; 
  std::string ts = "";
  while(cin >> fs >> ts){ 
    stack<char> s; 
    stack<char> seq;
    stack<char> aux;
    std::cout << "[" << std::endl; 
    if(ispossible(fs,ts)){ solve(seq,s,aux,fs,ts,0);} 
    std::cout << "]" << std::endl;
  } 
  return 0; 
} 
