// Ricardo Nieto Fuentes
// nifr91@gmail.com
//
// ===========================================================================
//
// Borrowers --- 230 
// 3.000 seconds 
//
// I mean your _borrowers of books_ -- those mutilators of collections, spoilers
// of symmetry of shelves, and creators of odd volumes. 
//              |--(charles Lamb, Essays of Elia (1823) 'The two races of men'). 
// 
// Like Mr. Lamb, librarians have their problems with borrowers too. People
// don't put books back where they should. Instead, returned books are kept at
// the main desk until a librarian is free to replace them in the right places
// on the shelves. Even for librarians, putting the right book on the right
// place can be very time-consuming. But since many libraries are now
// computerized, you can write a program to help. 
// 
// When a borrower takes out or returns a book, the computer keeps a record of
// the title. Periodically, the librarians will ask your program for a list of
// books that have been returned so the books can be returned to their correct
// places on the shelves. Before they are returned to the shelves, the returned
// books are sorted by author and then title using the ASCII collating
// sequence.  Your program should output the list of returned books in the same
// order as they should appear in the shelves. For each book, your program
// should tell the librarian which book (including those previously shelved) is
// already on the shelf before which the returned should go. 
//  
// Input: 
//
// First, the stock of the library will be listed, one book per line, in no 
// particular order. Initially, the are all on the shelves. No two books have
// the same title. The format of each line will be: 
//
// |"title" by author 
//
// The end of the stock listing will be marked by a line containing only the 
// world: 
// 
// |END
//
// Following the stock list will be a series of records of books borrowed and
// returned, and requests from librarians for assistance in restocking the 
// shelves. Each record will appear on a single line, in one of the following
// formats: 
//
// |BORROW title
// |RETURN title
// |SHELVE
//
// The list will be terminated by a line containing only the word: 
//
// |END
//
// Output: 
//
// Each time the SHELVE command appears, your program should output a series
// of instructions for the librarian, one per line int he format: 
// 
// |Put title1 after title2
//
// or, for the special case of the book being the first of the collection: 
// 
// |Put title first
//
// After the set of instructions for each SHELVE, output a line containing 
// only the word: 
//
// |END
//
// Assumptions: 
// 
// . A title is at most 80 characters long. 
// . An author is at most 80 characters long.
// . A title will not contain the double quote (") character.
// 
// ============================================================================
//
// Sample Input: 
//
// |"The Canterbury Tales" by Chaucer, G.
// |"Algorithms" by Sedgewick, R.
// |"The C Programming Language" by Kernighan, B. and Ritchie, D.
// |END
// |BORROW "Algorithms"
// |BORROW "The C Programming Language"
// |RETURN "Algorithms"
// |RETURN "The C programmingLAnguage"
// |SHELVE
// |END
// 
// Sample Output: 
//
// |Put "The C Programming Language" after "The Canterbury Tales"
// |Put "Algorithms" after "The C programming Language"
//
//=============================================================================


// Solución -------------------------------------------------------------------
// Almacenar los libros en un arreglo
// Ordenar
// Para cada opción 
//  borrow - Almacenar libro prestado y marcarlo como prestado
//  return - Almacenar libro regresado eliminándolo de los libros prestados
//  shelve - Ordenar los libros regresado
//           Buscar la posición de cada libro regresado y desmarcarlo 
//           Imprimir
// ----------------------------------------------------------------------------

#include<iostream>
using namespace std; 

// Estructura que representa un libro tiene un nombre, y author, su comparación
// es por author y luego nombre 
struct book{
  string name =""; 
  string author = ""; 
  bool operator<(const book & other) const{ 
    if (this->author > other.author){ return false;}
    if (this->author == other.author){ return this->name < other.name;}
    return true; 
  }
} ;


// Ordenar una lista con sort_estable en O(n lg n), requiere el operador <
template <class T> 
void 
merge_sort(T * a,int len){ 
  if(len < 2){ return; } 

  int llen = len/2; 
  int rlen = len - llen; 

  T la[llen] = {}; 
  for(int i=0; i < llen; i+=1){ la[i] = a[i]; } 
  T ra[rlen] = {};
  for(int i=0; i < rlen; i+=1){ ra[i] = a[i+llen];} 

  merge_sort(la,llen); 
  merge_sort(ra,rlen); 

  int li=0; 
  int ri=0; 
  int ai=0;
  while((li < llen) && (ri < rlen)){ 
    if(ra[ri] < la[li]){ 
      a[ai] = ra[ri]; 
      ri += 1; 
    }
    else{ 
      a[ai] = la[li]; 
      li += 1; 
    } 
    ai += 1; 
  } 
  while(li < llen){ 
    a[ai] = la[li]; 
    li+=1; 
    ai+=1; 
  } 
  while(ri < rlen){ 
    a[ai] = ra[ri]; 
    ri += 1; 
    ai += 1;
  } 
} 

// Leer la información de un libro
bool 
read_book(book & b){ 
  // Bandera para saber si se escribe el nombre o el autor
  bool author = false; 

  // Leer la línea y regresar falso si no hay nada que procesar
  string line = "";
  getline(cin,line);
  if(line == "END"){return false;}

  // Para cada letra de la línea, se genera una palabra, cuando se encuentra
  // un espacio se agrega al autor o al nombre en función de la bandera
  string word = "";
  bool first_word = true;
  for(int i=0; i < (int)line.size(); i+=1){ 
    char c = line[i];  
    if (c == ' '){
      if(word == "by"){
        author = true;
        word = ""; 
        first_word = true;
        continue;
      }
      if(!first_word){word = " " + word;} 
      else{first_word = false;} 
      if(author){b.author += word;}
      else{ b.name += word;} 
      word = ""; 
    }else{word += c;} 
  } 
  b.author +=  " " + word; 
  return true; 
} 

// ==========================================================================

int 
main (int num_args, char ** args){ 

  // No esperamos más de 100_000 libros 
  int NBOOKS = 100000;

  // Vectores para almacenar el estado 
  book books[NBOOKS]      = {};
  bool isborrowed[NBOOKS] = {};
  int borrowed[NBOOKS]    = {};
  int returned[NBOOKS]    = {};

  // Leer los libros 
  int line_num = 0; 
  while(read_book(books[line_num])){ 
    isborrowed[line_num] = false; 
    line_num += 1; 
  } 
  int nbooks = line_num;

  // Ordenar los libros 
  merge_sort<book>(books,nbooks);

  // Leer las operaciones
  string operation = ""; 
  string name = ""; 
  int borrow_cnt = 0;
  int return_cnt = 0;
  while(cin >> operation){
    if (operation == "END"){break;}

    // Si la operación es SHELVE no se requiere leer el nombre
    if (operation  != "SHELVE" ){ 
      cin.ignore();  
      getline(cin,name);
    }

    // Si se presta, se marca como prestado y se agrega a los libros prestados
    if(operation == "BORROW"){ 
      for(int i=0; i < nbooks; i+=1){ 
        if(books[i].name == name){
          isborrowed[i] = true;
          borrowed[borrow_cnt] = i;
          borrow_cnt += 1;
          break; 
        } 
      } 
    }
    // Si se regresa se elimina de los prestados y se agrega a los regresados
    else if(operation == "RETURN"){ 
      for(int i=0; i < borrow_cnt; i+=1){ 
        if(books[borrowed[i]].name == name){
          returned[return_cnt] = borrowed[i];
          borrow_cnt -=1;
          return_cnt +=1;
          int aux = borrowed[borrow_cnt];
          borrowed[borrow_cnt] = i; 
          borrowed[i] = aux; 
          break;
        } 
      }
    }
    // Cuando se almacenan se ordenan, cada uno se desmarca como prestado y 
    // se imprime el primer libro anterior que no este prestado.
    else if (operation == "SHELVE"){
      merge_sort<int>(returned,return_cnt); 
     
      for(int i=0; i < return_cnt; i+=1){
        isborrowed[returned[i]] = false; 
    
        int prev_book = returned[i]-1; 
        while((i >= 0) && isborrowed[prev_book]){ prev_book -= 1;}
        cout << "Put " << books[returned[i]].name  << " "; 
        if(prev_book < 0){ cout << "first";}
        else{ cout << "after " << books[prev_book].name; } 
        cout << endl; 
      } 
      cout << "END" << endl; 
      return_cnt = 0;
    } 
  } 

  return 0;
} 
