// Ricardo Nieto Fuentes
// nifr91@gmail.com 
//
// ============================================================================
//
// Expert Enough? --- 1237
// 3.000 seconds 
//
// Auto-mobile Charting & Manufacturing (ACM) is a company that specializes in 
// manufacturing automobile spare parts. Being one of the leading automotive 
// companies in the world. ACM are sure to keep up the latest information in 
// that world. In the 100-year anniversary of the company, ACM compiled a huge
// list of range of prices of any automobiles ever recorded in the history. ACM
// then wants to develop a program that they called Automobile Expert System 
// AES for short. 
//
// The program receives a prices P as an input, and searches through the 
// database for a car maker in which 'P' falls int their range of lowest 
// price 'L' and highest prices 'H' of car they ever made. The program then 
// outputs the car maker name. IF the database contains no or more than one
// car maker that satisfies the query, the program produce output 
// "UNDETERMINED" (without quotes). Not so expert, huh? You are about to 
// develop that program for ACM. 
//
// Input 
//
// The input begins with a line containing an integer T (T <= 10), the number 
// of test cases follow. Each case begins with the size of the database D
// (D < 100_000). The next each of the D lines contains 'M','L' and 'H' 
// (0 < L < H < 1_000_000) which are the name of the maker (contains no 
// whitespace and will never exceed 20 characters), the cars lowest price 
// the maker ever made, and the cars highest price the maker ever made 
// respectively. Then there is the number of query Q (Q < 1_000) follows. 
// Each of the next 'Q' lines contains an integer 'P' (0 < P < 1_000_000), the
// query price. 
//
// Output 
//
// Output for each query should be one line containing the name of the maker or
// the string "UNDETERMINED" (without quotes) if there is no maker or more than 
// than one maker that satisfies the query. You should separate output for 
// different cases by one empty line. 
//
// ==========================================================================
//
// Sample Input 
//
// |1
// |4
// |HONDA 10000 45000
// |PEUGEOT 12000 44000
// |BMW 30000 75900
// |CHEVROLET 7000 37000
// |4
// |60000
// |7500
// |5000
// |10000
//
// Sample Output
// 
// |BMW
// |CHEVROLET
// |UNDETERMINED
// |UNDETERMINED
// 
// ============================================================================

#include<iostream> 
using namespace std; 


int main(int argc, char ** argv){ 
 
  // Leer el número de casos de prueba
  int testcases; 
  cin >> testcases; 
  
  string makers[10000]; 
  int    lowest[10000];
  int    highest[10000];

  // Procesar cada caso de prueba
  for(int t=0; t < testcases; t+=1){ 
    // Separar los casos de prueba con un espacio
    if( t > 0) { cout << endl; } 

    // Leer los datos 
    int num_makers; 
    cin >> num_makers; 
    for(int i=0; i < num_makers; i+=1){ 
      cin >> makers[i]; 
      cin >> lowest[i]; 
      cin >> highest[i]; 
    } 
    int num_queries; 
    cin >> num_queries; 

    // Para cada pregunta 
    for(int qi=0; qi < num_queries; qi += 1){ 
      int query; 
      cin >> query; 


      // Recorrer el arreglo y ver cuantos  elementos están en el rango 
      int cnt = 0; 
      int index = -1; 
      for(int i=0; i < num_makers; i+=1){
        if((highest[i] >= query) && (lowest[i] <= query)){ 
          cnt+=1; 
          index = i; 
          if (cnt > 1){ break; } 
        } 
      } 

      // Si no existe elemento o hay mas de uno no hay respuesta, en caso de
      // que exista uno solo se presenta el nombre del productor de coches.
      if(!cnt || (cnt > 1)){ cout << "UNDETERMINED" << endl; }
      else{ cout << makers[index] << endl; } 
    } 
  }
 
} 
