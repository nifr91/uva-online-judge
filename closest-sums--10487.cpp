// Ricardo Nieto Fuentes
// nifr91@gmail.com 
//
// ============================================================================
//
// Closest Sums --- 10487
// 3.000 seconds
//
// Given is a set of integers and then a sequence of queries. A query gives 
// you a number and asks yo find a sum of two distinct numbers from the set, 
// which is closest to the query number. 
//
// Input 
//
// Input contains multiple cases. 
// Each case starts with an integer n ( 1 < n <= 1000), which indicates, how 
// many numbers are in the set of integers. Next 'n' lines contains 'n' numbers.
// Of course there is only one number in a single line. The next line contains
// a positive integer 'm' giving the number of queries 0 < m < 25. The next
// 'm' lines contain an integer of query, one per line.
// Input is terminated by a case whose 'n = 0'. Surely, this case needs no 
// processing. 
//
// Output 
//
// Output should be organized as in the sample below. For each query output
// one line giving the query value and the closest sum in the format as in the
// sample. 
// Inputs will be such that no ties will occur. 
//  
// ===========================================================================
//
// Sample Input
// 
// |5
// |3
// |12
// |17
// |33
// |34
// |3
// |1
// |51
// |30
// |3
// |1
// |2
// |3
// |3
// |1
// |2
// |3
// |3
// |1
// |2
// |3
// |3
// |4
// |5
// |6
// |0
//
// Sample Output 
//
// |Case 1:
// |Closest sum to 1 is 15.
// |Closest sum to 51 is 51.
// |Closest sum to 30 is 29.
// |Case 2:
// |Closest sum to 1 is 3.
// |Closest sum to 2 is 3.
// |Closest sum to 3 is 3.
// |Case 3:
// |Closest sum to 4 is 4.
// |Closest sum to 5 is 5.
// |Closest sum to 6 is 5.
//
//==============================================================================

#include<iostream>
using namespace std; 

int main(int argc, char ** argv){ 

  int a[1000]; 
  
  int t=1;
  while(true){ 
    int n; 
    cin >> n;
    if (n == 0){break; } 
    for(int i=0; i < n; i+=1){ cin >> a[i]; } 
  
    cout << "Case " << t << ":" << endl; 

    int m; 
    cin >> m; 
    for(int q=0; q < m; q+=1){ 
      int integer; 
      cin >> integer;
      
      int closest = a[0] + a[1]; 
      int closest_diff = abs(integer - closest);
      for(int i=0; (i < n) && (closest_diff > 0); i+=1){ 
        for(int j=i+1; (j < n) && (closest_diff > 0); j+=1){ 
          int sum = a[i] + a[j]; 
          int diff = abs(integer - sum);
          if ((diff) < (closest_diff)){
            closest_diff  = diff; 
            closest = sum;
          } 
        } 
      } 
      cout << "Closest sum to " << integer << " is " << closest << "." <<endl;
    } 
    t+=1;
  } 

  return 0; 
} 
