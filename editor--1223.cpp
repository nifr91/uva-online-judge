// Ricardo Nieto Fuentes
// nifr91@gmail.com
//
// ===========================================================================
//
// Editor --- 1223
//
// Mr. Kim is a professional programmer. Recently he wants to design a new 
// editor which has as many functions as possible. Most editors support a simple
// search function that finds one occurrence (or all occurrences successively) 
// of a query patter string in the text. 
//
// He observed that the search function in commercial editors (does nothing)
// if no query patter is given. His idea of a new search function regards each
// substring of the given text as a query patter string itself an his new 
// function finds another occurrence in the text. The problem is that there can
// be occurrences of many substrings in the text. So, Mr. Kim decides that 
// the new functions finds only occurrences of the longest substring in the 
// text in order to remedy the problem. A formal definition of the search 
// function is as follows: 
//
// Given a text string 's', find the longest substring in text string 's' such 
// that the substrings appears at least twice. The two occurrences are allowed
// to overlap. 
//
// Input : 
//
// Your program is to read from standard input. The input consists of 'T' test
// cases. The number of test cases 'T' is given in the first line of the input.
// For each test case, a text string 'S' is given in one line. Fr every string,
// the length is less than 5_000 and the alphabet is the set of lowercase 
// English characters. 
//
// Output: 
//
// Your program is to write to standard output. Print exactly one line for each 
// test case. Print the length of the longest substring in the text string 'S'
// such that the substring appears at leas twice. 
//
// ============================================================================
//
// Sample Input: 
// 
// |3
// |abcdefghikjlmn
// |abcabcabc
// |abcdabcabb
//
// Sample Output
//
// |0
// |6
// |3
//
// ============================================================================
#include<iostream> 
using namespace std; 

// Solución -------------------------------------------------------------------
// 
// Generar un arreglo de sufijos, generar el arreglo de prefijo común más largo
// empleando el arreglo de sufijos y regresar el máximo valor. 
//
// ----------------------------------------------------------------------------


// Es una permutación de los posibles sufijos, representados por 
// el índice de su primer elemento en la cadena original, en el que 
// los sufijos están en orden lexicográfico.
//
struct suffixarray{ 

  // El rango de un sufijo (suffix::rank) es un par de enteros < len(str) ; {f,s} 
  // que índica el orden relativo que tienen el sufijo hasta i+(2**k) y 
  // hasta i + (2**(k+1)) el valor 'i' indica la posición en donde inicia 
  // el sufijo en la cadena original 
  //
  struct suffix{ struct rank{
      int f = 0; 
      int s = 0; 
      int i = 0; 
      rank(){}
      rank(int _f, int _s,int _i):f(_f),s(_s),i(_i){} 

      bool operator<(const rank & other) const{ 
      if (this->f == other.f){ return (this->s < other.s); } 
      return(this->f < other.f); 
      }

      bool operator==(const rank & other) const{ 
        return((this->f == other.f) && (this->s == other.s)); 
      } 
      bool operator!=(const rank & other) const{return !(*this == other);} 
    };
  }; 


  // Sort estable 
  void mergesort(suffix::rank * a, int len){ 
    // Caso base 
    if(len < 2){ return; } 

    // Dividir en dos el arreglo 
    int m = len/2;
    int llen = m; 
    int rlen = len - m; 

    // Copiar el arreglo  en los dos sub-arreglos 
    suffix::rank l[llen] = {};
    for(int i=0; i < llen; i+=1){l[i] = a[i];} 

    suffix::rank r[rlen] = {};
    for(int i=0; i < rlen; i+=1){r[i] = a[i+m];} 
   
    // Ordenar los dos sub-arreglos
    mergesort(l,llen);
    mergesort(r,rlen);

    // Combinar los sub-arreglos
    int li=0; 
    int ri=0; 
    int ai=0;
    while ((li < llen) && (ri < rlen)){ 
      if((l[li] < r[ri]) || (l[li] == r[ri])){ 
        a[ai] = l[li]; 
        li += 1;
      }else{ 
        a[ai] = r[ri]; 
        ri += 1;
      } 
      ai += 1;
    } 

    while(li < llen){ 
      a[ai] = l[li]; 
      li += 1;
      ai += 1;
    } 
    while(ri < rlen){ 
      a[ai] = r[ri]; 
      ri += 1;
      ai += 1;
    } 
  } 


  // Permutación del arreglo que define al suffix array
  int sa[40000];
  // Tamaño 
  int size = 0; 
  // Cadena original 
  string str = ""; 

  // constructor 
  suffixarray(string & str){ 
    // Añadimos un char especial  al final del string
    this->str = str + "$";
    this->size = this->str.size();

    // Rango de cada sufijo 
    suffix::rank ranks[this->size];

    // Calcular el rango de cada sufijo (en función de su orden en el alfabeto,
    // a < b <... < z < A < ...  <Z < 0 .. < 9 .. )
    for(int i=0; i < this->size-1; i+=1){ 
      ranks[i] = suffix::rank(this->str[i]-'$'+1,this->str[i+1]-'$',i);
    }
    ranks[this->size-1].i = this->size-1;
   
    // Ordenar los sufijos por las dos primeras letras 
    // $$ < a$ < ab$ ... 
    mergesort(ranks,this->size);

    // de forma similar a radix sort, se van ordenando los sufijos en 
    // potencias de 2
    //
    int rnk = 0; 
    int offset = 1; 
    int tmprank[this->size];
    while(rnk < this->size-1){ 
      
      // Los sufijos tienen rango distinto si son diferentes hasta la 
      // k-ésima potencia y tienen el mismo rango si son iguales
      rnk = 0;
      tmprank[0] = ranks[0].f ;
      ranks[0].f = rnk;
      for(int i=1; i < this->size; i+=1){
        bool increment = ranks[i].s != ranks[i-1].s;
        increment = increment || (ranks[i].f != tmprank[i-1]);
        if(increment){rnk +=1;}
        tmprank[i] = ranks[i].f;
        ranks[i].f = rnk;
      } 

      // mapa del rango que tiene cada posición 
      // r0 r1 r2 ...
      for(int i=0; i < this->size; i+=1){tmprank[ranks[i].i] = ranks[i].f;} 

      // El siguiente sufijo de tamaño 2**(k+1) contiene al sufijo (2**k)
      // por lo que empleamos el rango sufijo de tamaño 2**k como la segunda
      // parte del rango de 2**(k+1)
      for(int i=0; i < this->size; i+=1){ 
        if((ranks[i].i+(1<<offset)) > this->size){ranks[i].s = 0;}
        else{ranks[i].s = tmprank[ranks[i].i+(1 << offset)];} 
      } 

      // Ordenar los sufijos por las dos primeras letras 
      // $$ < a$ < ab$ ... 
      mergesort(ranks,this->size);

      // Pasamos a la siguiente potencia
      offset += 1;
    } 

    // Almacenar el orden en que aparecen los índices de los sufijos en el 
    // arreglo de sufijos
    for(int i=0; i < this->size; i+=1){this->at(i) = ranks[i].i;} 
  }

  int & at(int index){return this->sa[index];} 

  string to_s() { 
    string str = "";
    for(int i=0; i < this->size; i+=1){ 
      str += to_string(this->at(i));
      str += " "; 
      str += this->str.substr(this->at(i),this->size);
      str += "\n";
    }
    str += "\n";
    return str; 
  } 
};


// El arreglo de prefijo común más largo es un arreglo que contiene en cada 
// posición i el valor máximo de caracteres que comparte el sufijo i con el 
// sufijo i+1 en el arreglo de sufijos.
//
struct lpcarray{ 
  // Cantidad de caracteres compartidos de i con i+1
  int lpc[40000] = {}; 
  // Tamaño 
  int size = 0; 

  // Constructor 
  lpcarray(suffixarray & sa){ 
    
    this->size = sa.size; 
   
    // Para poder calcularlo de forma eficiente realizamos el calculo 
    // empleando el orden de los sufijos en la cadena original para ello 
    // se crea un mapa de sa -> str
    int inv[this->size] = {}; 
    for(int i=0; i < this->size; i+=1){inv[sa.at(i)] = i;} 

    // Para cada sufijo 
    int k = 0; 
    for(int i=0; i < this->size; i+=1){ 

      // Si es el último sufijo no hay mas que hacer, pasamos al siguiente
      if(inv[i] == (this->size-1)){
        k = 0;
        continue;
      } 

      // Nos colocamos en el siguiente sufijo
      int ni = sa.at(inv[i]+1); 
      while(true){ 
        // Solo mientras son iguales avanzamos 
        if((i+k)  >= this->size){break;} 
        if((ni+k) >= this->size){break;} 
        if((sa.str[i+k] != sa.str[ni+k])){break;} 
        k+=1;
      } 
     
      // Guardamos cuantos caracteres tienen en común
      this->at(inv[i]) = k;

      // el siguiente sufijo tiene al menos k-1 caracteres en común
      if(k > 0){k-=1;} 
    } 
  }

  int & at(int index){ return this->lpc[index];}

  string to_s() { 
    string str = "";
    for(int i=0; i < this->size; i+=1){ 
      str += to_string(this->at(i));
      str += "\n";
    }
    str += "\n";
    return str; 
  }
} ;

// Regresa el índice en el arreglo de prefijos que tiene el máximo valor.
int max_prefix(lpcarray & lpc){ 
  int mx = 0; 
  for(int i=0; i < lpc.size; i+=1){
    if(mx < lpc.at(i)){mx = lpc.at(i);}
  } 
  if(mx == 0){ return -1; } 
  for(int i=1; i < lpc.size; i+=1){if(mx == lpc.at(i)){ return i;}} 
  return -1; 
} 

// ============================================================================

int main(int num_args, char ** args){ 
  int num_testcases = 0; 
  cin >> num_testcases; 
  for(int t=0; t < num_testcases; t+=1){ 
    string str = ""; 
    cin >> str; 

    auto sa = suffixarray(str); 
    auto lpc = lpcarray(sa);

    cout << lpc.at(max_prefix(lpc)) << endl;
  } 
} 
