// Ricardo Nieto Fuentes
// nifr91@gmail.com
// ============================================================================
// 
// Stone Game --- 10165 
// 3.000 seconds 
//
// Jack and Jim are playing an interesting stone game. At the beginning of the
// game there are N pile(s) of stones. Each pile has pi (i = 1..N, 1 <= P <=
// 2*10**9)  stones. They take turns to take away some of the stones. 
// There are some rules: they must choose one pile at a time. They can take 
// away any number of stones except 0, of course, not bigger than the number of
// stones in the pile. One who takes away the last stone will win the game. 
// Jack is the first to fetch the match, and Jim is the second. Now Jack ask
// for your help, to decide while facing some initializations whether he is 
// sure to win or not. 
//
// Input: 
//
// The input file contains several scenarios. Each of them consists of 2 lines:
// The first line consists of a number 'N'. The second line consists of 'N' 
// numbers meaning pi (i = 1..N). 
// There is only one space between two border numbers.
// The input files is ended with N=0.
//
// Output: 
//
// For each scenario, print a line containing 'Yes' if he is sure to win or 
// 'No' otherwise. 
// 
// ============================================================================
// Sample Input: 
//
// |1
// |100
// |3
// |1 5 1
// |4
// |1 1 1 1
// |0
//
// Sample Output: 
//
// |Yes
// |Yes
// |No
//
// ============================================================================

// Solución -------------------------------------------------------------------
//
// Consiste en un juego de nim, por lo que basta en calcular el xor de todos 
// los números si es == 0 no gana y si es distinto gana.
// ----------------------------------------------------------------------------

#include<iostream>
using std::cin; 
using std::cout;
using std::endl; 

int main(int num_args, char **args){ 
  int n = 0; 
  
  while(cin >> n){
    if (!n){break;} 
    int nimxor = 0; 
    for(int i=0; i < n; i+=1){ 
      int pi = 0; 
      cin >> pi; 
      nimxor ^= pi; 
    }
    cout << ((nimxor) ? "Yes" : "No") << endl; 
  } 

  return 0; 
} 


