// Ricardo Nieto Fuentes
// nifr91@gmail.com
//
// ============================================================================
//
// Horror Dash --- 11799 
// 
// It is that time of the year again!
//
// It is that time of the year again! Colorful balloons and brightly colored
// banners spread out over your entire neighborhood for just this one occasion.
// It is the annual clown's festival at your local school. For the first time
// in their lives, students from the school try their hands at being the best
// clown ever. Some walk on long poles, others try to keep a crowd laughing for
// the day with stage comedy, while others still try out their first juggling
// act --- some `master clowns' even teach these juggling tricks to visitors at
// the festival.  
//
// As part of the festival, there is a unique event known as the "Horror Dash".
// At this event,N (1 <= N <= 100) students dressed in the scariest costumes
// possible start out in a race to catch a poor clown running on the same
// track. The clown trips over, loses his mind, and does all sorts of comical
// acts all while being chased round and round on the track. To keep the event
// running for as long as possible, the clown must run fast enough not to be
// caught by any of the scary creatures. However, to keep the audience on the
// edge of their seats, the clown must not run too fast either. This is where
// you are to help. Given the speed of every scary creature, you are to find
// out the minimum speed that the clown must maintain so as not to get caught
// even if they keep on running forever.
//
// Input ----------------------------------------------------------------------
//
// The first line of input contains a single integer T (T <= 50), the number of
// test cases. This line is followed by T input cases. Each input case is on a
// single line of space-separated integers. The first of these integers is N, 
// the number of students acting as scary creatures. The rest of the line has 
// N more integers c_0,c_1,....c_n-1, each representing the speed of a creature 
// in meters per second (1 <= c_i <= 10_000 for each i). You can assume that
// they are always running in the same direction on the track. 
//
// Output ---------------------------------------------------------------------
//
// There should be a single line of output for each test case, formatted as 
// "Case c: s'. Here, 'c' represents the serial number of the input case,
// starting with 1, while 's' represents the required speed of the clown in
// meters per second. 
//
// ============================================================================
//
// Sample input
// 
// |2
// |5 9 3 5 2 6
// |1 2
//
// Sample output 
//
// |Case 1: 9
// |Case 2: 2
//

#include <iostream>
using namespace std;

int main(void){ 

  // Leer la cantidad de casos
  int num_test_cases; 
  cin >> num_test_cases; 

  // Procesar cada caso
  for(int i=0; i < num_test_cases; i+=1){ 
    
    // Leer el primer número, la cantidad de criaturas 
    int num_creatures; 
    cin >>  num_creatures; 

    // Para cada criatura leer su velocidad y si es más rápida que nuestra
    // velocidad del payaso, actualizamos la velocidad mínima para no ser 
    // alcanzados.
    int min_vel = 0;
    for(int k=0; k < num_creatures; k+=1){ 
      int vel; 
      cin >> vel; 
      min_vel = (vel > min_vel) ? vel : min_vel; 
    } 

    // Regresamos la velocidad mínima para no ser alcanzado
    cout << "Case " << (i+1) << ": " << min_vel << endl; 
  } 

  return(0);
} 
