// Ricardo Nieto Fuentes
// nifr91@gmail.com
//
// ===========================================================================
//
// Have Fun with Matrices --- 11360
// 2.000 seconds
//
// We have a matrix of size NXN. Each value of the matrix occupies an integer
// from [0,9]. A few operations are going to be preformed on this matrix. 
// We would like to know hoe the matrix looks like after these operations are
// performed sequentially.
//
// There could be five different types of operations.
//
// row a b
//  In this operation, row a is interchanged with row b.
// col a b
//  In this operation, col a is interchanged with col b.
// inc 
//   In this operation, every cell value is increased by 1 (modulo 10). That 
//   is, if after adding 1 a cell value becomes 10 we change it to 0.
// dec 
//  In this operation every cell value is decreased by 1 (module 10). That is
//  if after sub-stracting 1, a cell value becomes -1 we change it to 9.
//  
// transpose 
//   In this operation, we simply transpose the matrix. Transposing a matris, 
//   denoted by A^T, means turning all the rows of the given matrix into 
//   columns and vice-versa.
//   e.g 
//   1 2 3    1 4 5
//   4 5 6 => 2 5 8
//   7 8 9    3 6 9
//
// Input: 
//
// The input file starts with an integer 'T' (T < 50) that indicates the number 
// of test cases. Each case starts with a positive integer 'N' (N < 10) that
// represents the size of the matrix. The next 'N' lines contains 'N' integers
// each. The value of each integer is in the range 0..9. Next there is a 
// line with an integer M (M < 50). Each of the next 'M' lines contain an 
// operation each. If the command is 'row a b' or 'col a b', then you must 
// assume 1 <=  a,b <= N and a != b. 
//
// Output: 
//
// For each case, output the case number on the first line. Then on the next 
// N lines output the content of the final matrix. Print a blank line after 
// each case (even after the very last one).
//
// =============================================================================
//
// Sample Input: 
//
// |2
// |4
// |1234
// |5678
// |1234
// |5678
// |1
// |transpose
// |3
// |000
// |111
// |000
// |2
// |row 1 2
// |inc
//
// Sample Output: 
//
// |Case #1
// |1515
// |2626
// |3737
// |4848
// |
// |Case #2
// |222
// |111
// |111
// |
// 
// ============================================================================


// Solution -------------------------------------------------------------------
//
// Empleando un arreglo de arreglos, implementar las tres operaciones 
// ----------------------------------------------------------------------------


#include<iostream>
using namespace std; 

// Esta función se encarga de intercambiar dos filas.
void
swap_row(int ** mat, int from, int to, bool transpose,int n){ 
  for(int i=0; i < n; i+=1){ 
    int aux = (transpose) ? mat[i][from] : mat[from][i]; 
    if (transpose){ 
      mat[i][from] = mat[i][to];
      mat[i][to] = aux;
    }else{ 
      mat[from][i] = mat[to][i];
      mat[to][i] = aux;
    }
  } 
} 

// ============================================================================

int
main(int num_args, char ** args){ 

  // Obtener el número de casos
  int test_cases = 0; 
  cin >> test_cases; 

  for(int t=0; t< test_cases; t+=1){  

    // Obtener el tamaño de la matriz 
    int n = 0; 
    cin >> n; 
   
    // Generar la matriz 
    int matrix[n][n] = {}; 
    int * mat[n] = {}; 
    char chr = 0; 
    for(int r=0; r < n; r+=1){ 
      mat[r] = matrix[r];
      for(int c=0; c < n; c+=1){ 
        cin >> chr; 
        mat[r][c] = chr - '0'; 
      } 
    } 

    // Obtener el número de preguntas 
    int queries = 0; 
    cin >> queries; 

    // realizar las operaciones  en O(N)
    int offset = 0; 
    bool transpose = false; 
    for(int q=0; q < queries; q+=1){ 

      // Leer la operación 
      string op = ""; 
      cin >> op; 
      int from = 0; 
      int to   = 0; 
      if ((op == "row") || (op == "col")){
        cin >> from >> to;
        from -= 1; 
        to -= 1;
      } 
   
      // Ejecutar la operación
      if(op == "inc"){offset += 1;} 
      if(op == "dec"){offset -= 1;}
      if(op == "transpose") {
        transpose = !transpose; 
      }  
      if(op == "row"){ swap_row(mat,from,to,transpose,n); } 
      if(op == "col"){ swap_row(mat,from,to,!transpose,n); } 
    } 

    // Agregar o eliminar el offset
    for(int r=0; r < n; r+=1){ 
      for(int c=0; c < n; c +=1){ 
        mat[r][c] = (((mat[r][c] + offset) % 10)+10) % 10; 
      } 
    } 

    // Imprimir
    cout << "Case #" << (t+1) << endl;  
    if(transpose){
      for(int r=0; r < n; r+=1){
        for(int c=0; c < n; c+=1){cout << mat[c][r];} 
        cout << endl; 
      } 
    }else{ 
      for(int c=0; c < n; c+=1){ 
        for(int r=0; r < n; r+=1){ cout << mat[c][r]; } 
        cout << endl; 
      }   
    } 

    cout << endl; 
  } 

  return 0; 
} 
