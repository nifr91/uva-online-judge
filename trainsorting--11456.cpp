// Ricardo Nieto Fuentes
// nifr91@gmail.com 
//
// ============================================================================
//
// Trainsorting --- 11456 
// 1.000 seconds 
//
// Erin is an engineer. She drives trains. She also arranges the cars within 
// each train. She prefers to put the cars in decreasing order of weight, with
// the heaviest car at the front of the train. 
//
// Unfortunately, sorting train cars is not easy. One cannot simply pick up a
// car and place it somewhere else. It is impractical to insert a car 
// within an existing train. A car may only be added to the beginning and end
// of the train. 
//
// Cars arrive at the train station in a predetermined order. When each car 
// arrives, Erin can add it to the beginning or end of her train, or refuse to 
// add it at all. The resulting train should be as long as possible, but the
// cars within it must be ordered by weight.
// 
// Given the weights of the cars in the order in which the arrive, what is the
// longest train that Erin can make?
//
// Input: 
//
// The first line is the number of test cases to follow. The test case follow, 
// one after another; the format of each test case is the following: 
//
// The first line contains an integer 0<= n <= 2_000, the number of cars. Each
// of the following 'n' lines contains a non-negative integer giving the weight
// of ta car. No two cars have the same weight. 
//
// Output:
//
// Output a single integer gibing the number of cars in the longest rain that 
// can be made with the given restrictions. 
//
// ============================================================================
//
// Sample Input: 
// 
// |1
// |3
// |1
// |2
// |3
//
// Sample Output: 
//
// |3
//
// ============================================================================


// Solución -------------------------------------------------------------------
// 
//  Calcular la secuencia mas larga máxima de incremento y decremento para 
//  cada posición. 
//
//  Notamos que para cada posición de la secuencia podemos agregar los 
//  carros de la secuencia de decremento más larga puede colocarse al final 
//  del tren. 
//
//  max(lis[i] + lds[i] - 1)
// ----------------------------------------------------------------------------

#include<iostream>

template<typename T> struct nvect{ 
  T buffer[20000] = {}; 
  int size = 0; 
  // Agregar elemento al final 
  T push(T val){ 
    this->buffer[this->size] = val; 
    this->size +=1; 
    return val; 
  } 
  // Eliminar último elemento añadido 
  T pop( ){ 
    this->size -= 1; 
    return this->buffer[this->size]; 
  } 
  // Acceder a un elemento 
  T & operator[](int index){
    if(index < 0){ index  = this->size + index; } 
    return this->buffer[index];
  } 
}; 


int main(int num_args, char ** args){ 
 
  // Leer el número de casos 
  int test_cases = 0; 
  std::cin >> test_cases; 
 
  for(int t=0; t < test_cases; t+=1){ 

    // Leer el número de carros 
    int ncars = 0; 
    std::cin >> ncars; 

    // Leer los pesos 
    nvect<int> wgts; 
    for(int i=0; i < ncars; i+=1){ 
      int wgt = 0; 
      std::cin >> wgt; 
      wgts.push(wgt);
    }

    // Calcular la secuencia de incremento más larga 
    nvect<int> lis; 
    for(int i=0; i <ncars; i+=1){lis.push(1);} 

    for(int i=lis.size-1; i >= 0; i -= 1){ 
      lis[i] = 1;
      for(int j=i+1; j < lis.size ; j+=1){ 
        if(wgts[i] < wgts[j]){lis[i] = std::max(lis[j] + 1, lis[i]);} 
      } 
    } 

    // Calcular la secuencia de decremento más larga 
    nvect<int> lds; 
    for(int i=0; i <ncars; i+=1){lds.push(1);} 
    for(int i=lds.size-1; i >= 0; i -= 1){ 
      lds[i] = 1;
      for(int j=i+1; j < lds.size ; j+=1){ 
        if(wgts[i] > wgts[j]){lds[i] = std::max(lds[j] + 1, lds[i]);} 
      } 
    } 

    // Encontrar la combinación más larga
    int sol = 0; 
    for(int i=0; i < ncars; i+=1){ sol = std::max(sol,lis[i] + lds[i] -1); }

    // Mostrar el resultado 
    std::cout << sol << std::endl;
  } 

} 
