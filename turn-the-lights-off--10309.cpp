// Ricardo Nieto Fuentes
// nifr91@gmail.com 
//
// ============================================================================
//
// Turn the lights off --- 10309
// 3.000 seconds 
//
// Since we all rely on mother earth, it's our duty to save her. Therefore, you
// are now asked to save energy by switching lights off. 
//
// A friend of yours has the following problem in his job. There is a grid of
// size 10x10, where each square has a light bulb and a light switch attached 
// to it. Unfortunately, these lights don't work as they are supposed to. 
// Whenever a switch is pressed, not only its own bulb is switched, but also 
// the ones left, right, above and under it. Of course if a bulb is on the edge
// of the grid, there are fewer bulbs switched. 
//
// When a light switches it means it's now on if it was off before and it's now
// off if it was on before. Look at the following examples, which show only a 
// small part of the whole grid. They show what happens if the middle switch is
// pressed. 'O' stands for a light that's on, '#' stands for a light that's off.
//
// ###    #O#
// ### -> OOO
// ###    #O#
//
// ###    #O#
// OOO -> ###
// ###    #O#
//
// Your friend loves to save energy and asks you to write a program that finds
// out if it is possible to turn all the lights off and of possible how many 
// times he has to press switches in order to turn all the lights off.
//
// 
// Input 
//
// There are several test cases in the input. Each test case is preceded by a 
// single word that gives a name for the test case. After that name there 
// follow 10 lines, each of which contains a 10 letter string consisting of 
// '#' and 'O'. The end of the input is reached when the name string is 'end'. 
//
// Output
//
// For every test case, print one line that consists of the test case name, a 
// single space character and the minimum number of times your friend has to
// press a switch. If it is not possible to switch of all the lights or 
// requires more than 100 presses then the case name should be followed by 
// space and then a '-1'. 
//
// ============================================================================
//
// Sample Input 
//
// |all_off
// |##########
// |##########
// |##########
// |##########
// |##########
// |##########
// |##########
// |##########
// |##########
// |##########
// |all_on
// |OOOOOOOOOO
// |OOOOOOOOOO
// |OOOOOOOOOO
// |OOOOOOOOOO
// |OOOOOOOOOO
// |OOOOOOOOOO
// |OOOOOOOOOO
// |OOOOOOOOOO
// |OOOOOOOOOO
// |OOOOOOOOOO
// |simple
// |#O########
// |OOO#######
// |#O########
// |####OO####
// |###O##O###
// |####OO####
// |##########
// |########O#
// |#######OOO
// |########O#
// |end 
//
// Sample Output 
//
// |all_off 0
// |all_on 44
// |simple 4
//
// ============================================================================
#include<iostream> 
using namespace std; 

// ============================================================================
// Solución propuesta 
//
// Probar las 2^10 possibles configuraciones de activaciones en la primer fila
// y después ir apagando las luces con la segunda fila. 
// 
// ============================================================================

int R = 10; 
int C = 10; 

// Switch 
void flip(bool ** mat, int r, int c){ 
  if ((r-1) >= 0){ mat[r-1][c] = !mat[r-1][c];} 
  if ((r+1) < R) {mat[r+1][c] = !mat[r+1][c];} 
  if ((c-1) >= 0){ mat[r][c-1] = !mat[r][c-1];}
  if ((c+1) < C) { mat[r][c+1] = !mat[r][c+1];} 
  mat[r][c] = !mat[r][c]; 
} 

// Una solución es válida si esta completamente apagado
bool valid_sol(bool ** mat){ 
  bool is_valid = true; 
  for(int r=0; r < R; r+=1){ 
    for(int c=0; c < C; c+=1){ 
      is_valid = (is_valid && mat[r][c]); 
    } 
  } 
  return is_valid;
} 

// Driver =====================================================================

int main(int argc, char ** argv){ 
  
  // Creamos una matriz que contiene el caso de prueba
  bool ** orig = new bool*[R];
  for(int r=0; r < R; r+=1){orig[r] = new bool[C];} 
  // Creamos una matriz que va a irse modificando 
  bool ** mat = new bool*[R];
  for(int r=0; r < R; r+=1){mat[r] = new bool[C];} 
  // Nombre del caso de prueba.
  string name; 

  // Mientras se pueda leer el caso de prueba
  while(cin >> name){ 
    // Terminamos si el nombre es 'end'
    if (name == "end"){ break; } 

    // Obtenemos el valor de los focos, se asigna true si está apagado '#'
    for(int r=0; r < R; r+=1){ 
      for(int c=0; c < C; c+=1){ 
        char chr; 
        cin >> chr;
        mat[r][c] = (chr == '#'); 
        orig[r][c] = mat[r][c]; 
      } 
    }

   
    // Para cada posible combinación de activaciones 'n' de la primer fila
    int min = 1<<30; 
    for (int n=0; n <= (1<<10); n+=1){ 
      
      // Restaurar el valor de la matriz
      for(int r=0; r < R; r+=1){ 
        for(int c=0; c < C; c+=1){ 
          mat[r][c] = orig[r][c];
        } 
      } 

      // Realizar las activaciones
      int cnt = 0;
      for(int c=0; c < C; c+=1){
        if(!(n&(1<<c))){
          flip(mat,0,c); 
          cnt+=1;
        } 
      } 

      // Para cada fila se van apagando los focos encendidos empleando la 
      // fila r+1 
      for(int r=1; r < R; r+=1){ 
        for(int c=0; c < C; c+=1){ 
          if(!mat[r-1][c]){ 
            flip(mat,r,c);
            cnt += 1; 
          } 
        } 
      } 

      // Si todos están apagados y hemos encontrado una mejor solución 
      // actualizamos el número de operaciones 
      if((cnt < min) && valid_sol(mat)){min = cnt;} 
    }

    // Si no se puede o el número es mayor a 100 regresamos -1 si no 
    // se regresa la cuenta. 
    cout << name  << " " << ((min <= 100) ? min : -1) << endl;  
  } 

  return 0;
} 
