//  Ricardo Nieto Fuentes nifr91@gmail.com
//  
//  ============================================================================
//  
//  Request for Proposal --- 10141
//  
//  When government, military, or commercial agencies wish to make a major
//  purchase, they first issue a Request for Proposal (RFP) which lists a
//  number of requirements that must be met by a successful proposal. Competing
//  suppliers issue Proposals, indicating which of the requirements are met,
//  and a price that will be charged should the proposal be accepted by the
//  agency issuing the RFP. 
//  
//  Because the agencies are staffed by bureaucrats and are accountable to the
//  other agencies staffed by bureaucrats, it is necessary to remove all human
//  judgment from the selection process. To this end, those evaluating the
//  proposals are given features sheets, which have one column for each
//  requirement and an additional column for price, and one row for each
//  Proposal. The evaluator reads each proposal and identifies each requirement
//  that is met; for each such requirement a check mark is placed in the
//  corresponding row (proposal) and column (requirement). After al, proposals
//  have been evaluated, the number of check marks in each row is added. Any
//  proposal that has the same number of chec marks as the number of
//  requirements is said to be compliant; otherwise the proposal is said to be
//  partially compliant. Many agencies award the contract to the lowest
//  compliant proposal; that is the compliant proposal with the lowest price.
//  If there is no compliant proposal, many agencies evaluate partial
//  compliance according to the following formula: 
//  
//  | compliance = number of requirements met / number of requirements 
//  
//  Your job is to select the Proposal with the highest compliance; if several
//  proposals have the same compliance you are to select from these proposals
//  the one with the lowest price. If several proposals have the same
//  compliance and price you are to select the first one in the input. 
//  
//  Input --------------------------------------------------------------------
//  
//  Your input will consist of the information for a number of RFPs and
//  associated proposals. The information for each RFP will consist of: 
//  
//  * a line containing two integers 0 < n <= 1_000, the number of
//    requirements, and 'p' the number of proposals. The line 0 0 indicates 
//    there are no more RFPs.  
//
//  * 'n' lines naming the requirements. Each requirement is a string up to 
//    80 characters long, terminated by the end of line. All strings are 
//    case sensitive.
//
//  * for each of the p proposals: 
//    
//    - a line naming the proposal (up to 80 characters terminated by end of 
//      line). 
//    - a line containing a floating point number 'd' and an integer 
//      0 <= r <= n; d is the price; r is the number of met requirement lines 
//      to follow. 
//    - for each met requirement, the name of the requirement, each on a 
//      separate line. All requirements are from the RFP requirement list, and
//      no requirements are duplicated. 
//
//  Output -------------------------------------------------------------------
//  
//  For each RFP, give the number of the RFP (see sample) followed by the name
//  of the best proposal, optimizing the criteria given. Leave a blank line
//  between the output for each pair of RFPs. 
//
//  ===========================================================================
//
//  Sample input
//
//  |6 4
//  |engine
//  |brakes
//  |tires
//  |ashtray
//  |vinyl roof
//  |trip computer
//  |Chevrolet
//  |20000.00 3
//  |engine
//  |tires
//  |brakes
//  |Cadillac
//  |7000.00 4
//  |ashtray
//  |vinyl roof
//  |trip computer
//  |engine
//  |Hyundai
//  |10000.00 3
//  |engine
//  |tires
//  |ashtray
//  |Lada
//  |6000.00 1
//  |tires
//  |1 1
//  |coffee
//  |Starbucks
//  |1.50 1
//  |coffee
//  |0 0
//
//  Sample Output
//
//  |RFP #1
//  |Cadillac
//  |
//  |RFP #2
//  |Starbucks
//
//  ==========================================================================

#include <iostream>
using namespace std;

int main(void){ 
  
  // número de requerimientos y de propuestas a recibir
  int num_requirements;
  int num_proposals;
 
  // Mientras se lea de stdin 
  for(int cnt = 0; cin >> num_requirements >> num_proposals; cnt+=1){ 
    cin.ignore();

    // Si el número de requerimientos es 0 significa que se ha terminado de
    // recibir casos 
    if (num_requirements == 0){ break;}  
    
    // Solo se agrega un salto de línea entre casos
    if(cnt != 0){ cout << endl; }
   
    // Leer las líneas 
    string dummy; 
    for(int i=0; i < num_requirements; i+=1){ getline(cin,dummy);} 

    // Valores iniciales de la mejor propuesta
    string best_proposal; 
    double best_proposal_cost = 1e200; 
    double best_proposal_compliance = -1e200; 

    // Para cada propuesta 
    for(int i=0; i < num_proposals; i+= 1){
      
      // Valores de la propuesta actual 
      string proposal_name; 
      double proposal_cost; 
      int    proposal_num_requirements; 

      // Leer los valores de cada propuesta 
      getline(cin,proposal_name);
      cin >> proposal_cost; 
      cin >> proposal_num_requirements;
      cin.ignore(); 

      // Leer los requerimientos
      for(int k=0; k < proposal_num_requirements; k+=1){ getline(cin,dummy);} 
     
      // Calculamos que tan buena es la propuesta
      double proposal_compliance = (double)proposal_num_requirements / num_requirements; 

      // Si la propuesta es mejor la aceptamos, si es igual la cambiamos 
      // si mejora el precio, como nos piden la primera ocurrencia basta con 
      // comprobar estos dos casos
      bool is_better      = (proposal_compliance > best_proposal_compliance);
      bool has_lower_cost = (proposal_cost < best_proposal_cost);
      bool is_equal       = (proposal_compliance == best_proposal_compliance);
      if( is_better || (is_equal && has_lower_cost)){ 
        best_proposal = proposal_name;
        best_proposal_cost = proposal_cost;
        best_proposal_compliance = proposal_compliance; 
      } 
    }

    // Imprimir
    cout << "RFP #" << (cnt+1)  << endl; 
    cout << best_proposal << endl; 
  } 

  return(0); 
} 
