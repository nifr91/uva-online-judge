// Ricardo Nieto Fuentes
// nifr91@gmail.com
//
// =======================================================================
//
// Finding words  --- 892 
// 3.000 seconds 
//
// A common problem when processing incoming text is to isolate the words
// in the text. This is made more difficult by the punctuation; words 
// have commas, "quote marks", (even brackets) next to them,
// or hy-phens in the middle of the word. This punctuation doesnt count as
// letters when the words have to be looked up in a dictionary by the 
// program. 
//
// For this problem you must separate out "clean" words from text, that 
// is words with no attached or embedded non-letters. A "word" is any 
// continuous string of non-whitespace characters, with whitespace 
// characters on each side of it. For this problem "whitespace" character
// is a space character or an end of line character, or the start or end
// of the file (so that, for example, if the input file consist of 
// 'Anne Bob', where  there is a space character between the A and B
// but no other, then there are two words 'Anne' and 'Bob').
//
// Input: 
//
// Input will consist of lines with no more than 60 characters in each 
// line. Every line will be terminated by a character which is not 
// whitespace(which will be followed immediately by and end-of-line 
// character). The input will be terminated by a line consisting of a 
// single '#'.
//
// Output: 
//
// Output must be the lines of the incoming tesxt, with the non-letters,
// stripped away from each word. A non-letter is any character which is 
// not a letter (a-z and A-Z) and not a whitespace character. Your program
// must not change the letters and space characters (algouhg space
// characters at the end of lines will be ignored by the judges). When
// a non-letter occurs in the middle of a word(ie there is no whitespace
// character next to it), it must be simply removed ( see what happens to 
// the word 'deosn't' in the example). A word which consists entirely of 
// non-letters will therefore removed entirely. 
//
// There is a special rule for hyphen ('-') when is the very last character
// in a line: 
//
// * the word part before the hyphen and the first word part on the next 
// line form a single word; 
// * this complete word must be written on a line by itself;
// * you can assume that there will always be a space before the word part
// on the first line, and a space after the word part on the second line.
//
// ========================================================================
//
// Sample Input:
//
// |A common problem when processing incoming text is to isolate
// |the words in the text. This is made more difficult by the 
// |punctuation; words have commas, "quote marks",
// |(even brackets)    next to them, or hy-
// |phens in the middle of the word.  This punctuation doesn't
// |count as letters when the words have to be looked up in a 
// |# dictionary by the 12345 "**&! program.
// |#
//
// Sample Output: 
//
// |A common problem when processing incoming text is to isolate
// |the words in the text This is made more difficult by the 
// |punctuation words have commas quote marks
// |even brackets    next to them or 
// |hy
// | in the middle of the word  This punctuation doesnt
// |count as letters when the words have to be looked up in a 
// | dictionary by the   program
//
//=========================================================================

// Solución ----------------------------------------------------------------
//
// Implementar de forma directa las reglas 
//
// -------------------------------------------------------------------------


#include<iostream>
#include<sstream>
#include<ctype.h>

using std::cin; 
using std::cout; 
using std::endl; 
using std::endl; 
using std::string;
using std::stringstream;


// ===========================================================================

int main(int num_args, char ** args){ 

  // Leer el texto 
  string line = ""; 
  while(getline(cin,line)){ 
    // Fin del texto 
    if(line == "#"){ break; } 

    // Leer las palabras de cada linea 
    stringstream ss(line); 
    string word = ""; 
    int pos = 0; 
    while(ss >> word){

      // Contar los espacios ignorados
      int spaces = 0; 
      while(line[pos] == ' '){
        spaces += 1;
        pos+=1;
      }
      // Concatenar los caracteres que conforman una palabra
      string trim = ""; 
      for(int i=0; i < (int)word.size(); i+=1){ 
        if(isalpha(word[i])){trim += word[i];} 
      } 
      pos += (word.size());

      // imprimir los espacios 
      for(int i=0; i < spaces; i+=1){cout << ' ';} 

      // manejar el caso especial de un hypen
      if(word[(int)word.size()-1] == '-'){ 
        getline(cin,line);
        ss = stringstream(line); 
        ss >> word;
        pos = 0;
        for(int i=0; i < (int)word.size(); i+=1){ 
          if(isalpha(word[i])){trim += word[i];} 
        } 
        pos += (word.size());    
        cout << endl << trim << endl; 
      }
      // Imprimir la palabra 
      else{ 
        cout << trim;  
      } 
    } 
    cout << endl; 
  } 
  return 0; 
} 
