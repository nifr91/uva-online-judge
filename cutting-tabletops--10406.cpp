// Ricardo Nieto Fuentes
// nifr91@gmail.com 
//
// ============================================================================
//
// Cutting Tabletops --- 10406
// 3.000 seconds 
//
// Bever Lumber hires beavers to cut wood. The company has recently received
// a shipment of tabletops. Each tabletop is a convex polygon. However, in this
// hard economic times of cutting costs the company has ordered the tabletops
// from a not very respectable but cheap supplier. Some of the tabletops have
// the right shape but they are slightly too big. The beavers have to chomp 
// of a strop of wood of a fixed with from each edge of the tabletop such that 
// they get a tabletop of a similar shape but smaller. Your task is to find the
// area of the tabletop after beavers are done. 
//
// Input: 
//
// Input consists of a number of cases each presented on a separate line. Each
// line consists of a sequence of numbers. The first number is 'd' the width of
// the strop of wood to be cut off of each edge of the tabletop in centimeters.
// The next number 'n' is an integer giving the number of vertices of the
// polygon. The next 'n' pairs of numbers present xi and yi coordinates of
// polygon vertices for 1 <= i <= n given in clockwise order.  A line
// containing only two zeroes terminate the input.  'd' is much smaller than
// any of the sides of the polygon. The beavers cut the edges one after another
// and after each cut the number of vertices of the tabletop is the same.
// 
//
// Output: 
//
// For each line of input produce one line of output containing one number of
// three decimals digits in the fraction giving the area of the tabletop after
// cutting.
// 
// ============================================================================
// 
// Sample Input: 
//
// |2 4 0 0 0 5 5 5 5 0
// |1 3 0 0 0 5 5 0
// |1 3 0 0 4 5.1961524 6 0
// |3 4 0 -10 -10 0 0 10 10 0
// |0 0
//
// Sample Output: 
//
// |1.000
// |1.257
// |2.785
// |66.294
//
// ============================================================================


// Solución  ------------------------------------------------------------------
//
// Emplear los vértices y la distancia d para encontrar los nuevos vértices 
// mediante las intersecciones de las aristas del nuevo polígono. Una vez 
// se tienen los vértices del nuevo polígono calcular el área.
//
// ----------------------------------------------------------------------------

#include<iostream>
#include<iomanip>
#include<cmath>
using std::cin;
using std::cout;
using std::endl; 
using std::min;
using std::setprecision;
using std::fixed;

// Estructura que representa un punto en el plano cartesiano como un vector
// del origen al punto con coordenadas x,y.
struct point {
  double x = 0; 
  double y = 0; 

  // Constructores -----------------------------------------------------------
  point(){}
  point(double _x,double _y):x(_x),y(_y){}
  point(const point &pt):x(pt.x),y(pt.y){}

  // Funciones ---------------------------------------------------------------

  point operator+(point o){ return point(this->x + o.x, this->y + o.y); }
  point operator-(){return point(-this->x,-this->y);}
  point operator-(point o){return (*this + -o);} 
  point operator/(double scalar){return point(this->x/scalar,this->y/scalar);} 

  // Producto cruz entre dos vectores en el plano, su signo indica el 
  // sentido del ángulo entre el punto y un segundo punto 'o'. En sentido 
  // horario (< 0) y en sentido anti-horario (> 0)
  double cross(point o){return ((this->x*o.y) - (this->y * o.x));} 

  // Magnitud del vector 
  double norm(){ return (this->x*this->x)+(this->y*this->y); } 
} ; 

// Obtener el área del polígono
double polygon_area(int n, point * poly) {
  double area = 0.0;
  for (int i = 0; i < n; i++)
    area += 
      poly[i].x * poly[(i + 1) % n].y - poly[(i + 1) % n].x * poly[i].y;
  return area / 2.0;
}

// Obtener los puntos de corte 
void get_cut_points(double d, point& p,point& q, point& cp, point& cq){
  double x = p.y - q.y; 
  double y = q.x - p.x; 
  double t = d / sqrt((p-q).norm());
  cp.x = p.x + x * t; 
  cp.y = p.y + y * t;
  cq.x = q.x + x * t; 
  cq.y = q.y + y * t;
}

// Calcular las intersecciones 
point get_intersect_points(point& p1, point& p2, point& q1, point& q2){
  double hq1 = (p2 - p1).cross(q1 - p1); 
  double hq2 = (p1 - p2).cross(q2 - p2); 
  double hq = hq1 + hq2;
  return point((q1.x * hq2 + q2.x * hq1) / hq, (q1.y * hq2 + q2.y * hq1) / hq);
}

// Driver =====================================================================

int main(int num_args, char ** args) {

  // Leer la distancia y el número de vertices
  double d = 0; 
  int    n = 0; 
  while (cin >> d >> n) {
    // Finalizar los casos de prueba
    if (d == 0 && n == 0){break;} 

    // Leer los vértices 
    point * points = new point[n]; 
    for (int i = n - 1; i >= 0; i--){ cin >> points[i].x >> points[i].y;} 
    
    // Obtener las aristas 
    point * cut_points = new point[n<<1]; 
    for (int i = 0; i < n; i++){ 
      get_cut_points(d, 
        points[i], 
        points[(i + 1) % n], 
        cut_points[i * 2], 
        cut_points[i * 2 + 1]);
    } 

    // Obtener los nuevos vértices 
    for (int i = 0; i < n; i++){ 
      points[i] = get_intersect_points(
        cut_points[i * 2], 
        cut_points[i * 2 + 1],
        cut_points[(i * 2 + 2) % (n * 2)], 
        cut_points[(i * 2 + 3) % (n * 2)]);
    } 

    // Calcular el área y mostrarla en pantalla.
    cout << setprecision(3) << fixed <<  polygon_area(n,points) << endl; 
  }

  return 0;
}
