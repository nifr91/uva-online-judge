// ricardo nieto fuentes
// nifr91@gmail.com 
//
// ======================================================================
//
// bars --- 12455
// 1.000 seconds
//
// Some things grow if you put them together. 
//
// we have some metallic bars, theirs length known, and if necessary, we want
// to solder some of them in order to obtain another being exactly a given 
// length long. no bar can be cut up. is it possible?
//
// input: 
//
// the first line of the input contains an integer 't' (0 <= t <= 50), 
// indicating the number of test cases. for each test case, three lines appear, 
// the first one contains a number '0 <= n <= 1000', representing the 
// length of the var we want to obtain. the second line contains a number 
// '1 <= p <= 20', representing the number of bars we have. the third line of 
// each test case contains 'p' numbers, representing the length of the 
// 'p' bars. 
//
// output : 
//
// for each test case the output should contain a single line, consists of the
// string 'yes' or the string 'no' depending on whether solution is possible or
// not. 
// ======================================================================
//
// sample input 
//
// |4
// |25
// |4
// |10 12 5 7
// |925
// |10
// |45 15 120 500 235 58 6 12 175 70
// |120
// |5
// |25 25 25 25 25
// |0
// |2
// |13 567
//
// sample output
//
// |no
// |yes
// |no
// |yes
//
// ======================================================================

#include<iostream> 
using namespace std; 

// solution  ------------------------------------------------------------------
//
// Enumerar las combinaciones de las barras y verificar si se cumple la 
// restricción de longitud
//
// ----------------------------------------------------------------------

int main(int argc, char ** argv){ 
  
  int test_cases = 0; 
  cin >> test_cases; 

  for(int t=0; t < test_cases; t+=1){ 
   
    // Leer los datos de entrada 
    int len = 0; 
    cin >> len;
    int num_bars = 0; 
    cin >> num_bars; 
    int bars[num_bars] = {}; 
    for(int i=0; i < num_bars; i+=1){ cin >> bars[i]; }
  
    // Para cada combinación 
    int yes = false; 
    for(int comb = 0; (comb < (1 << (num_bars+1))) && !yes; comb+=1){ 
      // Sumar la longitud de la barra si se utiliza
      int l = 0; 
      for(int i = 0; (i < num_bars) && !yes; i+=1){ 
        if((comb & (1 << i))){ l += bars[i]; } 
      }
      // Si se a encontrado una solución se termina de iterar
      if (l == len){ yes = true; } 
    }
    cout << (yes ? "YES" : "NO") << endl; 
  } 
  

  return 0; 
} 
