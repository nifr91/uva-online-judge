// Ricardo Nieto Fuentes
// nifr91@gmail.com
//
// ============================================================================
//
// All Integer Average --- 12060
//
// When we need to find the arithmetic average of some numbers we always tend
// to keep the result in a floating-point number as in many cases the average
// is a fraction. But if all the numbers are integers then we should not use 
// floating-point numbers to store the average, as floating-point numbers can
// lead to all sorts of precision errors. In this problem your job is to find
// the average of some integers and express it in the form a or b/c or a(b/c), 
// where 'a' , 'b', and 'c' are all integers. In this problem we will denote
// this average as 'avg'. 
//
// a) If |avg| > 1 and a fractional number then we should print it as a(b/c)
// b) If |avg| < 1 and a fractional number then we should print it as b/c
// c) If avg is a integer we should print it as 'a'. 
// d) If avg is negative the whole result should be preceded by a minus (-) 
//    sign and a space. 
// e) In the printed fractional part b and c should be relative prime. In 
//    other words gcd(b,c) should be 1. 
// f) The horizontal bar in the fraction should be performed using only the 
//    hyphen -. Its length shoyld be equal to the number of digits in c. 
//    if the number of digits in 'b' is less than 'c' then 'b' shoyld be 
//    right justified on the horizontal bar. 
//
// Input 
//
// The input file contains less than 101 sets of inputs. Each set of input 
// is given in a single line. The description of each line is given as below: 
// 
// The first integer 'n' (1 <= n <= 100) of a line denotes how many numbers are
// to be averaged. It is followed by 'n' numbers, all of which have absolute 
// value less than 10_000. 
// Input is terminated by a case where n = 0. This case should not be processed.
//
// Output 
//
// For each line of input produce two or four lines of outputs. The first line
// contains the serial number of the output. The next one or three lines
// contain the value of the average following the rules specified above. The
// output lines should not contain any trailing spaces. And there must not be
// any leading or trailing spaces other than the ones that are required for
// formatting. 
//
// ============================================================================
// 
// Sample Input 
// 
// |3 1 2 3
// |3 -1 -2 -3
// |3 1 2 4
// |4 2 4 6 10
// |3 -1 -2 -4
// |10 1 1 1 1 1 1 1 1 1 4
// |10 1 -1 1 -1 1 -1 -1 1 1 1
// |10 1 -1 1 -1 1 -1 -1 1 1 -3
// |0
// 
// Sample Output 
//
// |Case 1:
// |2
// |Case 2:
// |- 2
// |Case 3:
// | 1
// |2-
// | 3
// |Case 4: 
// | 1
// |5-
// | 2
// |Case 5:
// |   1
// |- 2-
// |   3
// |Case 6:
// |  3
// |1--
// | 10
// |Case 7:
// |1
// |-
// |5
// |Case 8:
// |  1
// |- -
// |  5
// 
// ============================================================================
//

#include<iostream> 
#include<iomanip>
using namespace std; 

// Función para calcular el GCD
int gcd(int u , int v) { 
  while (v != 0){ 
    auto r = u % v; 
    u = v; 
    v = r; 
  } 
  return u; 
} 

int main (int argc, char ** argv){ 

  // Vector para almacenar los datos y la cantidad de datos
  int vals[100]; 
  int n; 

  // Contador del número de caso 
  int cnt = 1; 
  while(true){ 

    // Leer los datos
    cin >> n; 
    if(!n){ break; } 
    for(int i=0; i < n; i+=1){ cin >> vals[i];}

    // Obtener el promedio 
    int sum = 0; 
    for(int i=0; i < n; i+=1){sum += vals[i];}

    // Obtener la razón y el residuo 
    int q = sum / n; 
    int r = sum % n;
    // Calcular el gcd entre el residuo y el divisor
    auto g = gcd(n,r);

    // Obtener los valores a,b,c
    int a = q; 
    int b = r / g; 
    int c = n / g; 

    // Convertir los valores a string 
    string stra = to_string(abs(a)); 
    string strb = to_string(abs(b)); 
    string strc = to_string(abs(c)); 

    // Símbolo de división
    string strdiv  =""; 
    for(int i=0; i < (int)strc.size(); i+=1){strdiv += "-"; } 
   
    // Obtener el tamaño de la justificación para el numerador y denominador
    int width = 0;
    if (sum < 0){width += 2;}
    if (a != 0){ width += stra.size();}
    if ((b!=0)){ width += strc.size();} 

    // Imprimir resultado 
    cout << "Case " << cnt << ":" << endl; 
    if (b!=0){ cout << setw(width) << strb << endl; } 
    cout << ((sum < 0) ? "- " : "") 
      << (((a != 0) || (b==0)) ? stra : "") 
      << ((b!= 0) ? strdiv : "") 
      << endl;
    if (b!=0){ cout << setw(width) << strc << endl; } 

    cnt+=1;
  } 
} 
