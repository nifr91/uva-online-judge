// Ricardo Nieto Fuentes
// nifr91@gmail.com 
//
// ============================================================================
// Amazing --- 556 
// 
// One of the apparently intelligent tricks that enthousiastic psychologists
// persuade mice to perform is solving a maze. There is still some controversy
// as to the exact strategies employed by the mice when engaged in such a task,
// but it has been claimed that the animal keepers eavesdropping on
// conversations between the mice have heard them say things like “I have
// finally got Dr.  Schmidt trained. Everytime I get through the maze he gives
// me food”.  
// 
// Thus when autonomous robots were first being built, it was decided that
// solving such mazes would be a good test of the ’intelligence’ built into
// such machines by their designers. However, to their chagrin, the first
// contest was won by a robot that placed a sensor on the right-hand wall of
// the maze and sped through the maze maintaining contact with the right-hand
// wall at all times. This led to a change in the design of mazes, and also to
// the interest in the behaviour of such robots. To test this behaviour the
// mazes were modified to become closed boxes with internal walls. The robot
// was placed in the south west corner and set of pointing east. The robot then
// moved through the maze, keeping a wall on its right at all times. If it can
// not proceed, it will turn left until it can proceed. All turns are exact
// right angles. The robot stops when it returns to the starting square. The
// mazes were always set up so that the robot could move to at least one other
// square before returning. The researchers then determined how many squares
// were not visited and how many were visited one, twice, thrice and four
// times. A square is visited if a robot moves into and out of it. Thus for the
// following maze, the values (in order) are: 2, 3, 5, 1, 0.  
//          
//           N [.][#][.][#][.] w [.][#][.][#][.] E [>][.][.][.][.] S # Wall .
//           Free cell
//
// Write a program to simulate the behaviour of such a robot and collect the
// desired values. 
//
// Input: 
//
// Input will consist of a series of maze descriptions. Each maze description
// will start with a line containing the size of the maze (b and w), this will
// be followed by 'b' lines, each consisting of 'w' characters, either '0' or
// '1'. Ones represent closed squares, zeroes represent open squares. Since the
// maze is enclosed, the outer wall is not specified. The file will be
// terminated by a line containing two zeroes. 
//
// Output 
//
// Output will consist of a series of lines, one for each maze. Each line will
// consist of 5 integer values representing the desired values, each value
// right justified in a field of width 3. 
//
// ============================================================================
//
// Sample input 
//
// |3 5 |01010 |01010 |00000 |0 0
//
// Sample Output
//
// |  2  3  5  1  0
//
// ============================================================================
// 
// Notas -------
// 
// La descripción del problema tiene problemas, la idea es que el robot en 
// recorre el laberinto siempre con la mano derecha sobre la pared de forma 
// que en el ejemplo de entrada recorre de la siguiente forma 
//
// [.][#][.][#][.]    [.][#][.][#][.]    [.][#][.][#][.]    [.][#][.][#][.]    
// [.][#][.][#][.] => [.][#][.][#][.] => [.][#][.][#][.] => [.][#][.][#][.] => 
// [>][.][.][.][.]    [1][>][.][.][.]    [1][1][>][.][.]    [1][1][1][>][.]    
// 
// [.][#][.][#][.]    [.][#][.][#][.]    [.][#][.][#][^]    [.][#][.][#][1]    
// [.][#][.][#][.] => [.][#][.][#][^] => [.][#][.][#][1] => [.][#][.][#][v] => 
// [1][1][1][1][^]    [1][1][1][1][1]    [1][1][1][1][1]    [1][1][1][1][1]    
//
// [.][#][.][#][1]    [.][#][.][#][1]    [.][#][.][#][1]    [.][#][.][#][1]    
// [.][#][.][#][2] => [.][#][.][#][2] => [.][#][.][#][2] => [.][#][^][#][2] => 
// [1][1][1][1][<]    [1][1][1][<][2]    [1][1][^][2][2]    [1][1][2][2][2]    
//
// [.][#][v][#][1]    [.][#][1][#][1]    [.][#][1][#][1]    [.][#][1][#][1]    
// [.][#][1][#][2] => [.][#][v][#][2] => [.][#][2][#][2] => [.][#][2][#][2] => 
// [1][1][2][2][2]    [1][1][2][2][2]    [1][1][<][2][2]    [1][<][3][2][2]    
//                                                          
// [.][#][1][#][1]
// [.][#][2][#][2]
// [1][2][3][2][2]
//
// De esta forma contamos el número de casillas visitadas 
// 
// 0 1 2 3 4 | veces 
// ----------+---------
// 2 3 5 1 0 | casillas
// ----------+---------

#include<iostream> 
#include<iomanip>
using namespace std; 

// Estructura que almacena la dirección en que se mueve el robot y su posición
struct roboto{ 
  int r; 
  int c;
  char dir = 'e'; 
  roboto(int _r, int _c):r(_r),c(_c){} 
  bool eq(roboto other){ return ((this->r == other.r) && (this->c == other.c));} 
};

// Movemos el robot
void move_robot(roboto * robot, int ** mat){

  // Posición del robot 
  int r = robot->r; 
  int c = robot->c; 
  roboto npos(r,c); 

  // Si no existe una pared a nuestra derecha, giramos a la derecha 
  if      ((robot->dir == 'n') && (mat[r][c+1] != -1)){robot->dir = 'e';}
  else if ((robot->dir == 's') && (mat[r][c-1] != -1)){robot->dir = 'w';} 
  else if ((robot->dir == 'e') && (mat[r+1][c] != -1)){robot->dir = 's';}
  else if ((robot->dir == 'w') && (mat[r-1][c] != -1)){robot->dir = 'n';} 

  // Obtenemos la posición que está al frente
  if      (robot->dir == 'n'){ npos = roboto(r-1,c); } 
  else if (robot->dir == 's'){ npos = roboto(r+1,c); } 
  else if (robot->dir == 'e'){ npos = roboto(r,c+1); } 
  else if (robot->dir == 'w'){ npos = roboto(r,c-1); } 

  // Si no podemos continuar (movernos al frente) nos giramos a la derecha
  // de lo contrario (podemos movernos) actualizamos la matriz y la posición 
  // del robot
  if (mat[npos.r][npos.c] == -1){
    if      (robot->dir == 'n'){ robot->dir = 'w';} 
    else if (robot->dir == 'w'){ robot->dir = 's';} 
    else if (robot->dir == 's'){ robot->dir = 'e';} 
    else if (robot->dir == 'e'){ robot->dir = 'n';} 
  }else{ 
    mat[r][c] += 1; 
    robot->r = npos.r; 
    robot->c = npos.c;
  } 
}

void print_matrix(roboto robot,int ** mat,int nr, int nc){ 
    for(int r=0; r < nr; r+=1){ 
      for(int c=0; c < nc; c+=1){ 
        if (mat[r][c] == -1){ cout << '#'; }
        else if ( (robot.r == r) && (robot.c == c)){
          if(robot.dir == 'n'){ cout << "^"; } 
          if(robot.dir == 's'){ cout << "v"; } 
          if(robot.dir == 'e'){ cout << ">"; } 
          if(robot.dir == 'w'){ cout << "<"; }
        }else { cout << mat[r][c] ; } 
      } 
      cout << endl; 
    } 
    cout << endl; 
} 

int main(int argc, char ** argv){ 

  int nr, nc; 
  // Leer los casos de prueba 
  while (cin >> nr >> nc){ 
    if(!nr && !nc){ break; } 
    nr += 2; 
    nc += 2; 

    // Inicializar la matriz, para simplificar las comparaciones, se 
    // extiende la matriz hacia los bordes. 
    int ** mat = new int*[nr];
    for(int r=0; r < nr; r+=1){ 
      mat[r] = new int[nc]; 
      for(int c=0; c < nc; c+=1){mat[r][c] = -1;}
    } 

    // Leer los valores en la matriz 
    for(int r=1; r < nr-1; r+=1){ 
      char chr; 
      for(int c=1; c < nc-1; c+=1){ 
        cin >> chr; 
        if (chr == '0'){ mat[r][c] = 0;} 
      } 
    } 
    // Iniciar en la esquina inferior izquierda
    auto start = roboto(nr-2,1);
    auto robot = roboto(start.r,start.c); 
 
    // Mover el robot hasta regresar a la meta  
    move_robot(&robot, mat);
    while(!robot.eq(start)){ move_robot(&robot,mat); }

    
    // Recorrer la matriz para almacenar la cuenta
    int values[5] = {0,0,0,0,0}; 
    for(int r=1; r < nr-1; r+=1){ 
      for(int c=1; c < nc-1; c+=1){ 
        if (mat[r][c] == -1){ continue;}
        values[mat[r][c]] +=1; 
      } 
    } 



    // Imprimir los valores
    for (int i =0; i < 5; i+=1){cout << setw(3) << values[i];} 
    cout << endl; 
  } 
  return(0); 
} 
