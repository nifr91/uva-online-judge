// Ricardo Nieto Fuentes
// nifr91@gmail.com 
//
// ============================================================================
//
// Exact Sum --- 11057
// 3.000 seconds 
//
// Peter received money from his parents this week and wants to spend it all 
// buying books. But he does not read a book so fast, because he likes to 
// enjoy every single word while he is reading. In this way, it takes him 
// a week to finish a book. 
//
// As peter receives money every two weeks, he decided to buy two books, then 
// he can read them until receive more money. As he whishes to spend all the 
// money, he should choose two books whose prices summed up are equal to the 
// money that he has. It is a little bit  difficult to find these books, so 
// Peter asks your help to find them. 
//
// Input 
//
// Each test case starts with 2 <= M <= 10_000, the number of available books. 
// Next line will have 'N' integers, representing the price of each book, 
// a book costs less than 1000001. Then there is another line with an integer 
// 'M', representing how much money Peter has. There is a blank line 
// after each test case. The input is terminated by end of file (EOF). 
//
// Output
//
// For each test case you must print the message: 'Peter should buy books whose 
// prices are i and j.' where 'i' and 'j' are the prices of the books whose sum
// is equal to 'M' and 'i' <= 'j'. You can consider that is always possible to 
// find a solution, if there are multiple solutions print the solution that 
// minimizes the difference between the prices 'i' and 'j'. After each test 
// case you must print a blank line. 
//
// ============================================================================
//
// Sample Input
//
// |2
// |40 40
// |80
// |
// |5
// |10 2 6 8 4
// |10
//
// Sample Output 
//
// |Peter should buy books whose prices are 40 and 40
// |
// |Peter should buy books whose prices are 4 and 6
// |
// 
// ============================================================================

#include<iostream> 
#include<algorithm>
using namespace std; 

// =============================================================================
// Solución propuesta 
//
// Probar para cada posible valor de i y usar búsqueda binaria para encontrar
// el menor valor de j que cumple con la suma. 
// =============================================================================


// Búsqueda binaria en el rango i..n-1
//
int binsearch(int * books,int n,int i,int money){ 
  int l = i+1; 
  int r = n-1;

  while(l < r){ 
    int m = (l + r) / 2;
    if((books[i] + books[m]) < money){ l = m+1;}
    else{r = m;} 
  } 
  return l; 
} 

struct ijpair{ 
  int i = 0; 
  int j = 0; 
  ijpair(int _i,int _j):i(_i),j(_j){}
};

// Driver =====================================================================

int main(int argc, char ** argv){ 
  int n; 
  while (cin >> n){ 
    
    // Leer los datos 
    int books[10000];
    for(int i=0; i < n; i+=1){ 
      cin >> books[i]; 
    } 
    int money; 
    cin >> money;

    // Ordenar los datos
    sort(books+0,books+n); 

    auto ij = ijpair(0,0); 
    int  val =  1<<30; 

    // Probar para cada valor 
    for(int i=0; i < n-1; i+=1){ 
      int j = binsearch(books,n,i,money);
      int diff = books[j] - books[i] ;
      bool valid =  (money ==  (books[j]+books[i])); 
      if (valid && (diff < val)){ 
        ij = ijpair(i,j);
        val = diff; 
      } 
    } 

    // Imprimir resultado 
    cout << "Peter should buy books whose prices are " 
      << books[ij.i]
      << " and " 
      << books[ij.j]
      << "." 
      << endl 
      << endl; 
  } 

  return 0; 
} 

