// Ricardo Nieto Fuentes
// nifr91@gmail.com 
//
// ============================================================================
//
// Nature --- 10685
// 3.000 seconds 
//
// In nature, there are alimentary chains. At the basis of this chain, we 
// generally have the vegetals. Small animals eat those vegetals and bigger 
// animals eat the smaller. There can be cycles in the chain, as 
// when some anima dies he starts a decomposition process which will transform
// its body into minerals that are a source of energy for the vegetals. 
//
// In this problem you will have to find the largest alimentary chain for a 
// given group of creatures. You can consider that if A is a predator of B
// then they are in the same chain. 
//
// Input: 
//
// The input file contains several input sets. The description of each set is 
// given below: 
//
// Each set starts with two integers 'C' (1 <= C <= 5_000), the number of
// creatures and 'R' (0 <= R <= 5_000) the number of relations. Follow C
// lines with the names of the creatures, each consisting of lower case 
// letters (a,b,...,z). No name is longer than 30 letters. Then there will be
// 'R' lines describing the relations. Each line will have 2 names of creatures
// meaning that the second creature is a predator if the first one. 
//
// You can assume that no creatures is a predator of himself. 
//
// Input is terminated by a set where C=R=0. This set should not be processed. 
// There is a blank line between two input sets. 
//
// Output: 
//
// For each input set produce one line of output, the size of the largest
// alimentary chain. 
//
// ============================================================================
//
// Sample Input: 
//
// |5 2
// |caterpillar
// |bird
// |horse
// |elefant
// |herb
// |herb caterpillar
// |caterpillar bird
// |
// |0 0
//
// Sample Output: 
// 
// |3
//
// ============================================================================

// Solución -------------------------------------------------------------------
//
// Implementar la estructura union-disjoin-set que nos permite unir conjuntos
// de forma eficiente. Y responder de forma eficiente si dos elementos
// pertenecen al mismo conjunto, así como el número de elementos en el
// conjunto. 
//
// Una vez implementado el uds , es suficiente con leer todas las 
// entradas, e ir uniendo los animales conforme a sus relaciones. 
//
// Se muestra el tamaño del conjunto con más elementos 
// ----------------------------------------------------------------------------

#include<iostream>

// Mapa K => V
template <class K, class V> 
struct nmap{ 

  // Estructura de nodo 
  struct node{ 
    int id; K k; V v; node * l; node * r; node * p;
    node(K _k,V _v,node * _l = nullptr, node *_r = nullptr, node * _p = nullptr):
      k(_k),v(_v),l(_l),r(_r),p(_p){ id = std::rand(); }
  };

  // Estructura para regresar dos nodos en el mapa
  struct node_pair{ 
    node * l; node * r; node_pair(node * _l, node * _r):l(_l),r(_r){}
  };


  // Raíz 
  node * root = nullptr; 
  int size = 0; 

  // Dividir el árbol por un valor ( los valores < o <= en un árbol 
  // (l) y los demás en otro (r). Ambos árboles mantienen la propiedad del 
  // treap
  node_pair split(node * root, K & key, bool leq = true){
    node * tl = nullptr; 
    node * tr = nullptr; 
    if(!root){ return node_pair(tl,tr);} 
    bool comp = (leq) ? (key <= root->k) : (key < root->k); 
    if (comp){
      tr = root; 
      auto tpair = split(tr->l,key,leq); 
      tr->l = tpair.r;
      tl = tpair.l;
    }
    else{ 
      tl = root;
      auto tpair = split(tl->r,key,leq); 
      tl->r = tpair.l;
      tr = tpair.r;
    } 
    return node_pair(tl,tr);
  } 

  // Combinar dos árboles (l y r) que tienen la propiedad del treap en 
  // un árbol, se cumple que para el árbol l los valores son < que los 
  // valores r
  node * merge(node * tl, node * tr){ 
    if(!tl || !tr){ return ((tl) ? tl : tr);}
    if(tl->id > tr->id){ 
      tl->r = merge(tl->r,tr);
      return tl; 
    } 
    else { 
      tr->l = merge(tl,tr->l); 
      return tr; 
    } 
  }

  // Encontrar en O(lg n) un valor en el árbol 
  node * find(K & key,node * root){ 
    if(!root || (root->k == key)){return root;} 
    if(key < root->k){return find(key,root->l); } 
    return find(key,root->r);
  } 

  // Agregar un elemento al árbol  en O(lg n)
  void insert(K key, V val){ 
    node * n = new node(key,val);
    auto npair = split(this->root,key); 
    auto l = npair.l;
    auto m = npair.r;
    npair = split(m,key,false);
    m = npair.l; 
    auto r = npair.r;
    this->root = merge(merge(l,n),r);
    this->size += 1; 
  } 

  // Eliminar un elemento al árbol  en O(lg n)
  void remove(K key){ 
    auto npair = split(this->root,key); 
    auto l = npair.l; 
    auto m = npair.r; 
    npair = split(m,key,false); 
    m = npair.l; 
    auto r = npair.r; 
    this->root = merge(l,r); 
    free_tree(m); 
    this->size -= 1; 
  } 

  // Liberar la memoria de un árbol
  void free_tree(node * n ){ 
    if (n){ 
      free_tree(n->l); 
      free_tree(n->r); 
      delete n; 
    } 
  } 

  // Acceder al elemento con llave key
  V & operator[](K key) {return this->find(key,this->root)->v;} 

};




template <typename T> 
struct ufds{ 

  // Clase nodo, contiene un valor, apuntador al padre, el rango del nodo y 
  // el número de nodos hijos.
  struct node{ 
    T val; 
    node * parent; 
    int cnt; 
    int rnk; 
    node(T val){ 
      this->val = val; 
      this->parent = this; 
      this->cnt = 1; 
      this->rnk = 0; 
    } 
  }; 

  // Utilizamos un mapa para almacenar los nodos de forma eficiente
  nmap<T,node*>kmap; 

  // Insertar un nuevo nodo
  void insert(T val){ 
    node * n = new node(val); 
    this->kmap.insert(val,n); 
  } 
 
  // Regresa true si esta en el conjunto y false si no esta
  bool inset(T val){ return(!!this->kmap.find(val,this->kmap.root)); }

  // Regresa el nodo padre del conjunto (el representante), aplica la 
  // compresión de camino, donde se reduce la altura del árbol a 1.
  node * find(T val){ 
    auto node = this->kmap[val];
    if(node->parent != node){ node->parent = find(node->parent->val);} 
    return (node->parent); 
  } 

  int merge(T v1, T v2){ 
    auto n1 = this->find(v1); 
    auto n2 = this->find(v2);
    
    // Los dos valores pertenecen al mismo conjunto si tienen el mismo 
    // representante.
    if(n1 == n2){ return n1->cnt;} 

    // Si los dos conjuntos tienen el mismo rango se combinan y se aumenta el 
    // rango , si no, se elije el que tenga menor rango como padre.
    node * p = nullptr; 
    node * c = nullptr; 
    if(n1->rnk == n2->rnk){ 
      p = n1; 
      c = n2; 
      n1->rnk += 1;
    }else if (n1->rnk > n2->rnk){ 
      p = n1; 
      c = n2;
    }else{ 
      p = n2; 
      c = n1; 
    } 

    // El hijo (menor rango) tiene com padre al de mayor rango (para 
    // no aumentar la altura). También se actualiza el número de nodos en el 
    // conjunto. 
    c->parent = p; 
    p->cnt += c->cnt; 

    return p->cnt; 
  } 

} ;


int main(int num_args, char ** args){ 

  // Leer la cantidad de animales y relaciones 
  int nanimals = 0; 
  int nrelations = 0; 
  while(std::cin >> nanimals >> nrelations){ 
    if(!nanimals && !nrelations){ break; } 
 
    // Leer los posibles animales  
    ufds<std::string> animals; 
    for(int i=0; i < nanimals; i+=1){ 
      std::string animal = ""; 
      std::cin >> animal; 
      animals.insert(animal); 
    } 

    // Crear las relaciones entre los animales 
    int max_chain = 1; 
    for(int i=0; i < nrelations; i+=1){ 
      std::string prey = ""; 
      std::string predator = ""; 
      std::cin >> prey >> predator; 

      // Almacenar el número máximo entre la cadena generada y el máximo actual
      max_chain = std::max(max_chain,animals.merge(prey,predator));
    } 

    // Desplegar el resultado 
    std::cout << max_chain << std::endl; 
  } 


  return 0; 
} 
