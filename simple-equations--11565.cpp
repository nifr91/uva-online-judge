// Ricardo Nieto Fuentes 
// nifr91@gmail.com
//
// ============================================================================
//
// Simple Equations --- 11565
// 3.000 seconds
//
// Let us look at a boring mathematics problem. 
//
// We have three different integers x,y, and z which satisfy the following 
// three relations: 
//
// * x + y + z = A
// * xyz = B
// * x**2 + y**2 + z**2 = C
//
// You are asked to write a program that solves for x, y and z for given 
// values of A,B and C. 
//
// Input 
//
// The first line of the input file gives the number of test cases 'N' 
// (N < 20). Each of the following 'N' lines gives the values of 'A', 'B'
// and 'C' (1 <= A,B,C <= 10000).
//
// Output: 
//
// For each test case, output the corresponding values of x,y and z if there
// are many possible answers, choose the one with the least value of x. 
// If there is a tie, output the one with the least value of y. If there is no
// solution, output "No solution." instead.
//
// ============================================================================
//
// Sample Input
//
// |2
// |1 2 3
// |6 6 14
// |1 2 3
//
// Sample Output 
//
// |No solution.
// |1 2 3
//
// ===========================================================================

#include<iostream>
using namespace std; 

// ----------------------------------------------------------------------------
// Solución propuesta 
// 
// Búsqueda completa en las variables y que cumplan las tres ecuaciones. 
//
// Optimización 1. 
// 
// Como se pide en orden (las combinaciones) se puede restringir el limite
// inferior como 
//
// |x=-10_000
// |y=x+1
// |z=y+1
//
// Optimización 2. 
// 
// Notar que el límite superior de el límite superior debido a que x < y < z
// de 'y' y de 'z' son : 
//
// |10_000 - (x+y)
// |10_000 - (x+x)
//
// Optimización 3. 
//
// Notar que debido a que x**2 + y**2 + z**2 <= 10_000  se puede limitar 
// a que x <= 100
// 
// Optimización 4. 
//
// Notar que una vez se tienen 'x' e 'y' se puede calcular 'z', con lo que 
// el algoritmo se vuelve O(n**2).
//
// ----------------------------------------------------------------------------

int main(int argc, char ** argv){ 

  // Leer los casos 
  int test_cases; 
  cin >> test_cases;

  for(int t=0; t < test_cases; t+=1){ 
    int a,b,c; 
    cin >> a >> b >> c; 
    bool found = false; 
    for(int x=-100; x < 101 ; x+=1){ 
      for(int y=x+1; y <= (a-x-x) ; y +=1){
        int p = x*y; 
        if (p >= b || p == 0){continue;}
        int  z = b / (x*y);
        if((z > y) && ((x+y+z) == a) && ((x*x + y*y + z*z) == c) && ((x*y*z)==b)){ 
          cout << x << " " << y << " " << z << endl; 
          found = true; 
          break; 
        } 
      } 
      if(found){break;}
    } 
    if(!found){cout << "No solution." << endl;} 
  }
  return 0; 
} 





