// Ricardo Nieto Fuentes 
// nifr91@gmail.com
//
// ============================================================================
//
// Bender B. Rodríguez Problem --- 11507
//
// Bender is a robot built by Mom's Friendly Robot Company at its plant in
// Tijuana, Mexico in 2996. He is a Bending-Unit 22, serial number 2716057 and
// chasis number 1729. He was created for the task of bending metal wires.
//
// Bender needs to bend a wire of length L (L >= 2 ). The wire is represented
// int the Bender's brain ( a MOS Technology 6502 microprocessor) as a line
// stucked in the origin of a tridimensional cartesian coordinate system, and
// exteded along the x positive axis (+x), so that the fixed extreme of the
// wire is in the coordinate (0,0,0) and the free extreme of the wire is in the
// coordinate (L,0,0). 
//
// Bender bends the wire at specific points,starting at the point (L-1,0,0) and
// ending at the point (1,0,0). For each i from L-1 to 1, Bender can take one
// of the following decisions:
//
// * Not to bend the wire at point (i,0,0).  * To bend the wire at point
// (i,0,0) and angle of pi/2 to be parallel to the axis +y, -y, +z or -z
//
//  For example, if L=3 and Bender bends the wire at (2,0,0) on the +y axis
//  direction and at (1,0,0) on the -y axis direction the result wold be: 
//
//  |  |
//  |  |
//  |  |   
//  |  | 
//  |  +************----    
//  |  0   1   2   3
//  |
//  |  |
//  |  |       
//  |  |       *
//  |  |       *
//  |  +********--------   
//  |  0   1   2   3
//  |
//  |  |
//  |  |      
//  |  |      
//  |  |      
//  |  +****------------   
//  |  0   *   2   3
//  |      *****
//
//  Given a sequence of bends, you must determine what direction is pointed by
//  the last segment of the wire (+x in the example). You can suppose that the
//  wire can intercept itself, after all is the future.
// 
// Input ----------------------------------------------------------------------
//
// The first line of each test case gives an integer L (2 <= L <= 100_000) 
// indicating the length of the wire. The second line of each test case 
// contains the L-1 decisions taken by Bender at each point, separated by 
// spaces. The j-th decision in the list (for each 1 <= j <= L-1) corresponds 
// to the decision taken at the point (L-j 0,0), and must be one of the 
// following:
// 
// at point(L-j,0,0)
//
// * 'No' if the wire is not bended
// * '+y' if the wire is bended on the +y axis
// * '-y' if the wire is bended on the -y axis
// * '+z' if the wire is bended on the +z axis
// * '-z' if the wire is bended on the -z axis
//
// The end of the input is indicated when L = 0
//
// Output ---------------------------------------------------------------------
// 
// For each case in the input, print one line with the direction pointed by the
// last segment of the wire, '+x', '-x', '+y', '-y','+z','-z' depending on the 
// case. 
//
// ============================================================================
//
// Sample input 
//
// |3
// |+z -z
// |3
// |+z +y
// |2
// |+z
// |4
// |+z +y +z
// |5
// |No +z No No
// |0
//
// Sample output 
//
// |+x
// |+z
// |+z
// |-x
// |+z
//

#include <iostream>
using namespace std;

// Rotaciones sobre el eje y el eje z para un vector
void ry(int* v,int sign); 
void rz(int * v,int sign);
// Dado in vector [x,y,z] imprimir +-x, +-y o +-z
void print_direction(int * v); 
 

int main(void){ 

  // Leer para cada caso el tamaño del cable, se termina cuando es 0.
  int len; 
  while(cin >> len){ 
    if(len == 0){ break; } 

    // Vector que indica la dirección del cable 
    int  v[] = {1,0,0};
      
    for(int i=1; i < len; i +=1){ 
      string inst; 
      cin >> inst; 
      // No se realiza ninguna operación 
      if(inst == "No"){ continue; } 
      // se rota en la dirección +y (sobre el eje z) en sentido positivo
      else if(inst == "+y"){ rz(v,1);} 
      // se rota en la dirección -y (sobre el eje z) en sentido negativo
      else if(inst == "-y"){ rz(v,-1); }
      // se rota en dirección +z (sobre el eje y) en sentido negativo
      else if(inst == "+z"){ ry(v,-1);} 
      // se rota en dirección -z (sobre el eje y) en sentido positivo
      else if(inst == "-z"){ ry(v,1);} 
    }
    
    // Se imprime la dirección final 
    print_direction(v);
  } 
  return(0);
} 


// Rotación de +-pi/2 sobre el eje y
//
// La matriz de rotación es 
//
// [C  , 0 , S]
// [0  , 1 , 0]
// [-S , 0 , C]
//
void ry(int* v,int sign){ 
  sign = (sign < 0)? -1 : 1; 
  int temp[3] = { sign * v[2], v[1], -sign*v[0], };
  for(int i=0; i < 3; i+=1){ v[i] = temp[i]; } 
}

// Rotación de +-pi/2 sobre el eje z
//
// La matriz de rotación es 
//
// [C ,-S , 0]
// [S , C , 0]
// [0 , 0 , 1]
//
void rz(int * v,int sign){ 
  sign = (sign < 0)? -1 : 1; 
  int temp[3] = { -sign * v[1], sign*v[0], v[2], };
  for(int i=0; i < 3; i+=1){ v[i] = temp[i]; } 
}

// Se imprime la dirección en términos de x y z 
//
// [ 1  , 0  , 0  ] := +x
// [ -1 , 0  , 0  ] := -x
// [ 0  , 1  , 0  ] := +y
// [ 0  , -1 , 0  ] := -y
// [ 0  , 0  , 1  ] := z
// [ 0  , 0  , -1 ] := -z
//
void print_direction(int * v){ 
  for(int i=0; i < 3; i+=1){ 
    if (v[i] != 0){ 
      cout 
        << ((v[i] < 0) ? "-" : "+") 
        << ((i == 0) ? "x" : (i==1)? "y" : "z") 
        << endl; 
    } 
  } 
}
