// Ricardo Nieto Fuentes
// nifr91@gmail.com 
//
// ============================================================================
// 
// Coconuts, revisited --- 616
// 3.000 seconds 
//
// The short story titled _Coconuts_ by Ben Ames Williams, appeared in the 
// _Saturday Evening Post_ on October 9, 1926. The story tells about five men
// and a monkey who were shipwrecked on an island. They spent the first night
// gathering coconuts. During the night, one man woke up and decided to take 
// his share of the coconuts. He divided them into five piles. One coconut was
// left over so he gave it to the monkey, then hid his share an went back to 
// sleep. 
//
// Soon a second man did the same thing. After dividing the coconuts into 
// five piles, one coconut was left over which he gave to the monkey. He then 
// hid his share and went back to bed. The third, fourth, and fifth man 
// followed exactly the same procedure. The next morning, after they all woke 
// up, they divided the remaining coconuts into five equal shares. This time
// no coconuts were left over. 
//
// An obvious question is "how many coconuts did they originally gather?" there
// are an infinite number of answers, but the lowest of these is 3_121. But, 
// that's not our problem here. 
//
// Suppose  turn the  problem around. If we know the number of coconuts that 
// were gathered, what is the maximum number of persons (and a monkey) that 
// could have been shipwrecked if the same procedure could occur?.
//
// Input: 
//
// The input will consist of a sequence of integers each representing the 
// number of coconuts gathered by a group of persons (and a monkey) that where
// shipwrecked. The sequence will be followed by a negative number. 
//
// Output: 
//
// For each number of coconuts, determine the largest number of persons who 
// could have participated in the procedure described above. Display the 
// results similar to the manner shown below, in the Sample Output. There
// may be no solution for some of the input cases; if so, state that 
// observation.
//
// ===========================================================================
//
// Sample input: 
//
// |25
// |30
// |3121
// |-1
//
// Sample Output:
//
// |25 coconuts, 3 people and 1 monkey
// |30 coconuts, no solution
// |3121 coconuts, 5 people and 1 monkey
//
// ============================================================================

// Solución -------------------------------------------------------------------
// 
// Sea 'p' el número de personas, 'n' el número de cocos, tenemos que existe 
// un 'k' tal que : 
//
// (n-1) = k p 
//
// en el segundo paso tenemos que existe un 'l' 
//
// k(p - 1) - 1 = l p  
//
// con k > l 
//
// Similarmente 
//
// l (p - 1) - 1 = m p 
//
// restando 
//
// (k - l)(p-1) = (l - m)p 
//
// como k - l > 0 y suponiendo que p > 2 -> gcd(p,p-1) = 1 entonces k - l = r p
//
// en particular k > p, como (n-1) = k p ->  n-1 > p**2 -> p < sqrt(n-1)
//
// ----------------------------------------------------------------------------

#include<iostream> 
#include<cmath>
using std::cin; 
using std::cout; 
using std::endl;
using std::max;

int main(int num_args, char ** args){ 
  
  int coconuts = 0; 

  // Para cada prueba 
  while (cin >> coconuts && coconuts >= 0){ 

    // Probamos todos desde srt(n-1) hasta 2
    int num_pers = 0;
    for(int personas = max((int)sqrt(coconuts-1),2); personas >= 2; personas -=1){ 
      // Solamente se considera aquellos que cumplen (n-1) = k p
      if((coconuts-1)/personas > 0 && ((coconuts-1)%personas) == 0){ 

        // Al cumplir simulamos 
        int n = coconuts;
        bool posible = true;
        for(int i=0; i < personas; i+=1){ 
          // En cada paso nos debe sobrar un coco 
          if ((n%personas) != 1){
            posible = false; 
            break;
          } 
          // Restamos los cocos de la persona y del mono 
          n = n - ((n-1)/personas) - 1;
        }
        // Al finalizar no deben sobrar cocos y debe poderse dividir entre 
        // el número de personas por igual
        if((n % personas)==0 && posible){
          num_pers = personas;
          break; 
        }
      } 
    } 

    // Desplegar la información
    cout << coconuts << " coconuts, " ;
    if(num_pers == 0){ cout << "no solution" << endl; } 
    else { cout << (num_pers) << " people and 1 monkey" << endl;} 
  } 

  return 0; 
} 
