// Ricardo Nieto Fuentes
// nifr91@gmail.com
//
// ============================================================================
//
// Integer Sequences from Addition of Terms --- 927
// 3.000 s
//
// We consider sequences formed from the addition of terms of a given sequence
// Let {a_n} , n=1,2,3..., be an arbitrary sequence of integers numbers; 'd' 
// a positive integer. We construct another sequence {b_m} , m=1,2,3,... by 
// defining 'b_m' as consisting of '(n)(d)' occurrences of the term a_n: 
//
// b_1 = a_1,....,a_1  :  d occurrences of a_1
// b_2 = a_2,....,a_2  : 2d occurrences of a_2
// b_3 = a_3,....,a_3  : 3d occurrences of a_3
// ....
// b_m = a_m,....,a_m  : md occurrences of a_m
//
// For example, if a_n = n, and d=1, then the resulting sequence {b_m} is : 
//
// 1,    2,2, 3,3,3, 4,4,4,4,....
// ---   ---  -----  -------
// b_1   b_2   b_3     b_4
// 
// Given 'a_n' and 'd' we want to obtain the corresponding k-th integer in the
// sequence {b_m}. For example, with 'a_n = n' and 'd=1' we have 3 for k =6; we
// have 4 for k=7. With 'a_n = n' and d=2, we have 2 for k=6; we have 3 for
// k=7.
// 
// Input
//
// The first line of input contains 'C' (0 < C < 100), the number of test cases
// that follows. 
//
// Each of the 'C' test cases consists of three lines: 
//
// 1. The first line represents 'a_n' a polynomial in 'n' of degree 'i' with 
//    non-negative integer coefficients in increasing order of the power: 
//
//    a_n = c_0 + c_1 n + c_2 n^2 + c_3 n^3 + .... + c_i n^i 
//
//    where c_j in N, j=0,...,i. This polynomial 'a_n' is codified by its 
//    degree 'i' followed by the coefficients c_j. All the numbers are 
//    separated by a single space. 
//
// 2. The second line is the positive integer 'd'. 
// 3. The third line is the positive integer 'k'. 
//
// It is assumed that the polynomial 'a_n' is a polynomial of degree less 
// or equal than 'i' ( 1 <= i <= 20) with non-negative integer coefficients
// 'c_j' ( 0 <= c_j <= 10_000), 1 <= d <= 100_000; 1 <= k <= 1_000_000. 
//
// Output 
// 
// The output is a sequence of lines, one for each test case. Each of these 
// lines contains the k-th integer in the sequence {b_m} for the corresponding
// test case. This value is less or equal than 2^63 - 1.
//
// ===========================================================================
//
// Sample Input 
//
// |2
// |4 3 0 0 0 23
// |25
// |100
// |1 0 1
// |1
// |6
//
//
// Sample Output 
//
// |1866
// |3
//
// ============================================================================

#include<iostream> 
using namespace std; 

// Usamos el algoritmo de Horner para evaluar de forma eficiente el 
// polinomio. 
// Se define una secuencia de constantes 
//
// b_n = cn
// b_n-1 = a_(n-1) + x*b_n
// ... 
// b_0 = c_0 + x*b_1
// de tal forma que b_0  = p(x)
//
long long polyval(int * c,int n,int deg){ 
  long long v = c[deg]; 
  for(int j=deg-1; j >= 0; j-=1){ 
    v = c[j] + (v*n); 
  } 
  return v; 
} 


// Se realiza una búsqueda binaria para encontrar el valor n en la 
// secuencia  
//
// |b_1| = 1d
// |b_2| = 2d
// |b_3| = 3d
// .... 
// |b_m| = md
// 
// tal que sum(|b_1|+...+|b_(n-1)|) < k. 
//
// Note que la suma es 1d + 2d + ... + md = d(1+2+...+n) = ((n+1)n/2)d
//
int bsearch(int k,int d){ 
  int  l = 1;
  int  r = 10000001;

  while (l < r){ 
    long long  m = (l + r) / 2; 
    long long n = (m*(m+1)*d)/ 2 ;   
  
    if (n < k) {l = m+1; }
    else { r = m; } 
  } 

  return l; 
} 

// Main =======================================================================

int main(int argc, char ** argv){ 
  // Coeficientes del polinomio 

  int c[21]; 
  // Obtener la cantidad de casos de prueba
  int testcases; 
  cin >> testcases; 

  // Para cada caso de prueba
  for(int t=0; t < testcases; t+=1){ 
    // Leer la entrada
    int deg; 
    cin >> deg; 
    for(int i=0; i <= deg; i+=1){cin >> c[i];} 
    int d; 
    int k; 
    cin >> d >> k; 
    
    // Encontrar el valor de n
    int n = bsearch(k,d); 
    // Evaluar en n
    cout << polyval(c,n,deg) << endl; 
  } 

  return 0;
} 
