// Ricardo Nieto Fuentes
// nifr91@gmail.com
//
// ============================================================================
//
// Frequent values --- 11235 
//
// You are given a sequence of 'n' integers a_1,a_2,...,a_n in non-decreasing
// order. In addition to that, you are given several queries consisting of 
// indices 'i' and 'j' (1 <=i <= j <= n). For each query, determine the most 
// frequent value among the integers a_i,...,a_j. 
//
// Input 
//
// The input consist of several test cases. Each test case starts with a line 
// containing two integers 'n' and 'q' (1<= n,q <= 100_000). The next line 
// contains 'n' integers a_1,...,a_n (-100_000 <= a_i <= 100_000, for each i 
// in {1,...,n}) separated by spaces. You can assume that for each 'i' in 
// {1,...,n-1}: a_i <= a_(i+1). The following q lines contain one query each
// , consisting of two integers 'i' and 'j' (1 <= i <= j <= n), which indicates
// the boundary indices for the query. 
//
// The last test case is followed by a line containing a single '0'. 
//
// Output
//
// For each query, print one line with one integer: The number of occurrences
// of the most frequent value within the given range. 
//
// ============================================================================
//
// Sample Input 
//
// |10 3
// |-1 -1 1 1 1 1 3 10 10 10
// |2 3
// |1 10 
// |5 10 
// |0
//
// Sample Output 
// 
// |1
// |4
// |3

#include <iostream> 
using namespace std; 

// Estructura auxiliar que representa los rangos 
struct range{ 
  int l; int r; 
  range(int _l,int _r):l(_l),r(_r){} 
  bool contains(range rng){return((this->l <= rng.l) && (rng.r <= this->r));}
  bool intersects(range rng){return ((this->l <= rng.r) && (rng.l <= this->r));} 
};

// Estructura que representa la solución, la cuál es un número y las veces
// que aparece repetido
struct sol{ 
  int val; int cnt; 
  sol(int _val, int _cnt):val(_val),cnt(_cnt){ } 
}; 

// Estructura que almacena los valores para un rango 
struct query_val{ 
  // Valor más repetido 
  sol mostrep; 
  // Valor a la izquierda del rango 
  sol lmost; 
  // Valor a la derecha del rango 
  sol rmost;
  query_val(sol _most, sol _left, sol _right):
    mostrep(_most),
    lmost(_left),
    rmost(_right){}
}; 

// Estructura que representa  un nodo, tiene el valor del nodo, y apuntadores
// al nodo hijo izquierdo y al nodo hijo derecho, así como el valor del rango
// que representa. 
struct node{ 
  node * l; 
  node * r; 
  query_val val;
  range rng; 
  node(query_val _val, range _rng):l(nullptr),r(nullptr),val(_val),rng(_rng){}
};


// Árbol de segmentos que nos permite calcular el O(lg(n)) el valor 
// requerido en un rango 
//
// Cada nodo del árbol representa el número de veces que el valor más 
// repetido esta presente en el intervalo.  El nodo almacena los siguientes 
// valores. 
// 
// el valor a la izquierda y el número de veces que esta en el intervalo  
// el valor a la derecha y el número de veces que esta en intervalo 
// el valor más repetido y el número de veces que esta en el intervalo 
//
// [llll mmmmm rrrrr]
//
// note que puede ocurrir que l == r -> l == m == r.
//
//
class segment_tree{ 

  // raíz; 
  node * root;

  // Inicialización de árbol de segmentos 
  node * init_st(range rng,int * ary){
    // Si es nodo hoja se crea el nodo y se asigna el valor del arreglo
    if(rng.l == rng.r){ 
      auto number = ary[rng.l]; 
      auto val = query_val(sol(number,1),sol(number,1),sol(number,1)); 
      return new node(val,rng);
    }
  
    // Si no es nodo hoja se divide en dos el segmento y se llama 
    // recursivamente 
    auto m = (rng.l + rng.r)/2; 

    node * nl = init_st(range(rng.l,m),ary); 
    node * nr = init_st(range(m+1,rng.r),ary); 
  
    // se unen los dos valores de los nodo hijos para obtener el nuevo valor 
    // del nodo
    auto val = merge_query_val(nl->val,nr->val); 
    auto n = new node(val,rng);
    n->l = nl; 
    n->r = nr; 
    return n; 
  }

  public: 
  
  // Constructor
  segment_tree(int * ary,int n){ this->root = init_st(range(0,n-1),ary); } 
  ~segment_tree(){ 
    this->free_nodes(this->root); 
  } 

  void free_nodes(node * n){
    if(n){ 
      free_nodes(n->l); 
      free_nodes(n->r); 
      delete n;
    } 
  } 

  // Consulta del valor en un rango
  int query(range rng){return(valin(rng,this->root).mostrep.cnt);} 

  // Consulta del valor en un rango 
  query_val valin(range rng,node * n){
    // Si toda la información del nodo se requiere se regresa el valor
    if (rng.contains(n->rng)){ return n->val;}

    // Si parte de la información se requiere se llama recursivamente y
    // se combinan los valores regresados
    if (rng.intersects(n->rng)){
      auto vl = valin(rng,n->l); 
      auto vr = valin(rng,n->r); 
      return(merge_query_val(vl,vr)); 
    }
    // Si no se requiere la información del nodo se regresa el neutro de la
    // operación
    return(query_val(sol(0,0),sol(0,0),sol(0,0))); 
  } 

  // Combinar dos valores del segmento
  //
  //         left               right 
  // [llll mmmmm rrrrr] + [bbbb nnnnn eeeee] 
  // 
  // note que si el valor 'r' de left y el valor 'b' de right son iguales tenemos
  //
  // [llll mmmm xxxxxxxx nnnnn eeeee] 
  //
  // solo se tiene que comprobar si el número de 'x' es mayor que los números 
  // de 'm' y 'n'.
  // En este caso existe la posibilidad de que 'l' == 'r' o 'b' == 'e' en 
  // cuyo caso se tiene 
  //
  // [ xxxxxxxxxxxx nnnnn eeeee] 
  // o 
  // [ lllll mmmmm xxxxxxxxxxxx ] 
  // o 
  // [ xxxxxxxxxxxxxxxxxxx ] 
  //
  query_val merge_query_val(query_val lval, query_val rval){
    // Por defecto generamos 
    // [llll kkkk bbbbb] 
    // donde k es el mas repetido entre el izquierdo y el derecho 
    auto newval = query_val(
      (lval.mostrep.cnt > rval.mostrep.cnt) ? lval.mostrep : rval.mostrep,
      lval.lmost, 
      rval.rmost);

    // Si los nodos están unidos por el valor de los extremos internos 
    // [ lllll mmmm  xxxxxxxxxxxx nnnnn eeeeeee] 
    // se comprueban los casos
    if (lval.rmost.val == rval.lmost.val){ 
      auto mid_cnt = lval.rmost.cnt + rval.lmost.cnt;
      // Si el valor de enmedio es el mas repetido se asigna
      if ((mid_cnt > lval.mostrep.cnt) && (mid_cnt > rval.mostrep.cnt)){
        newval.mostrep.val = lval.rmost.val; 
        newval.mostrep.cnt = mid_cnt; 
      }
      //  si el rango entero solo estaba conformado por un número se 
      //  actualiza el valor de la cuenta. 
      if(lval.lmost.val == lval.rmost.val){newval.lmost.cnt = newval.mostrep.cnt;}
      if(rval.lmost.val == rval.rmost.val){newval.rmost.cnt = newval.mostrep.cnt;}
    } 
    return newval;
  }; 
}; 

// Driver ---------------------------------------------------------------------
int main(int argc, char ** argv){ 
  int n = 0; 
  int queries = 0;
  int * ary = new int[100000]; 
 
  // Mientras se tengan casos de prueba
  while (cin >> n >> queries){ 
    if (n == 0){break;}

    // Leer el arreglo 
    for(int i=0; i < n; i+=1){cin >> ary[i];}

    // Crear segment tree 
    auto st = segment_tree(ary,n); 
  
    // Para cada pregunta se lee el rango y se pregunta sobre él. 
    range rng(0,0); 
    for(int t=0; t < queries; t+=1){ 
      cin >> rng.l >> rng.r; 
      rng.l -= 1; 
      rng.r -= 1;
      cout << st.query(rng) << endl; 
    } 
  }

  // Liberar memoria
  delete [] ary; 

  return(0); 
} 



