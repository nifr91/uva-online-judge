// Ricardo Nieto Fuentes
// nifr91@gmail.com 
//
// ============================================================================
//
// Word Problem --- 895
// 3.000 seconds
//
// In a popular puzzle often found in newspapers, a set of letter is provided
// and the challenge is to find how many different words can be made from these
// letters. This problem is designed to take all the fun out of it by
// automating the process.
//
// Input: 
//
// Input will be in two parts. The first part will be a dictionary of less than
// 1_000 lines, containing words to be searched for. Each line will contain one
// word of up to 10 characters. Each word will be in lower case. The words will
// be in alphabetical order. The end of the dictionary will be indicated by a
// line consisting of a single '#' character.
//
// After the dictionary there will be data for several word puzzles, each on a
// separate line. Each puzzle line will have from one to 7 lower case letters,
// separated by one or more spaces. Your task is to arrange some or all of
// these letters to form words in the dictionary. The list of puzzles will be
// terminated by a line consisting of a single '#'. 
//
// Output: 
//
// For each puzzle line in the input, a single line fo output should be produced
// containing the number of different words in the dictionary that can be formed
// using the letters in the puzzle line. 
//
// Note that each letter may be used only as many times as it appears in the 
// puzzle line. For instance, the puzzle line 'ull' may produce the word 
// 'lul' but not the word 'lull'.
// 
// ============================================================================
//
// Sample Input: 
//
// |ant
// |bee
// |cat
// |dog
// |ewe
// |fly
// |gnu
// |#
// |bew
// |bbeeww
// |tancugd
// |#
//
// Sample Output: 
//
// |0
// |2
// |3
//
// ============================================================================


// Solución -------------------------------------------------------------------
//
// Generar un mapa número letras -> cuenta palabras 
// para cada posible combinación de letras del puzzle buscar en el mapa.
//
// ----------------------------------------------------------------------------

#include<iostream>
#include<sstream>
using std::cin; 
using std::cout;
using std::to_string;
using std::endl;
using std::string;
using std::stringstream;

// Estructura para almacenar la palabra
struct nword { 
  int letter[26] = {}; 
  nword(){} 
  nword(string & w){for(int i=0; i < (int)w.size(); i+=1){ letter[w[i]-'a'] += 1;}} 

  // Comparación para agregar al mapa
  bool operator < (const nword & o){ 
    for(int i=0; i < 26; i+=1){ 
      if(this->letter[i] == o.letter[i]){continue;} 
      else if (this->letter[i] < o.letter[i]){ return true;} 
      else{ return false; } 
    } 
    return false; 
  } 
  bool operator == (nword & o){ return (!(*this < o) && !(o < *this)); } 
  bool operator <= (nword & o){return ((*this < o) || (*this == o));} 
} ;


// Mapa K => V
template <class K, class V> struct nmap{ 

  // Estructura de nodo 
  struct node{ 
    int id; K k; V v; node * l; node * r; node * p;
    node(K _k,V _v,node * _l = nullptr, node *_r = nullptr, node * _p = nullptr):
      k(_k),v(_v),l(_l),r(_r),p(_p){ id = std::rand(); }
  };

  // Estructura para regresar dos nodos en el mapa
  struct node_pair{ 
    node * l; node * r; node_pair(node * _l, node * _r):l(_l),r(_r){}
  };

  // Raíz 
  node * root = nullptr; 
  int size = 0; 

  // Dividir el árbol por un valor ( los valores < o <= en un árbol 
  // (l) y los demás en otro (r). Ambos árboles mantienen la propiedad del 
  // treap
  node_pair split(node * root, K & key, bool leq = true){
    node * tl = nullptr; 
    node * tr = nullptr; 
    if(!root){ return node_pair(tl,tr);} 
    bool comp = (leq) ? (key <= root->k) : (key < root->k); 
    if (comp){
      tr = root; 
      auto tpair = split(tr->l,key,leq); 
      tr->l = tpair.r;
      tl = tpair.l;
    }
    else{ 
      tl = root;
      auto tpair = split(tl->r,key,leq); 
      tl->r = tpair.l;
      tr = tpair.r;
    } 
    return node_pair(tl,tr);
  } 

  // Combinar dos árboles (l y r) que tienen la propiedad del treap en 
  // un árbol, se cumple que para el árbol l los valores son < que los 
  // valores r
  node * merge(node * tl, node * tr){ 
    if(!tl || !tr){ return ((tl) ? tl : tr);}
    if(tl->id > tr->id){ 
      tl->r = merge(tl->r,tr);
      return tl; 
    } 
    else { 
      tr->l = merge(tl,tr->l); 
      return tr; 
    } 
  }

  // Encontrar en O(lg n) un valor en el árbol 
  node * find(K & key,node * root){ 
    if(!root || (root->k == key)){return root;} 
    if(key < root->k){return find(key,root->l); } 
    return find(key,root->r);
  } 

  // Agregar un elemento al árbol  en O(lg n)
  void insert(K key, V val){ 
    node * n = new node(key,val);
    auto npair = split(this->root,key); 
    auto l = npair.l;
    auto m = npair.r;
    npair = split(m,key,false);
    m = npair.l; 
    auto r = npair.r;
    this->size += 1;
    this->root = merge(merge(l,n),r);
  } 

  // Eliminar un elemento al árbol  en O(lg n)
  void remove(K key){ 
    auto npair = split(this->root,key); 
    auto l = npair.l; 
    auto m = npair.r; 
    npair = split(m,key,false); 
    m = npair.l; 
    auto r = npair.r; 
    this->root = merge(l,r); 
    this->size -= 1; 
    free_tree(m); 
  } 

  bool includes(K key){return(!!this->find(key,this->root));} 

  // Acceder al elemento con llave key
  V & operator[](K key) {return this->find(key,this->root)->v;} 

  // Liberar la memoria de un árbol
  void free_tree(node * n ){ 
    if (n){ 
      free_tree(n->l); 
      free_tree(n->r); 
      delete n; 
    } 
  } 

  void keys(K * a){ 
    int i = 0; 
    this->get_keys(a,this->root,i); 
  } 

  // Obtener las llaves
  void get_keys(K * a,node * n,int &i){ 
    if(!n){ return ; } 
    get_keys(a,n->l,i);
    a[i] = n->k; 
    i+=1;
    get_keys(a,n->r,i); 
  }

};


int main(int num_args, char ** args){ 
 
  // Leer el diccionario 
  string word; 
  nmap<nword,int> map; 
  while( cin >> word){ 
    if(word == "#"){break;} 
    if(map.includes(word)){map[word] += 1;} 
    else {map.insert(word,1);}
  } 
  cin.ignore();

  // Leer los problemas
  char puzzle[10];
  string line; 
  while(getline(cin,line)){
    if(line == "#"){ break; } 
    stringstream ss(line); 
    string w; 
    int len =0; 
    while(ss >> w){ 
      puzzle[len] = w[0];
      len+=1; 
    } 

    // Para cada posible combinación de letras 
    nmap<nword,int> found;
    int sol_cnt = 0; 
    for(int i=0; i < (1<<len); i+=1){ 
      // Generar la palabra formada
      string s = ""; 
      for(int j=0; j < len; j+=1){ if(i & (1 << j)){s+=puzzle[j];} } 
      nword word(s); 
      // Buscar en el mapa 
      if(map.includes(word) && !found.includes(word)){
        sol_cnt += map[word];
        found.insert(word,0);
      }
    }
    found.free_tree(found.root);

    // Mostrar la solución
    cout << sol_cnt << endl; 
  } 

  return 0; 
} 
