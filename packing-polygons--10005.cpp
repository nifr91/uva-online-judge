// Ricardo Nieto Fuentes 
// nifr91@gmail.com 
//
// ============================================================================
//
// Packing Polygons --- 10005
// 3.000 seconds 
//
// Given a polygon of 'n' points (not necessarily convex), your goal is to say 
// whether there is a circle of a given radius R that contains the polygon or 
// not.
//
// Input: 
//
// The input consists of several input cases. The first line of each input case
// is the number 'n' (n < 100) of vertices in the polygon. Then you are given
// 'n' lines each containing a couple of integers that define the coordinates 
// of the vertices. The last line of the input case will be a real number 
// indication the radius R of the circle. 
//
// The end of the input will be signaled by an input case with n = 0 vertices, 
// that must not be processed.
//
// You may assume that no vertex will appear twice in any given input case.
//
// Output: 
//
// If the polygon can be packed in a circle of the given radius you have to 
// print: 
// 
// |The polygon can be packed in the circle.
// 
// if the polygon cannot be packed you have to print: 
//
// |There is no way of packing that polygon.
//
// ===========================================================================
//
// Sample Input: 
//
// |3
// |0 0
// |1 0
// |0 1
// |1.0
// |3
// |0 0
// |1 0
// |0 1
// |0.1
// |0
//
// Sample Output :
//
// |The polygon can be packed in the circle.
// |There is no way of packing that polygon.
//
// ============================================================================


// Solución -------------------------------------------------------------------
//  
//  El algoritmo de Welzl permite calcular el circulo mínimo que contiene 
//  un conjunto de puntos, con esperanza de tiempo lineal. La idea principal 
//  del algoritmo consiste en : 
//
//  Ir agregando al circulo los puntos de forma aleatoria. 
//  
//  * Si el punto esta dentro del círculo pasamos al siguiente.
//  * Si el punto no esta en el circulo, se agrega a la circunferencia 
//    y se recalcula recursivamente con los puntos que ya se han procesado. 
//    Para tener puntos en la circunferencia se tienen tres posibilidades. 
//    - 0 puntos, entonces se coloca el circulo en el punto con radio 0.
//    - 1 puntos, entonces se coloca el centro del circulo entre los dos 
//                puntos y el radio es sqrt(norm(p1 - p2))/2.
//    - 2 puntos, entonces se genera el círculo que circunscribe el triangulo 
//                formado por los tres puntos.
//                En esta situación, existen tres posibilidades, que el 
//                círculo sea el que esta dado por los dos primeros puntos 
//                y el tercero se encuentre en la circunferencia, o que 
//                el circulo este a la derecha o a la izquierda. 
//
// ----------------------------------------------------------------------------

#include<iostream>
#include<cmath>
using std::cin; 
using std::cout; 
using std::endl; 
using std::string; 
using std::to_string;
using std::min; 
using std::max;

// Estructura que representa un punto en el plano cartesiano como un vector
// del origen al punto con coordenadas x,y.
struct point {
  double x = 0; 
  double y = 0; 

  // Constructores -----------------------------------------------------------
  point(){}
  point(double _x,double _y):x(_x),y(_y){}
  point(const point &pt):x(pt.x),y(pt.y){}

  // Funciones ---------------------------------------------------------------

  point operator+(point o){ return point(this->x + o.x, this->y + o.y); }
  point operator-(){return point(-this->x,-this->y);}
  point operator-(point o){return (*this + -o);} 
  point operator/(double scalar){return point(this->x/scalar,this->y/scalar);} 

  // Producto cruz entre dos vectores en el plano, su signo indica el 
  // sentido del ángulo entre el punto y un segundo punto 'o'. En sentido 
  // horario (< 0) y en sentido anti-horario (> 0)
  double cross(point o){return ((this->x*o.y) - (this->y * o.x));} 

  // Magnitud del vector 
  double norm(){ return (this->x*this->x)+(this->y*this->y); } 
} ; 

// Estructura que representa un círculo como la posición de su centro 'c' y
// la magnitud de su radio 'r'.
struct circle{ 

  point  c = point(0,0); 
  double r = 0;

  // Constructores ------------------------------------------------------------
  circle(point center, double radius):c(center),r(radius){}
  circle(const circle & cr):c(cr.c),r(cr.r){}

  // Circulo con diametro igual a la distancia entre dos puntos.
  circle(point p, point q){ 
    this->c = (p+q)/2; 
    this->r = sqrt(max((this->c-p).norm(),(this->c-q).norm()));
  } 

  // Círculo que circunscribe al tríangulo 'pqs'
  circle(point p, point q, point s){ 
    double ox = (min(min(p.x,q.x),s.x) + max(min(p.x,q.x),s.x))/2;
    double oy = (min(min(p.y,q.y),s.y) + max(min(p.y,q.y),s.y))/2;

    double px = p.x - ox; 
    double py = p.y - oy; 
    double qx = q.x - ox; 
    double qy = q.y - oy; 
    double sx = s.x - ox; 
    double sy = s.y - oy; 

    double d = (px * (qy - sy) + qx *(sy - py) + sx * (py -qy))*2; 
    if (fabs(d) <= 1e-12){
      this->c = point(0,0); 
      this->r = 0; 
    }else{ 
      double pxnorm = px*px + py*py; 
      double qxnorm = qx*qx + qy*qy; 
      double sxnorm = sx*sx + sy*sy; 
      double x = (pxnorm * (qy-sy) + qxnorm * (sy-py) + sxnorm * (py - qy))/d; 
      double y = (pxnorm * (sx-qx) + qxnorm * (px-sx) + sxnorm * (qx - px))/d;
      this->c = point(ox + x, oy + y); 
      this->r = 
        sqrt( max(max((this->c-p).norm(),(this->c-q).norm()),(this->c-s).norm()));
    } 
  } 

  // Determinar si un círculo contiene un punto, i.e la distancia del punto 
  // al centro del circulo es menor o igual al radio.
  bool contains(point pt,double eps = 1e-9){return ((c-pt).norm() - r*r) < eps;} 
};


// Estructura tipo arreglo 
template <typename T> struct array{ 
  T * buff = new T[2];
  int buff_size = 2;
  int size = 0; 

  // Constructores -----------------------------------------------------------
  array(int buffsize){ 
    delete [] this->buff; 
    this->buff = new T[buffsize]; 
    this->buff_size = buffsize;
  } 
  ~array(){ delete [] this->buff; } 

  array(const array<T> & o){ 
    delete [] this->buff; 
    this->buff = new T[o.size]; 
    this->buff_size = o.size;
    this->size = o.size; 
    for(int i=0; i < this->size; i+=1){ 
      this->buff[i] = o.buff[i]; 
    } 
  } 

  // Funciones ----------------------------------------------------------------

  // Agregar un elemento al final del arreglo 
  T push(T val){ 
    check_resize(); 
    this->buff[this->size] = val; 
    this->size += 1; 
    return val;
  } 

  // Eliminar el último elemento del arreglo 
  T pop(){ 
    this->size -= 1; 
    return this->buff[this->size];
  } 

  // Acceder a un elemento en el arreglo 
  T & operator[](int index){
    index = check_index(index); 
    return this->buff[index]; 
  } 
  const T & operator[](int index)const { 
    index = check_index(index); 
    return this->buff[index]; 
  } 

  // Revisar si el índice está en los límites, permite acceder al último 
  // elemento con -1, penultimo -2, ...
  int check_index(int index){ 
    if(index < 0){ index = this->size + index;} 
    if(index < 0 || index >= this->size){throw "index out of bounds";} 
    return index; 
  } 

  // Función para redimensionar el arreglo de forma dinámica.
  void check_resize(){ 
    if(this->size == this->buff_size){
      this->buff_size *= 2;
      T * new_buff = new T[this->buff_size]; 
      for(int i=0; i < this->size; i+=1){new_buff[i] = this->buff[i];} 
      delete [] this->buff;
      this->buff = new_buff;
    }
  }

  // Eliminar todos los elementos en el arreglo 
  void clear(){this->size = 0;} 

  // Regresa un nuevo arreglo con los elementos desordenados
  array<T> shuffle() const{ 
    array<T> shuffled(*this);
    for(int i=0; i < shuffled.size; i+=1){ 
      int k = i + (rand() % (shuffled.size-i)); 
  
      T temp = shuffled[i]; 
      shuffled[i] = shuffled[k]; 
      shuffled[k] = temp; 
    } 
    return shuffled; 
  } 
}; 

// Algoritmo Welzl ------------------------------------------------------------

circle welzl(array<point> & points, point p); 
circle welzl(array<point> & points, point p);
circle welzl(array<point> & points, point p, point q); 

// Caso base, no hay ningún punto en la circunferencia 
circle welzl(array<point> &points){

  // Vamos a incluir puntos de forma aleatoria.
  auto shuffled = points.shuffle(); 

  // Se agrega cada punto 
  circle c(point(0,0),0); 
  for(int i=0; i < shuffled.size; i+=1){
    point p = shuffled[i]; 
    
    // Si el punto no esta en el círculo, entonces lo agregamos a la 
    // circunferencia y llamamos a la función para el caso 1 punto
    if(!c.contains(p)){ 
      shuffled.size = i+1;
      c = welzl(shuffled,p);
      shuffled.size = points.size; 
    } 
  } 

  return c;
} 


// Caso 1 punto en la circunferencia, se genera un circulo en que tiene el 
// punto en su circunferencia.
circle welzl(array<point> &points , point p){ 

  // Circulo con p en su circunferencia
  circle c = circle(p,0);

  // Se agrega cada punto al círculo 
  for(int i=0; i < points.size; i+=1){ 
    point q = points[i];
    
    // Si el circulo no contiene al punto y solo contiene 1 punto, entonces
    // generamos un circulo con dos puntos. Si el círculo contiene dos 
    // puntos en su circunferencia entonces el tercer punto debe agregarse.
    if(!c.contains(q) ){ 
      if (c.r == 0){ c = circle(p,q); }
      else{ 
        int len = points.size; 
        points.size = i+1;
        c = welzl(points, p,q); 
        points.size = len; 
      } 
    } 
  }
  return c; 
} 

// Caso 2 puntos en la circunferencia, se generan tres circulo y se 
// intenta ir agregando los puntos, al final se regresa el más pequeño que 
// contenga los tres puntos.
circle welzl(array<point> & points, point p, point q){ 

  // Generar los tres círculos
  auto c = circle(p,q); 
  auto l = circle(point(0,0),0); 
  auto r = circle(point(0,0),0); 

  // Por cada punto que no este en el circulo de dos puntos, ver si esta a la
  // izquierda o a la derecha
  point pq = q - p; 
  for(int i=0; i < points.size; i+=1){
    point s = points[i]; 

    // Si el circulo qp contiene al punto, pasamos al siguiente punto 
    if (c.contains(s)){ continue; } 

    // Si el círculo no contiene al punto, entonces generamos una 
    // circunferencia con el tercer punto y se clasifica como izquierda o 
    // derecha.
    double cross = pq.cross(s - p); 
    circle cc = circle(p,q,s);
    if(cc.r == 0){continue;} 
    else if (cross > 0 && (l.r == 0 || pq.cross(cc.c-p) > pq.cross(l.c-p))){
      l = cc; 
    } 
    else if(cross < 0 && (r.r == 0 || pq.cross(cc.c-p) < pq.cross(r.c-p))){ 
      r = cc;
    } 
  } 

  // Elegir el círculo a regresar
  if((l.r == 0) && (r.r == 0)){ return c; } 
  else if(l.r == 0){ return r; } 
  else if(r.r == 0){ return l; } 
  else{return (l.r <= r.r) ? l : r ;} 
} 


// Driver =====================================================================

int main(int num_args, char ** args){ 
  srand(time(nullptr)); 
  array<point> ary = array<point>(10); 

  // Leer el número de puntos 
  int num_points; 
  while(cin >> num_points){ 

    // Un polígono sin puntos indica el fina del problema
    if(num_points == 0){ break; } 
    
    // Borrar los puntos del caso de prueba anterior y leer los vértices del 
    // nuevo polígono.
    ary.clear();
    for(int i=0; i < num_points; i+=1){ 
      point p; 
      cin >> p.x >> p.y; 
      ary.push(p);
    }
    // Leer el radio que se pregunta.
    double radius = 0;
    cin >> radius; 


    // Calcular el círculo mínimo que contiene los puntos.
    auto c = welzl(ary); 

    // si el radio es mayor o igual cabe, si no no cabe.
    if(c.r <= radius){ 
      cout << "The polygon can be packed in the circle." << endl; 
    }else{ 
      cout << "There is no way of packing that polygon." << endl; 
    } 
  } 
  return 0; 
} 
