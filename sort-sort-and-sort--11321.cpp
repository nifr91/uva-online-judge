// Ricardo Nieto Fuentes
// nifr91@gmail.com
//
// ============================================================================
//
// Sort! Sort! and Sort!!! --- 11321
// 3.000 seconds 
//
// Hmm! Here you are asked to do a simple sorting. You will be given 'N' 
// numbers and a positive integer 'M'. You will have to sort the 'N' numbers
// in ascending order of their modulo 'M' value. If there is a tie between an
// odd number and an even number (that is their module M value is the same) then
// the odd number will precede the even number. If there is a tie between two
// odd numbers (that is their modulo M value es the same) then the larger odd
// number will precede the smaller odd number and if there is a tie between 
// two even numbers (that is their module 'M' value is the same) then the
// smaller even number will precede the larger even number.
//
// For remainder value of negative numbers follow the rule of C programming
// language: A negative number can never have modulus grater than zero 
// E.G -100 % 3 = -1, -100 % 3 = 0, etc. 
//
// Input: 
//
// The input file contains 20 sets of inputs. Each set starts with two integers
// 'N' (0 < N <= 10_000) and 'M' ( 0 < M <= 10_000) which denotes how many 
// numbers are within this set. Each of the next 'N' lines contains 
// one number each. These numbers should fit in a 32-bit signed integer. Input
// is terminated by a line containing two zeroes.
//
// Output:
//
// For each set of input produce N+1 lines of outputs. The first line of each 
// set contains the value of N and M. The next N lines contain N numbers, sorted
// according to the rules mentioned above. Print the last two zeroes of the 
// input file in the output file also.
//
// ============================================================================
//
// Sample Input: 
// 
// |15 3
// |1
// |2
// |3
// |4
// |5
// |6
// |7
// |8
// |9
// |10
// |11
// |12
// |13
// |14
// |15
// |0 0
//
// Sample Output: 
//
// |15 3
// |15
// |9
// |3
// |6
// |12
// |13
// |7
// |1
// |4
// |10
// |11
// |5
// |2
// |8
// |14
// |0 0
//
// ===========================================================================


// Solution  ------------------------------------------------------------------
//
// Implementar in mergesort con los valores especificados
//
// ----------------------------------------------------------------------------


#include<iostream>
using namespace std; 

// Objeto que representa el número del problema 
struct num{ 
  // Número original 
  int number = 0; 
  // modulo 
  int mod = 0; 
  // Si es par o impar
  bool even = true; 

  // Comparación especificada en el problema 
  bool operator<(const num & other) const{ 
    if (this->mod > other.mod){return false;} 
    if (this->mod == other.mod){ 
      if(this->even && other.even){ 
        return this->number < other.number; 
      }else if(!this->even && !other.even){ 
        return this->number > other.number; 
      }else{ 
        return !this->even; 
      } 
    }
    return true; 
  } 
}; 


// Función que ordena los números en O(n lg n)
void
merge_sort(num * nums, int len){ 
  // Caso base 
  if(len < 2){ return ; } 

  // Copiar en dos sub-arreglos el arreglo nums
  int llen = len/2;
  int rlen = len - llen; 
  num l[llen] = {}; 
  for(int i=0; i < llen; i+=1){ l[i] = nums[i]; } 
  num r[rlen] = {}; 
  for(int i=0; i < rlen; i+=1){ r[i] = nums[i+llen];} 

  // Ordenar los dos sub-arreglos recursivamente
  merge_sort(l,llen); 
  merge_sort(r,rlen); 

  // Copiar los sub-arreglos en el arreglo original
  int li=0; 
  int ri=0; 
  int i=0; 
  while( (li < llen) && (ri < rlen)){ 
    if(r[ri] < l[li]){ 
      nums[i] = r[ri]; 
      ri += 1; 
    }else{ 
      nums[i] = l[li]; 
      li+=1; 
    } 
    i += 1; 
  } 
  while(li < llen){ 
    nums[i] = l[li]; 
    li+=1; 
    i +=1; 
  } 
  while(ri < rlen){ 
    nums[i] = r[ri]; 
    ri +=1; 
    i +=1; 
  } 
} 

// ============================================================================


int main(int num_args, char ** args){ 
  // Leer n y m 
  int n =0 ; 
  int m =0 ; 
  while(cin >> n >> m){ 
    // Si 0 0 se termina 
    if(!n && !m){ break; } 

    // Leer los números calculando su modulo y si es par o impar
    num nums[n] = {}; 
    for(int i=0; i < n ; i+=1){ 
      cin >> nums[i].number;
      nums[i].mod = nums[i].number % m; 
      nums[i].even = !(nums[i].number % 2); 
    } 
   
    // Ordenar 
    merge_sort(nums,n); 

    // Mostrar los resultados 
    cout << n << " " << m << endl; 
    for(int i=0; i < n; i+=1){ 
      cout << nums[i].number << endl; 
    } 
  } 
  cout << "0 0" << endl; 
  
  return 0; 
} 
