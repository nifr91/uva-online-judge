// 
// Ricardo Nieto Fuentes
// nifr91@gmail.com
// 
// =============================================================================
// 
// uva.onlinejudge
// 
// Division of Nlogonia --- 11498
// 
// After centuries of hostilities and skirmishes between the four nations 
// living in the land generally known as Nlogonia, and years of negotiations 
// involving diplomats, politicians and the armed forces of all interested
// parties, with mediation by UN, NATO,G7 and SBC, it was at last agreed by 
// all the way to end the dispute, dividing the land into four independent 
// territories. 
// 
// It was agreed that one point, called _division point_, with coordinates
// established in the necotiations, would define the country division, in the 
// following way. Two lines, both containing the division point, one in the 
// north-south direction and one in the east-west direction, would be drain 
// on the map, dividing the land in to four new countries. Starting from 
// the western-most, northen-most quadrant in clockwise direction, the new 
// countries will be called northewestern nlogonia, northeastern nlogonia,
// southeastern nlogonia and southwestern nlogonia. 
// 
// |               N 
// |               |  
// |  Northwestern |  Northeastern 
// |  Nlogonia     |  Nlogonia 
// |               |
// | W ------------+--------------- E 
// |  Southwestern |  Southeastern 
// |  Nlogonia     |  Nlogonia 
// |               |
// |               |
// |               S
// | 
// | + division point 
// | 
// 
// The UN determined that a page in the internet should exist so that the 
// inhabitants could check in which of the countries their homes are. You 
// have been hired to help implementing the system. 
// 
// Input ---------------------------------------------------------------------
// 
// The input contains several test cases. The first line of a test case 
// contains one integer 'K' indicating the number of queries that will be 
// made (0 < K <= 10^3). The second line of a test case contains two 
// integeres 'N' and 'M' representing the coordinates of the division point
// (-10^4 < N,; < 10^4). Each of the K following lines contains two integers 
// 'X' and 'Y' representing the coordinates of a residence 
// (-10^4 <= X,Y <= 10^4). The end of input is indicated by a line containing 
// only the number zero. 
// 
// Output -------------------------------------------------------------------
// 
// For each test case in the input yout program must print one line containing: 
// 
//   * 'divisa' (means border in portuguese) if the residence is on one of the 
//     border lines (nort-south or east-west). 
//   * 'NO' if the residence is in northwester Nlogonia
//   * 'NE' if the residence is in northeaster Nlogonia
//   * 'SE' if the residence is in Southeastern Nlogonia 
//   * 'SO' if the residence is in Southwestern Nlogonia 
// 
// =============================================================================
// 
// Sample input-----------------------------------------------------------------
// 
// |3
// |2 1
// |10 10
// |-10 1
// |0 33
// |4
// |-1000 -1000
// |-1000 -1000
// |0 0
// |-2000 -10000
// |-999 -1001
// |0
// 
// Sample Output----------------------------------------------------------------
// 
// |NE
// |divisa
// |NO
// |divisa
// |NE
// |SO
// |SE
//
//

#include<iostream>
using namespace std; 

int main(void){ 

  // Number of requests
  int k; 
  // Coordinates of division point
  int n,m;
  // Coordinates of house
  int x,y; 

  while(true){ 
    cin >> k; 
    if(k == 0){ break;} 

    cin >> n >> m; 
    
    for(int i=0; i < k ; i+=1){ 
      cin >> x >> y; 

      if( (x == n) || (y == m))  { cout << "divisa" << endl; }
      else if((x > n) && (y > m)){ cout << "NE" << endl;} 
      else if((x < n) && (y > m)){ cout << "NO" << endl;} 
      else if((x > n) && (y < m)){ cout << "SE" << endl;} 
      else if((x < n) && (y < m)){ cout << "SO" << endl;} 
    } 
  } 

  return(0); 
} 
