// Ricardo Nieto Fuentes
// nifr91@gmail.com 
//
// uva.onlinejudge.org
// 
// Relational Operator -- 11172
// 
// Some operators checks about the relationship between two values and these 
// operators are called relational operators. Given two numerical values 
// your job is just to find out the relationship between them that is: 
// 
// * First one is greater than the second
// * First one is less than the second 
// * First and second one are equal 
//
// Input 
// 
// First line of the input file is an integer t (t <15) which denotes how many
// sets of inputs are there. 
// Each of the next t lines contains two integers and 'a' and 'b' 
// (|a|,|b| < 1_000_000_001) 
//
// Output 
// 
// For each line of input produce one line of output. This line contains any 
// one of the relational operators '<','>','=', which indicates the relation 
// that is appropriate for the given two numbers. 
//
// Sample input 
//
// |3
// |10 20
// |20 10
// |10 10 
//
// Sample output 
// 
// |<
// |>
// |=
//

#include<iostream> 

using namespace std; 

int main(){ 
 
  int first,second; 
  int num_operations; 

  cin >> num_operations; 

  for(int i=0; i < num_operations; i+=1){ 
    cin >> first >> second;  
    cout << ((first <= second) ? ((first < second)? '<' : '=') : '>') << endl; 
  } 

  return (0); 
} 
