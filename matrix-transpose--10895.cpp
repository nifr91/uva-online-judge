// Ricardo Nieto Fuentes
// nifr91@gmail.com 
//
// ============================================================================
//
// Matrix Transpose --- 10895
// 3.000 
//
// A matrix is a rectangular array of elements, most commonly numbers. A matrix
// with 'm' rows and 'n' columns is said to be an m by n matrix. For example: 
//    
//     [ 1  3  2]
// a = [ 0  4 -1]
//     [ 0  0  0]
//     [ 5 -2 11]
// 
// is a 4 by 3 matrix of integers.
//
// The individual elements of a matrix are usually given lowercase symbols and
// are distinguished by subscripts. The ith row and jth column of matrix A is 
// usually referred to as a_ij. For example a_23 = -1. Matrix subscripts are 
// 1-based. 
//
// The transpose of a matrix 'M', denoted 'M^T' is formed by interchanging the
// rows and columns of M. That is the ijth element of M^T is the jth element of
// M. For example, the transpose of matrix A above is : 
//
// [ 1  0 0  5]
// [ 3  4 0 -2]
// [ 2 -1 0 11]
//
// A matrix is said to be sparse of there are relatively few non-zero elements. 
// As a 'm' by 'n' matrix has m*n number of elements, storing all elements of a 
// large sparse matrix may be inefficient as there would be many zeroes. There
// are a number of ways to represent sparse matrices, but essentially they are
// all the same: store only the non-zero elements of the matrix along with their
// row and column. 
// 
// You are to write a program to output the transpose of a sparse matrix of 
// integers.
//
// Input: 
//
// You are given several sparse matrix in a row, each of them described as
// follows.  The first line of the input corresponds to the dimensions of the
// matrix, m and n (which are the number of rows and columns, respectively of
// the matrix). You are then given m sets of numbers, which represents the rows
// of the matrix Each set consist of two lines which represents a row of the
// matrix. The first line of a set starts with the number r, which is the
// number of non-zero elements int that row, followed by 'r' numbers which
// correspond to the column indices of the column indices of the non-zero 
// elements in that row, ins ascending order; the second line has 'r' integers
// which are the matrix elements of that row. For example, matrix A above
// would have the following representation: 
//
// |4 3
// |3 1 2 3
// |1 3 2
// |2 2 3
// |4 -1
// |0
// |
// |3 1 2 3
// |5 -2 11
//
// Note that for a row with all zero elements, the corresponding set would just
// be one number, '0', int the first line followed by a blank line.
// You may assume:
//
// . the dimensions of the sparse matrix would not exceed 10_000 by 10_000
// . the number of non-zero elements would be no more than 1_000
// . each element of the matrix would be in the range -10_000 to 10_000 
// . each line has no more than 79 characters.
//
// Output
//
// For each input case, the tranpose of the given matrix in the same 
// representation.
//
// ============================================================================
//
// Sample Input 
//
// |4 3
// |3 1 2 3
// |1 3 2
// |2 2 3
// |4 -1 
// |0
// |
// |3 1 2 3
// |5 -2 11
//
// Sample Output
//
// |3 4
// |2 1 4
// |1 5
// |3 1 2 4
// |3 4 -2
// |3 1 2 4
// |2 -1 11
//
// ============================================================================

// Solution -------------------------------------------------------------------
// 
// Es posible implementar la matriz mediante un mapa de mapas de enteros 
//
// { int => { int => int} } 
//
// de forma que si el elemento i,j es cero no se encontrará en el mapa y 
// si está se encontrará en la posición int[i][j]. 
//
// Con esta implementación rala, solo se requiere generar la matriz 
// transpuesta e imprimir.
//
// ----------------------------------------------------------------------------

#include<iostream>

// Estructura de vector con funciones tipo pila.
//
template<class T> struct vect{ 
  T buffer[20000] = {}; 
  int size = 0; 
  // Agregar elemento al final 
  T push(T val){ 
    this->buffer[this->size] = val; 
    this->size +=1; 
    return val; 
  } 
  // Eliminar último elemento añadido 
  T pop( ){ 
    this->size -= 1; 
    return this->buffer[this->size]; 
  } 
  // Acceder a un elemento 
  T & operator[](int index){return this->buffer[index];} 
}; 


// Mapa K => V
template <class K, class V> struct nmap{ 

  // Estructura de nodo 
  struct node{ 
    int id; K k; V v; node * l; node * r; node * p;
    node(K _k,V _v,node * _l = nullptr, node *_r = nullptr, node * _p = nullptr):
      k(_k),v(_v),l(_l),r(_r),p(_p){ id = std::rand(); }
  };

  // Estructura para regresar dos nodos en el mapa
  struct node_pair{ 
    node * l; node * r; node_pair(node * _l, node * _r):l(_l),r(_r){}
  };


  // Raíz 
  node * root = nullptr; 
  int size = 0; 

  // Dividir el árbol por un valor ( los valores < o <= en un árbol 
  // (l) y los demás en otro (r). Ambos árboles mantienen la propiedad del 
  // treap
  node_pair split(node * root, K & key, bool leq = true){
    node * tl = nullptr; 
    node * tr = nullptr; 
    if(!root){ return node_pair(tl,tr);} 
    bool comp = (leq) ? (key <= root->k) : (key < root->k); 
    if (comp){
      tr = root; 
      auto tpair = split(tr->l,key,leq); 
      tr->l = tpair.r;
      tl = tpair.l;
    }
    else{ 
      tl = root;
      auto tpair = split(tl->r,key,leq); 
      tl->r = tpair.l;
      tr = tpair.r;
    } 
    return node_pair(tl,tr);
  } 

  // Combinar dos árboles (l y r) que tienen la propiedad del treap en 
  // un árbol, se cumple que para el árbol l los valores son < que los 
  // valores r
  node * merge(node * tl, node * tr){ 
    if(!tl || !tr){ return ((tl) ? tl : tr);}
    if(tl->id > tr->id){ 
      tl->r = merge(tl->r,tr);
      return tl; 
    } 
    else { 
      tr->l = merge(tl,tr->l); 
      return tr; 
    } 
  }

  // Encontrar en O(lg n) un valor en el árbol 
  node * find(K & key,node * root){ 
    if(!root || (root->k == key)){return root;} 
    if(key < root->k){return find(key,root->l); } 
    return find(key,root->r);
  } 

  // Agregar un elemento al árbol  en O(lg n)
  void insert(K key, V val){ 
    node * n = new node(key,val);
    auto npair = split(this->root,key); 
    auto l = npair.l;
    auto m = npair.r;
    npair = split(m,key,false);
    m = npair.l; 
    auto r = npair.r;
    this->root = merge(merge(l,n),r);
    this->size += 1; 
  } 

  // Eliminar un elemento al árbol  en O(lg n)
  void remove(K key){ 
    auto npair = split(this->root,key); 
    auto l = npair.l; 
    auto m = npair.r; 
    npair = split(m,key,false); 
    m = npair.l; 
    auto r = npair.r; 
    this->root = merge(l,r); 
    free_tree(m); 
    this->size -= 1; 
  } 

  // Liberar la memoria de un árbol
  void free_tree(node * n ){ 
    if (n){ 
      free_tree(n->l); 
      free_tree(n->r); 
      delete n; 
    } 
  } 

  // Obtener las llaves
  void get_keys(vect<K> & a,node * n){ 
    if(!n){ return ; } 
    get_keys(a,n->l);
    a.push(n->k); 
    get_keys(a,n->r); 
  }


  // Acceder al elemento con llave key
  V & operator[](K key) {return this->find(key,this->root)->v;} 

};



// Esta función se encarga de imprimir la matriz en el formato especificado 
void printmatrix(nmap<int,nmap<int,int>> rm,int nrows, int ncols){  
  
  // Imprimir el número de filas y columnas 
  std::cout << nrows << " " << ncols << std::endl; 
  for(int r=1; r <= nrows; r+=1){

    // Obtener la mapa de la fila 
    auto cm = rm[r];

    // Obtener las columnas no zero de de la fila 
    vect<int> cols; 
    cm.get_keys(cols,cm.root);

    // Imprimir los índices de las columnas
    std::cout << cols.size; 
    for(int c=0; c < cols.size; c+=1){std::cout << " " << cols[c];} 
    std::cout << std::endl; 

    // Imprimir los valores de las columnas 
    for(int c=0; c < cols.size;c+=1){ 

      // Obtener la columna y su valor 
      auto val = cm[cols[c]];
    
      std::cout << val; 
      if(c < cols.size-1){ std::cout << " "; } 
    } 
    std::cout << std::endl; 
  } 
} 

// Función que realiza la transpuesta de la matriz rala. 
// Para ello la posición a_ij = a^t_ji 
nmap<int,nmap<int,int>>
tranpose_matrix(nmap<int,nmap<int,int>> & rm,int nrows, int ncols){

  // Inicializar el mapa que contendrá la nueva matriz 
  nmap<int,nmap<int,int>> tm;
  for(int c=1; c <= ncols; c+=1){tm.insert(c,nmap<int,int>());} 

  // Para cada fila de la matriz original 
  for(int r=1; r <= nrows; r+=1){

    // Obtener la fila y sus columnas no cero 
    auto cm = rm[r];
    vect<int> cols; 
    cm.get_keys(cols,cm.root); 

    // Para cada columna no zero 
    for(int c=0; c < cols.size; c+=1){

      // Obtener la columna y el valor 
      auto val = cm[cols[c]];

      // Colocar en la matriz  matriz transpuesta
      tm[cols[c]].insert(r,val);
    } 
  }
  return tm; 
} 

// ============================================================================

int main(int num_args, char ** args){ 

  // Leer las dimensiones de la matriz 
  int nrows = 0; 
  int ncols = 0; 
  while( std::cin >> nrows >> ncols) { 
    
    // Obtener los valores de la matriz 
    nmap<int,nmap<int,int>> rowmap; 

    for(int r=1; r <= nrows; r+=1){ 
      // Leer el número de valores no zero 
      int nonzero = 0; 
      std::cin >> nonzero; 
     
      // Leer los índices de las columnas 
      int cols[nonzero] = {};
      for(int i=0; i < nonzero; i+=1){std::cin >> cols[i];} 

      // Leer los valores de las columnas 
      nmap<int,int> colmap; 
      for(int i=0; i < nonzero; i+=1){ 
        int value = 0; 
        std::cin >> value; 
        colmap.insert(cols[i],value); 
      } 

      // Insertar los valores no zero de la fila
      rowmap.insert(r,colmap); 
    } 

    // Invertir la matriz 
    auto tranpose = tranpose_matrix(rowmap,nrows,ncols); 

    // Imprimir la matriz 
    printmatrix(tranpose,ncols,nrows); 
  } 
  return 0; 
} 
