// Ricardo Nieto Fuentes
// nifr91@gmail.com
//
// ============================================================================
//
// Tour de France --- 11242 
// 3.000 seconds 
//
// A racing bicycle is driven by a chain connecting two sprockets. Sprockets
// are grouped into two clusters: the front cluster (typically consisting of 
// 2 or 3 sprockets) and a rear cluster (typically consisting between 5 and 10 
// sprockets). At any time the chain connects one of the front sprockets to
// one of the rear sprockets. 
//
// 
// The drive ratio - the ratio of the angular velocity of the pedals to that
// of the wheels - is the quotient 'n/m' where 'n' is the number of teeth on 
// the rear sprocket and 'm' is the number of teeth on the front sprocket. Two
// drive ratios d_1 < d_2 are adjacent if there is no other drive ratio 
// d_1 < d_3 < d_2. 
// The spread between a pair of drive ratios d_1 < d_2 is their quotient: 
// d_2/d_1. 
// You are to compute the maximum spread between two adjacent drive ratios 
// achieved by a particular pair of front and rear clusters.
//
// Input 
//
// Input consist of several test cases, followed by a line containing 0. Each 
// test case is specified by the following input: 
//
// * f: the number of sprockets in the front cluster
// * r: the number of sprockets in the rear cluster
// * f integers, each giving the number of teeth on one of the gears in the 
//   front cluster 
// * r integers, each giving the number of teeth on one of the gears int the 
//   rear cluster
//
// You may assume that no cluster has more than 10 sprockets and that no gear
// has fewer than 10 or mor than 100 teeth. 
//
// Output 
//
// For each test case, output the maximum spread rounded to two decimal places.
//
// ============================================================================
//
// Sample Input
//
// |2 4
// |40 50
// |12 14 16 19
// |0
//
// Sample Output
//
// |1.19
//
// ============================================================================

#include<iostream>
#include<cmath> 
#include<iomanip> 
#include<algorithm>

using namespace std; 

// Función para ordenar (merge sort)
void sort(double * ary, int size){ 

  // caso base 
  if (size == 1){ return ; } 
  
  // Dividir el arreglo en dos [  |  ] 
  int m = size/2;
  double * left  = new double[m];
  double * right = new double[size - m];
  for(int i=0; i < m; i+=1){left[i] = ary[i];} 
  for(int i=0; i < (size-m); i+=1){right[i] = ary[m+i];}  

  // Ordenar recursivamente 
  sort(left,m); 
  sort(right,size-m);

  // Merge 
  int l=0; 
  int r=0; 
  int k=0; 
  while((l < m) && (r < (size-m))){ 
    if (left[l] <= right[r]){ 
      ary[k] = left[l];
      l+=1; 
    }else{ 
      ary[k] = right[r];  
      r+=1; 
    }
    k+=1;
  } 
  while(l<m){ 
    ary[k] = left[l]; 
    l+=1; 
    k+=1;
  } 
  while(r < (size-m)){ 
    ary[k] = right[r];
    r+=1;
    k+=1;
  }
} 

int main(int argc, char ** argv){ 

  int front,rear; 
  int f[10];
  int r[10];
  double ratios[100]; 

  // Para cada caso de prueba 
  while (cin >> front >> rear){ 
    if (front == 0){ break; }
    
    // Leer los datos de entrada
    for(int i=0; i < front; i+=1){cin >> f[i];}
    for(int i=0; i < rear;  i+=1){cin >> r[i];}
 
    // Calcular las posibles combinaciones O(10*10)
    int cnt = 0; 
    for(int i=0; i < front; i+=1){ 
      for(int j=0; j < rear; j+=1){ 
        ratios[cnt] = (double)f[i] / (double)r[j]; 
        cnt += 1;
      }
    }
  
    // Ordenar las relaciones pues la condición es que d1 < d2 y no exista d3
    // que se encuentre entre ellos (d1 < d3 < d2). 
    int n = front*rear; 
    sort(ratios,n);

    // Recorrer todos los ratios por pares O(n).
    double max = 0; 
    for(int i=0; i < (n-1); i+=1){ 
      double v = ratios[i+1]/ratios[i];
      if(v > max){max = v;}
    } 

    // Imprimir el resultado 
    cout << fixed << setprecision(2) << (max) << endl; 
  } 
  return 0;
} 
