// Ricardo Nieto Fuentes
// nifr91@gmail.com
//
// ============================================================================
//
// Grid Game --- 11553
// 1.000 seconds 
//
// Alice and Bob both have lots of candies but want more. They decide to play 
// the following turn-based game. 
// They fill an 'n x n' grid 'M' with random integers. Alice begins the 
// game by crossing of an uncrossed row 'i' of the grid. Now it's Bob turn and
// he crosses off an uncrossed column 'j' of the grid. At the end of Bob's turn
// Alice takes the number of candies in the i-th row and jth column of M, 
// call this value M(i,j), from Bob. (If M(i,j) is negative, then Alice 
// gives |M(i,j)| candies to Bob). The game continues alternating turns from 
// Alice to Bob until the entire board is crossed off. 
// What is the largest amount of candies that Alice can win from Bob (or 
// least amount to lose if she cannot win) if both Alice and Bob play optimally?
//
// [-1][-3][ 2][ 4]  [==][==][==][==]   [==][##][==][==]
// [ 3][ 4][-3][ 2]  [ 3][ 4][-3][ 2]   [ 3][||][-3][ 2]
// [-2][-1][ 4][-4]  [-2][-1][ 4][-4]   [-2][||][ 4][-4]
// [ 3][ 2][-1][-2]  [ 3][ 2][-1][-2]   [ 3][||][-1][-2]
// 
// The beginning of a game between Alice (==) and Bob(||)
//
// Input: 
//
// The first line of the input contains an integer 't' (1 <= t <= 20) the
// number of test cases. Each test case starts with 'n' (1 <= n <= 8), the size
// of the grid. Then follow 'n' lines containing 'n' numbers separated by
// spaces describing 'M'. We call the jth number on ith line M(i,j) (-1000 <=
// M(i,j) <= 1000). 
//
// Output 
//
// For each test case, print the largest amount of candies that Alice can win
// from Bob. If she cannot win print the negative number indicating the 
// minimum number of candies she loses. 
//
// ============================================================================
//
// Sample Input
//
// |3
// |2
// |10 10
// |-5 -5
// |2
// |10 -5
// |10 -5
// |2
// |10 -5
// |-5 10
// 
// Sample Output
//
// |5
// |5
// |-10
//
// ============================================================================

#include <iostream>
#define loop while(true)
using namespace std; 

// Solución -------------------------------------------------------------------
// 
// Note que Bob siempre puede elegir la mejor opción que minimice el valor 
// que puede ganar Alice (independientemente de la selección de Alice. Por 
// ello el problema se traduce a encontrar la combinación de jugadas de Bob
// que minimicen la suma de las posiciones M(i,j). 
//
// Para ello se generan todas las permutaciones (posibles jugadas de Bob) y
// se calcula la suma. 
// Se regresa la suma mínima que puede obtener Bob que corresponde a la máxima
// solución de Alice (si ambos juegan de forma optima).
//
// ----------------------------------------------------------------------------


// Vector
struct vect{ 
  int buff[8]={};
  int size = 0;
  // Agregar valor al final 
  int push(int val){ 
    this->buff[this->size] = val; 
    this->size += 1;
    return val; 
  } 
  // Eliminar último elemento 
  int pop(void){ 
    this->size = max(0,this->size-1); 
    return this->buff[this->size];
  } 
  // Operador para acceder a un elemento 
  int & operator[](int index){ 
    if(index < 0){ index = this->size+1; } 
    return this->buff[index]; 
  } 
  // Eliminar todos los elementos del arreglo
  void clear(void){ 
    this->size = 0; 
  } 
}; 


// Función para encontrar la siguiente permutación
bool nextpermutation(vect & ary){ 

  // Encontrar le sufijo no incremental mas grande 
  // [0 2 1]
  //    ^
  int i = ary.size-1; 
  while(i > 0 && ary[i-1] >= ary[i]){i -= 1;}

  // Verificar si es la última permutación
  // [3 2 1] ? 
  //  ^
  if(i <= 0){ return false; }

  // ary[i-1] es el pivote
  // [0 2 1]
  //  * ^ '
  // y encontrar el elemento primer elemento a la derecha que excede el pivote
  int j = ary.size-1; 
  while(ary[j] <= ary[i-1]){j-=1;} 
  if( j < i){return false; } 

  // ary[j] será el nuevo pivote
  // [1 2 0]
  //  * ^ '
  ary[i-1] ^= ary[j];
  ary[j]   ^= ary[i-1];
  ary[i-1] ^= ary[j];

  // Invertir el sufijo 
  // [1 0 2]
  j = ary.size-1;
  while(i < j){ 
    ary[i] ^= ary[j]; 
    ary[j] ^= ary[i];
    ary[i] ^= ary[j];
    i+=1;
    j-=1;
  } 

  // Se tiene la siguiente permutación
  return true; 
} 

int main(int argc, char **argv){ 

  // Matriz que contendrá los valores 
  int mat[8][8] = {};

  // Procesar los casos
  int test_cases = 0; 
  cin >> test_cases; 
  for(int t=0; t < test_cases; t +=1){ 
    // Leer la matriz 
    int n = 0; 
    cin >> n; 
    for(int r=0; r < n; r+=1){for(int c=0; c < n; c+=1){cin >> mat[r][c];}}

    // Generar la primer permutación
    auto order = vect();
    for(int i=0; i < n; i+=1){order.push(i);}

    // Para cada permutación Bob quiere minimizar la ganancia de Alice
    int minval = 1 << 30;
    loop { 
      int sum = 0; 
      for(int i=0; i < order.size; i+=1){ sum += mat[i][order[i]]; } 
      minval = min(sum,minval); 
      if(!nextpermutation(order)){break;}
    } 

    // Imprimir el resultado
    cout << minval << endl; 
  } 
  return 0;
} 
