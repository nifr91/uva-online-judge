// Ricardo Nieto Fuentes
// nifr91@gmail.com
//
// ============================================================================
//
// Printer Queue --- 12100
// 3.000 seconds
//
// The only printer in the computer science student's union is experiencing an 
// extremely heavy workload. Sometimes there are hundreds of jobs in the 
// printer queue and you may have to wait for hours to get a single page of 
// output. 
//
// Because some jobs are more important than others, the Hacker General has 
// invented and implemented a simply priority system for the print job queue. 
// Now each hob is assigned a priority between 1 and 9 (with 9 being the
// highest priority and 1 being the lowest), and the printer operates as 
// follows.
//
// The first job J in queue is taken from the queue. 
// If there is some job in the queue with a higher priority than job J, then 
// move J to the end of the queue without printing it. 
// Otherwise, print job J (and do not put it back in the queue). 
//
// In this way, all those important muffin recipes that the Hacker General is 
// printing get printed very quickly. Of course, those annoying term papers 
// that other are printing may have to wait for quite some time to get printed
// but that's life. 
//
// Your problem with the new policy is that is has become quite tricky to
// determine when your print job will actually be completed. You decide to
// write a program to figure this out. The program will be given the current
// queue (as a list of priorities) as well as the position of your job in the
// queue, and must then calculate how long it will take until your job is
// printed, assuming that no additional jobs will be added to the queue. To
// simplify matters, we assume that printing a job always takes exactly one
// minute, and that adding and removing jobs from the queue is instantaneous.
//
//
// Input: 
//
// One line with a positive integer: the number of test cases (at most 100). 
// Then for each test case: 
//
// . one line with two integers 'n' and 'm', where 'n' is the number of jobs
// in the queue(q <= n <= 100) and 'm' is the position of your job (0 <= m 
// <= n-1). The first position in the queue is the number 0, the second is 
// the number 1 and so on. 
//
// . one line with 'n' integers in the range 1 to 9, giving the priorities of
// the jobs in the queue. The first integer gives the priority of the first 
// job, the second integer the priority of the second job and so on.
//
// Output: 
//
// For each test case, print one line with a single integer; the number of 
// minutes until your job is completely printed, assuming that no additional 
// print jobs will arrive.
//
// ============================================================================
// 
// Sample Input:
//
// |3
// |1 0
// |5
// |4 2
// |1 2 3 4
// |6 0
// | 1 1 9 1 1 1
// 
// Sample Output: 
//
// |1
// |2
// |5
//
// ============================================================================


// Solution -------------------------------------------------------------------
// 
// Generar un heap, para implementar la cola de prioridad y sacar los elementos
// hasta completar nuestra tarea. Notemos que el problema requiere que la tarea
// que tenga menos prioridad que la más alta prioridad se pospone y se manda al
// final de la lista.  Para mantener un record de esta parte del problema
// tenemos un arreglo que va a ir almacenando este orden y solo se realizarán
// aquellas tareas que tengan la misma prioridad que la más alta. 
// 
// Ejemplo: 
//
// pos = 0
// time = 0
//
// list  [1*,1,9,1,1,1]
// queue [9,1*,1,1,1,1]
//
// time = 0
// list  [_,1,9,1,1,1,1*]
// queue [9,1*,1,1,1,1]
//
// time = 0
// list  [_,_,9,1,1,1,1*,1]
// queue [9,1*,1,1,1,1]
// 
// time = 1
// list  [_,_,_,1,1,1,1*,1]
// queue [_,1*,1,1,1,1,1]
//
// time = 2
// list  [_,_,_,_,1,1,1*,1]
// queue [_,_,1,1,1,1,1]
//
// time = 3
// list  [_,_,_,_,_,1,1*,1]
// queue [_,_,_,1,1,1,1]
//
// time = 4
// list  [_,_,_,_,_,_,1*,1]
// queue [_,_,_,_,1,1,1]
//
// time = 5
// list  [_,_,_,_,_,_,_,1]
// queue [_,_,_,_,_,1,1]
//


// ----------------------------------------------------------------------------


#include<iostream> 

struct job{ 
  int p = 0; 
  int id = 0; 
  bool operator<(const job & o) const{ 
    return (this->p > o.p); 
  } 
} ;


// Objeto para tener el orden de las prioridades
struct heap{ 
  job buff[200]; 
  int size = 0; 

  // Métodos para acceder a los nodos padre e hijos
  int parent(int i){ return ((i-1) >> 1); } 
  int left(int i){ return ((i<< 1)+1); } 
  int right(int i){ return ((i<< 1)+2); } 

  // Intercambiar dos elementos en el buffer
  void swap(int a, int b){ 
    job aux = this->buff[a]; 
    this->buff[a] = this->buff[b]; 
    this->buff[b] = aux; 
  } 

  // Regresar el elemento con mayor prioridad
  job top(){return this->buff[0];} 

  // Eliminar el elemento con menor prioridad
  job pop(){ 
    swap(0, this->size-1);
    this->size -= 1; 
    heapify(0); 
    return this->buff[this->size];
  } 

  // Agregar un elemento a la cola de prioridad 
  void push(job val){
    this->buff[this->size] = val; 
    this->size += 1; 
    heapify(this->size-1); 
  } 

  // Mantener la propiedad del heap, se asume que la propiedad se cumple 
  // en los hijos pero no en el padre, en el padre pero no en los hijos o 
  // se cumple tanto para el padre como para los hijos.
  void heapify(int i){ 
    if(i < 0){ return ; } 
    int pi = parent(i); 
    int li = left(i); 
    li = (li < this->size) ? li : -1; 
    int ri = right(i); 
    ri = (ri < this->size) ? ri : -1; 

    job * b = this->buff; 
    int next = i; 

    if((pi >= 0) && (b[i] < b[pi])){ next = pi; } 
    if((li >= 0) && (ri >= 0) && ((b[li] < b[i])||(b[ri] < b[i]))){
      if((b[li] < b[i]) && (b[ri] < b[i])){next = (b[li] < b[ri]) ? li : ri;} 
      else if(b[li] < b[i]){ next = li; } 
      else{next = ri;} 
    }
    else if((li >= 0) && (b[li] < b[i])){ next = li; } 
    else if((ri >= 0) && (b[ri] < b[i])){ next = ri; } 

    if(next == i){return;} 
    swap(next,i);
    heapify(next);  
  } 
}; 

// Estructura para almacenar el orden en que se procesan las tareas
struct vect{ 
  job buff[10000] = {}; 
  int size = 0; 

  // Agregar un elemento al inicio del arreglo 
  job push(job val){ 
    this->buff[this->size] = val ; 
    this->size += 1; 
    return val; 
  }
  // Acceder a un elemento en el arreglo 
  job & operator[](int index){return this->buff[index];} 
}; 


// ============================================================================

int main(int num_args, char ** args){ 

  // Obtener el número de casos 
  int test_cases = 0; 
  std::cin >> test_cases; 
  for(int t=0; t < test_cases; t+=1){ 
    // Obtener el número de trabajos y la posición de nuestro trabajo 
    int njobs = 0; 
    int pos = 0; 
    std::cin >> njobs >> pos; 

    // Leer los trabajos 
    vect v; 
    heap q;
    for(int j=0; j < njobs; j+=1){ 
      job aux; 
      std::cin >> aux.p;
      aux.id = j; 
      v.push(aux); 
      q.push(aux); 
    } 

    // Si el proceso tiene la máxima prioridad se ejecuta (se aumenta el tiempo)
    // si no se manda al final del orden.
    int time = 0; 
    int ptr = 0;
    while(q.size){ 
      job currjob = v[ptr];
      job nextjob = q.top();

      if(currjob.p == nextjob.p){
        q.pop();
        time += 1;
        if(currjob.id == pos){break;}
      } 
      else{v.push(currjob);} 
      ptr += 1;
    } 

    // Imprimir el tiempo 
    std::cout << time << std::endl; 
  } 

  return 0; 
} 

