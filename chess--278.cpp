// Ricardo Nieto Fuentes
// nifr91@gmail.com
//
// ============================================================================
//
// Chess --- 278
//
// Almos everyone knows the problem of putting eight queens on an 8x chessboard
// such that no Queen can take another Queen. Jan Timman (a famous Dutch 
// chessplayer) wants to know the maximum number of chesspieces of ine kind
// which can be put on a mxn board with a certain size such that no piece can
// take another. Because it's rather difficult to find a solution by hand, he 
// ask your help to solve the problem. 
//
// He doesn't need to know the answer for every piece. Pawns seems rather 
// uninteresting and he doesn't like bishops anyway. He only wants to know how
// many Rooks, Knights, Queens or Kings can be placed on one board, such that 
// one piece cant take any other. 
//
// Input 
//
// The first line of input contains the number of problems. A problem is 
// stated on one line and consist of one character from the following set 'r'
// ,'k','Q','K', meaning respectively the chesspieces Rook, Knight, Queen or 
// King. The character is followed by the integers (4 <= m <= 10) and 
// n ( 4 <= n <= 10), meaning the number of rows and the number of columns of 
// the board. 
//
// Output
//
// For each problem specification in the input your program should output the
// maximum number of chesspieces which can be put on the board with the given 
// format so the are not in position to take any other piece. 
//
// Note the bottom left square is 1,1
//
// ===========================================================================
//
// Sample Input
//
// |2
// |r 6 7
// |k 8 8
//
// Sample Output
// 
// |6
// |32
//
// ============================================================================

#include<iostream> 
using namespace std;

// La idea principal es calcular el número de piezas que podemos colocar. 
// Importante notar que no es necesario obtener la configuración. 
// 
// Tablero 
// [ ][#][ ][#][ ]
// [#][ ][#][ ][#]      
// [ ][#][ ][#][ ]      
// [#][ ][#][ ][#]      
// [ ][#][ ][#][ ]      
//
// Torre                Reina              Rey                Caballo 
// [ ][ ][x][ ][ ]  [x][ ][x][ ][x]   [ ][ ][ ][ ][ ]    [ ][x][ ][x][ ]
// [ ][ ][x][ ][ ]  [ ][x][x][x][ ]   [ ][x][x][x][ ]    [x][ ][ ][ ][x]      
// [x][x][r][x][x]  [x][x][Q][x][x]   [ ][x][K][x][ ]    [ ][ ][k][ ][ ]      
// [ ][ ][x][ ][ ]  [ ][x][x][x][ ]   [ ][x][x][x][ ]    [x][ ][ ][ ][x]      
// [ ][ ][x][ ][ ]  [x][ ][x][ ][x]   [ ][ ][ ][ ][ ]    [ ][x][ ][x][ ]      
//
// Notemos que la torre podemos colocar tantas como el mínimo de las filas y
// las columnas 
//
// [r][x][x][x][x]
// [x][r][x][x][x]
// [x][x][r][x][x]
// [x][x][x][r][x]
//
// Al igual que la torre la reina se pueden clocar tantas como el mínimo de las
// filas y las columnas 
//
// [x][x][Q][x][x]
// [Q][x][x][x][x]
// [x][x][x][Q][x]
// [x][Q][x][x][x]
//
// Para el rey notemos que se pueden colocar cada 2 columnas y se pueden 
// rellenar cada 2 filas
//
// [K][x][K][x][K]
// [x][x][x][x][x]
// [K][x][K][x][K]
// [x][x][x][x][x]
//
// Para el caballo note que solo ataca casillas del color opuesto, por lo que 
// se debe colocar el que tenga mayor número de cuadros.
//
// [k][x][k][x][k]
// [x][k][x][k][x]
// [k][x][k][x][k]
// [x][k][x][k][x]
// 

int main(int argc, char ** argv){ 

  // Leer el número de casos de prueba
  int testcases; 
  cin >> testcases; 

  // Para cada caso de prueba 
  for(int t=0; t < testcases; t+=1){ 

    // Leer los valores 
    char piece; 
    int  nrows,ncols; 
    cin >> piece >> nrows >> ncols;

    int cnt = 0;

    // Dependiendo de si es rey 'K', caballero 'k', torre 'r' o reina 'Q' 
    // calculamos el número de de piezas que es posible colocar. 
    if      (piece == 'K'){ cnt = (((nrows+1)/2)* ((ncols+1)/2));}
    else if (piece == 'r'){ cnt = (nrows < ncols) ? nrows : ncols; } 
    else if (piece == 'Q'){ cnt = (nrows < ncols) ? nrows : ncols; } 
    else if (piece == 'k'){ 
      int n = (nrows*ncols); 
      cnt = n/2;
      cnt = ((n % 2) == 0) ? cnt : cnt+1;
    }

    // Imprimir
    cout << cnt << endl ; 
  } 
  return(0); 
} 
