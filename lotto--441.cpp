// Ricardo Nieto Fuentes
// nifr91@gmail.com 
//
// =========================================================================
//
// Lotto --- 441
// 3.000 seconds
//
// In the German Lotto you have to select 6 numbers from the set {1,2,...,40}. 
// A popular strategy to play Lotto - although it doesn't increase your chance
// of winning - is to select a subset S containing k (k > 6) of these 49 
// numbers, and then play several games with choosing numbers only from S. 
//
// For example for k=8 and S={1,2,3,5,8,13,21,34} there are 28 possible games:
// [1,2,3,5,8,13], [1,2,3,5,8,21],[1,2,3,5,8,34],[1,2,3,5,13,21],
// ....,[3,5,8,13,21,34]
// 
// Your job is to write a program that reads in the number 'k' and the set 'S' 
// and prints all possible games choosing numbers only from S. 
//
// Input 
//
// The input file will contain one or more tes cases. Each test case consist of
// one line containing several integers separated from each other by spaces. 
// The first integer on the line will be the number k (6 < k < 13). Then k 
// integers, specifying the set S, will follow in ascending order. 
// Input will be terminated by a value of zero (0) for 'k'. 
//
// Output 
// 
// For each test case, print all possible games, each game on one line.  The
// numbers of each game have to be sorted in ascending order and separated from
// each other by exactly one space. The games themselves have to be sorted
// lexicographically, that means sorted by the lowest number firs, then by the
// second lowest and so on, as demonstrated in the sample output below.  The
// test cases have to be separated from each other by exactly one blank line.
// Do not put a blank line after the las test case.
//
// ============================================================================
//
// Sample Input
//
// |7 1 2 3 4 5 6 7
// |8 1 2 3 4 5 8 13 21 34
// |0
//
// Sample Output 
//
// |1 2 3 4 5 6
// |1 2 3 4 5 7
// |1 2 3 4 6 7
// |1 2 3 5 6 7
// |1 2 4 5 6 7
// |1 3 4 5 6 7
// |2 3 4 5 6 7
// |
// |1 2 3 5 8 13
// |1 2 3 5 8 21
// |1 2 3 5 8 34
// |1 2 3 5 13 21
// |1 2 3 5 13 34
// |1 2 3 5 21 34
// |1 2 3 8 13 21
// |1 2 3 8 13 34
// |1 2 3 8 21 34
// |1 2 3 13 21 34
// |1 2 5 8 13 21
// |1 2 5 8 13 34
// |1 2 5 8 21 34
// |1 2 5 13 21 34
// |1 2 8 13 21 34
// |1 3 5 8 13 21
// |1 3 5 8 13 34
// |1 3 5 8 21 34
// |1 3 5 13 21 34
// |1 3 8 13 21 34
// |1 5 8 13 21 34
// |2 3 5 8 13 21
// |2 3 5 8 13 34
// |2 3 5 8 21 34
// |2 3 5 13 21 34
// |2 3 8 13 21 34
// |2 5 8 13 21 34
// |3 5 8 13 21 34
//
//=============================================================================

#include<iostream>
using namespace std; 

// ----------------------------------------------------------------------------
// Solución propuesta 
//
// Se pide enumerar lexicográficamente las posibles formas de seleccionar 
// 6 elementos en k. 
//
// (k 6) = (k!)/(6! (k-6)!)
//
// Para ello se va a ir generando el vector de combinaciones, donde para la 
// primer posición podemos tomar desde 1 .. k 
// 
// 1 2 3 4 
//
// -> [_ _ _ ] -> [1 _ _ ] -> [1 2 _] -> [1 2 3] 
//                                    -> [1 2 4]
//                         -> [1 3 _] -> [1 3 4]
//             -> [2 _ _ ] -> [2 3 _] -> [2 3 4]
// ----------------------------------------------------------------------------


// Para simplificar se implementa un vector que nos permite hacer
// push (agregar un elemento al final) y pop (quitar el último elemento)
struct vect{ 
  int * data; 
  int size = 0; 
  int buff_size = 0;

  // Reservar memoria 
  vect(int n):buff_size(n){
    this->data = new int[this->buff_size];
    for(int i=0; i < n; i+=1){this->data[i] = 0;} 
  }
  // Liberar memoria 
  ~vect(){delete [] this->data;} 
 
  // Agregar un elemento  al final 
  void push(int val){ 
    this->data[this->size] = val; 
    this->size += 1; 
  } 
  // Eliminar un elemento al final 
  int pop(void){ 
    this->size -= 1; 
    return this->data[this->size]; 
  } 
  // Ver el elemento en posición 'index'
  int at(int index){ return this->data[index]; } 
  // Borrar todos los elementos
  void clear(void){ this->size = 0; } 

  string to_s(){ 
    string str = ""; 
    for(int i=0; i < this->size; i+=1){ 
      str += to_string(this->at(i));
      if (i < this->size-1){ str += " "; } 
    } 
    return str; 
  } 
} ;


// Función para generar todas las combinaciones
//
void comb(vect & s, vect & aux,int i,int len){ 
  // Si el vector auxiliar tiene el tamaño especificado se imprime
  if (aux.size == len){ 
    for(int j=0; j < len; j+=1){ cout << aux.at(j) << ((j == (len-1)) ? "":" "); } 
    cout << endl; 
    return;
  } 
  // Consideramos los siguientes elementos para colocarlos en la posición actual
  for(int j=i; (j < s.size) && ((s.size - j) >= (len - s.size)); j+=1){
    aux.push(s.at(j)); 
    comb(s,aux,j+1,len); 
    aux.pop();
  } 
} 

int main(int argc, char ** argv){ 
  
  int k; 
  auto s = vect(13); 
  auto aux = vect(6);
  bool first = true;

  while (cin >> k){ 

    // Si k es cero se termina 
    if(!k){break; } 

    // Formato 
    if(!first){cout << endl;}
    else{first = false;} 

    // Leer los datos
    int val; 
    s.clear();
    for(int i =0; i < k ; i+=1){ 
      cin >> val; 
      s.push(val);
    }
    // Imprimir
    comb(s,aux,0,6);
  } 
  return 0; 
} 
