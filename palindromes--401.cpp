// Ricardo Nieto Fuentes
// nifr91@gmail.com 
//
// ============================================================================
//
// Palindromes --- 401
// 3.000 seconds 
//
// A regular palindrome is a string of numbers or letter that is the same 
// forward as backwards. For example the string "ABCDEDCBA" is a palindrome 
// because it is the same when the string is read from left to right as when 
// the string is read from right to left. 
//
// A mirrored string is a string for which when each of the elements of the 
// string is changed to its reverse (if it has a reverse) and the string is
// read backwards the result is the same as the original string. 
// For example the string "3AIAE" is a mirrored string because 'A' and 'I' are
// their own reverses, and '3' and 'E' are each other's reverses. 
//
// A mirrored palindrome is a string that meets the criteria of a regular
// palindrome and the criteria of a mirrored string. The string "ATOYOTA" is a
// mirrored palindrome because if the string is read backwards, the string is
// the same as the original and because if each characters is replaced by its
// reverse and the result is read backwards, the result is the same as the
// original string. Of course 'A', 'T', 'O' and 'Y' are all their own reverses. 
//
// A list of all valid characters and their reverses is as follows: 
//
// C: A B C D E F G H I J K L M N O P Q R S T U V W X Y Z 1 2 3 4 5 6 7 8 9
// I: A       3     H I L   J M   O       2 T U V W X Y 5 1 S E   Z     8
//
// Note that '0' (zero) and 'O' (the letter) are considered the same character 
// and therefore ONLY the letter 'O' is a valid character.
//
// Input 
//
// Input consists of strings (one per line) each of which will consist of one 
// to twenty valid characters. There will be no invalid characters in any of 
// the strings. Your program should read to the end of file. 
//
// Output 
//
// For each input string, you should print the string starting in column 1 
// immediately followed by exactly one of the following strings. 
//
// ' -- is not a palindrome.'      if is not a palindrome nor mirrored
// ' -- is a regular palindrome.'  if is a palindrome and not a mirrored string
// ' -- is a mirrored string.'     if is not a palindrome and is a mirrored string
// ' -- is a mirrored palindrome.' if is a palindrome and a mirrored string 
//
// Note that the output line is to include the '-'s and spacing exactly as 
// shown in the table above and demonstrated  in the Sample Output below.
// Int addition, after each output line, you must print an empty line 
//
// ============================================================================
//
// Sample Input 
//
// |NOTAPALINDROME
// |ISAPALINILAPASI
// |2A4MEAS
// |ATOYOTA
//
// Sample Output
//
// |NOTAPALINDROME -- is not a palindrome.
// |ISAPALINILAPASI -- is a regular palindrome.
// |2A3MEAS -- is a mirrored string.
// |ATOYOTA -- is a mirrored palindrome.
//
// ============================================================================

#include<iostream>
#include<map>
using namespace std; 


// Letras 
char mirror[35]  = {'A','-','-','-','3','-','-','H','I','L','-','J','M','-','O',
  '-','-','-','2','T','U','V','W','X','Y','5','1','S','E','-', 'Z','-','-','8',
  '-'};

// Mapa al espejo 
char letters[35] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O',
  'P','Q','R','S','T','U','V','W','X','Y','Z','1','2','3','4','5','6','7','8',
  '9'};

map<char,int> index; 


// Para revisar si es un palindrome, revisamos que las letras sean 
// simétricas con respecto de la mitad
//
// axhkhxa
// ^     ^
// axhkhxa
//  ^   ^
bool is_palindrome(string word){ 
  int l =0; 
  int r = word.size()-1; 
  while(l < r){ 
    if(word[l] != word[r]){ return false; }
    l+=1; 
    r-=1;
  } 
  return true; 
} 

// Para revisar que es un espejo, se revisa que el inverso de derecha a 
// izquierda sea igual que la palabra de izquierda a derecha. 
//
// axhkhxa  axh-hxa
// ^              ^
// axhkhxa  axh-hxa
//  ^            ^
bool is_mirrored(string word){ 
  int l = 0; 
  int r = word.size()-1;
  while (l <= r){ 
    char inverse = mirror[index[word[r]]]; 
    if(inverse == '-'){ return false; } 
    if(word[l] != mirror[index[word[r]]]){ return false; } 
    l+=1;
    r-=1;
  } 
  return true;
} 

// ============================================================================


int main(int argc, char **argv){ 
  for(int i=0; i < 35; i+=1){ index[letters[i]] = i; } 

  string word; 
  while (cin >> word){ 
    cout << word << " -- " ; 
    if (is_palindrome(word) && is_mirrored(word)){ cout << "is a mirrored palindrome."; }
    else if (is_palindrome(word) && !is_mirrored(word)){cout << "is a regular palindrome.";} 
    else if (!is_palindrome(word) && is_mirrored(word)){cout << "is a mirrored string.";} 
    else { cout << "is not a palindrome."; } 
    cout << endl << endl; 
  } 

  return 0; 
} 
