// Ricardo Nieto Fuentes
// nifr91@gmail.com
//
// ==========================================================================
//
// Sum It UP --- 574
// 3.000 seconds 
//
// Given a specified total 't' and a list of 'n' integers, find all 
// distinct sums using numbers from the list that add up to 't'. For example
// if 't=4', 'n=6' and the list is '[4,3,2,2,1,1]', then there are four 
// different sums that equal 4: 4, 3+1, 2+2, and 2+1+1 ( a number can be used
// within a sum as many times as it appears in the list and a single number 
// counts as a sum). Your job is to solve this problem in general. 
//
// Input: 
//
// The input file will contain one or more test cases, one per line. Each test
// case contains 't' , the total, followed by 'n', the number of integers in 
// the list, followed by 'n' integers, x1,x2,...,xn. If 'n=0' it signals the 
// end of the input; otherwise, 't' will be a positive integer less than 
// 1_000, 'n' will be an integer between 1 and 12 (inclusive), and x1,...xn
// will be positive integers less than 100. All numbers will be separated by
// exactly one space. The numbers in each list appear in nonincreasing order, and
// there may be repetitions.
//
// Output: 
//
// For each test case, first output a line containing 'Sums of ', the total and
// a colon. Then output each sum, oner per line; if there are no sums, output
// the line 'NONE'. The numbers within each sum must appear in nonincreasing
// order. A number may be reapeated in the sum as many times as it was repeated
// in the original list. The sums themselves must be sorted in decreasing order
// based on the numbers appeared in the sum. In other words the sums must be
// sorted by their first number, sums with the first number must be sorted by
// the second; and so on. Within each test case, all sums must be distinct; the
// same sum cannot appear twice. 
//
// =============================================================================
//
// Sample Input: 
//
// |4 6 4 3 2 2 1 1
// |5 3 2 1 1
// |400 12 50 50 50 50 50 50 25 25 25 25 25 25
// |0 0
//
// Sample Output: 
//
// |Sums of 4:
// |4
// |3+1
// |2+2
// |2+1+1
// |Sums of 5:
// |NONE
// |Sums of 400:
// |50+50+50+50+50+50+25+25+25+25
// |50+50+50+50+50+25+25+25+25+25+25
//
// ============================================================================

#include<iostream>
using namespace std; 


// Solución -------------------------------------------------------------------
// 
// Generar las 2**12 posibles soluciones, pero detectar si una solución 
// ya no es factible, es decir sum(ary[]) > t, abandonar la exploración de 
// esa combinación de elementos. 
//
// Para detectar si una combinación es repetida se emplea un mapa str => bool. 
// 
// ----------------------------------------------------------------------------

// Estructura para el mapa 
struct node{ 
  int id; string k; bool v; node * l; node * r; node * p;
  node(string _k,bool _v,node * _l = nullptr, node *_r = nullptr, node * _p = nullptr):
    k(_k),v(_v),l(_l),r(_r),p(_p){ id = rand(); }
};

// Estructura para regresar dos nodos en el mapa
struct node_pair{ 
  node * l; node * r; node_pair(node * _l, node * _r):l(_l),r(_r){}
};


// Mapa string => bool 
struct nmap{ 
  node * root = nullptr; 
  node_pair split(node * root, string key, bool leq = true);
  node * merge(node * tl, node * tr);
  node * find(string key,node * root);
  void insert(string key, bool val); 
};

// Arreglo 
struct nary{ 
  int buff[1<<15] = {}; 
  int size = 0; 
  int push(int n); 
  int pop(); 
  int & at(int index); 
  void reverse(); 

  void to_cout(); 
  string to_s();
  int sum();
  bool ispartialsol(int target); 
  int perm(nary &);
} ;

// Función para comprobar la suma
int nary::sum(){ 
  int s = 0; 
  for(int i=0; i < this->size; i+=1){ s += (this->at(i));} 
  return s; 
} 

// Función para comprobar que la suma hasta el momento es menor o igual que 
// la suma requerida.
bool nary::ispartialsol(int target){ 
  int s = this->sum(); 
  return (s <= target);
} 

// Función recursiva para calcular todas las posibles soluciones, podando 
// cuando una solución no es valida.
//
// sol     : arreglo que contiene la solución
// numbers : Posibles números a utilizar
// valid   : mapa para almacenar las soluciones ya encontradas
// target  : suma a formar
// ptr     : posición en numbers a partir de la que se van a considerar los siguientes
//           numeros 
int solve(nary & sol,nary & numbers,nmap & valid,int target,int ptr){ 
  int cnt = 0;

  // Caso base, se ha encontrado la suma, si se encuentra en valid es una 
  // combinación repetida, en caso de no estar se contabiliza y se muestra
  if(sol.sum() == target){
    string str = sol.to_s();
    if(!valid.find(str,valid.root)){ 
      valid.insert(str,true); 
      cout << str << endl;
      cnt = 1;
    }
  } 

  // Podamos si la solución parcial ya ha superado el total esperado.
  if(!sol.ispartialsol(target)){return cnt;}

  // Para cada posible número probamos agregarlo a la suma
  for(int i=ptr; i < numbers.size; i+=1){
    sol.push(numbers.at(i)); 
    cnt += solve(sol,numbers,valid,target,i+1);
    sol.pop();
  } 

  // Regresamos el número de soluciones encontradas
  return cnt;
} 

// Desplegar la solución en el formato especificado 
string nary::to_s(){ 
  string s = "";
  for(int i=0 ; i< this->size-1; i+=1){ 
    s += to_string(this->at(i));
    s += "+";
  }
  s += to_string(this->at(-1));
  return s;
} 

// Driver ====================================================================

int main(int num_args, char ** args){ 
  int sum = 0; 

  while(cin >> sum ){ 
    // Leer los datos
    nary numbers; 
    int n = 0; 
    cin >> n ; 
    if(!n){ break;} 
    for(int i=0; i < n; i+=1){ 
      int e = 0; 
      cin >> e;
      numbers.push(e);
    } 


    // Resolver el problema y en caso de no encontrarse ninguna solución 
    // regresar NONE
    auto valid = nmap(); 
    nary sol; 
    cout << "Sums of " << sum << ":" << endl; 
    if (!solve(sol,numbers,valid,sum,0)){ 
      cout << "NONE" << endl;
    } 
  } 
  return 0; 
} 

// Funciones NARY =============================================================

int nary::push(int v){ 
  this->buff[this->size] = v;
  this->size += 1;
  return v;
} 

int nary::pop(){ 
  this->size -= 1;
  return this->buff[this->size]; 
} 

int & nary::at(int i){
  if(i < 0){i = this->size+i; } 
  return this->buff[i];
} 

void nary::reverse(){ 
  int l = 0; 
  int r = this->size-1;
  while(l < r){ 
    this->buff[l] ^= this->buff[r]; 
    this->buff[r] ^= this->buff[l];
    this->buff[l] ^= this->buff[r];
    l+=1; 
    r-=1;
  } 
} 

// Funciones NMAP ============================================================

node_pair nmap::split(node * root, string key, bool leq){
  node * tl = nullptr; 
  node * tr = nullptr; 
  if(!root){ return node_pair(tl,tr);} 
  bool comp = (leq) ? (key <= root->k) : (key < root->k); 
  if (comp){
    tr = root; 
    auto tpair = split(tr->l,key,leq); 
    tr->l = tpair.r;
    tl = tpair.l;
  }
  else{ 
    tl = root;
    auto tpair = split(tl->r,key,leq); 
    tl->r = tpair.l;
    tr = tpair.r;
  } 
  return node_pair(tl,tr);
} 

node * nmap::merge(node * tl, node * tr){ 
    if(!tl || !tr){ return ((tl) ? tl : tr);}
    if(tl->id > tr->id){ 
      tl->r = merge(tl->r,tr);
      return tl; 
    } 
    else { 
      tr->l = merge(tl,tr->l); 
      return tr; 
    } 
  }
node * nmap::find(string key,node * root){ 
  if(!root || (root->k == key)){return root;} 
  if(key < root->k){return find(key,root->l); } 
  return find(key,root->r);
} 

void nmap::insert(string key, bool val){ 
  node * n = new node(key,val);
  auto npair = split(this->root,key); 
  auto l = npair.l;
  auto m = npair.r;
  npair = split(m,key,false);
  m = npair.l; 
  auto r = npair.r;
  this->root = merge(merge(l,n),r);
} 

