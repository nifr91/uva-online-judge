// Ricardo Nieto Fuentes
// nifr91@gmail.com 
//
// ============================================================================
//
// Hangman Judge --- 489
//
// In "Hangman Judge", you are to write a program that judges a series of 
// HAngman games. For each game, the answer to the puzzle is given as well as
// the guesses. Rules are the same as the classic game of hangman, and are given 
// as follows: 
//
// 1. The contestant tires to solve the puzzle by guessing one letter at a time. 
// 2. Every time a guess is correct, all the characters in the word that match 
// the guess will be "tirend over". For example if your guess is 'o' and the 
// word is 'book', then both 'o's in the solution will be counted as solved. 
// 3. Every time a wrong guess is made, a stroke will be added to the drawing 
// of a hangman, which need 7 strokes to complete. Each unique wrong guess 
// only counts against the contestant once. 
// 4. If the drawing of the hangman is completed before the contestant has 
// successfully guessed all the characters of the word, the contestant loses.
//
// 5. If the contestant has guessed all the characters of the word before the
// drawing is complete, the contestant wins the game. 
//
// 6. IF the contestant does not guess enough letter to either win or lose, the
// contestant chickens out. 
// 
// Your task as the "Hangman Judge" is to determine, for each game, whether the
// contestant wins, loses, or fails to finish a game. 
//
// Input : 
// 
// Your program will be given a series of inputs regarding the status of a game.
// All input will be in lower case. The first line of each section will contain
// a number to indicate which round of the game is being played; the next line 
// will be the solution to the puzzle; the las line is a sequence of the 
// guesses made by the contestant. A round number of "-" would indicate the end
// of all games(and input). 
//
// Output: 
//
// The output of your program is to indicate which round of the game the 
// contestan is currently playing as well as the result of the game. There are
// three possible results; 
//
// You win.
// You lose.
// You chickened out.
//
// ===========================================================================
//
// Sample Input 
//
// |1
// |cheese
// |chese
// |2
// |cheese
// |abcdefg
// |3
// |cheese
// |abcdefgij
// |-1 
// 
// Sample Output
//
// |Round 1
// |You win.
// |Round 2
// |You chickened out.
// |Round 3
// |You lose.
// ===========================================================================

#include<iostream>
using namespace std; 

// Main 
int main(int argc, char ** argv){ 
  int round; 
  string answer,guesses; 

  // Por cada juego 
  while(cin >> round){ 
    if(round < 0){ break; } 

    // Obtenemos la respuesta y los movimientos del jugador
    cin >> answer >> guesses; 

    // Contamos cuantas veces se equivoca y cuantas letras se han revelado
    int wrong = 0; 
    int revealed = 0; 
    
    // Solo se cuenta una vez cada letra
    bool repeated[26];
    for(int i=0; i < 26; i+=1){ repeated[i] = false;} 

    // Para cada movimiento del jugador
    for(int k=0; k < (int)guesses.size(); k+=1){ 
      // Si ya ha intentado esa letra no hacemos nada  
      if(repeated[guesses[k]-'a']){ continue;} 
      
      // Se revisa cada letra de la respuesta, si esta se revela y se actualiza
      // la bandera 'correct'
      bool correct = false; 
      for(int l=0; l < (int)answer.size();l+=1){ 
        if((answer[l] == guesses[k]) ){ 
          revealed += 1; 
          correct = true; 
          repeated[guesses[k]-'a'] = true;
        } 
      } 
      // Si tenemos la respuesta el jugador ha ganado.
      if (revealed == (int)answer.size()){ break;}
      
      // Si el jugador se equivocó en la letra se aumenta el número de errores
      if (!correct ){
        wrong +=1;
        repeated[guesses[k]-'a'] = true;
      } 
    } 

    // Finalmente se muestra la información si tiene >= 7 errores pierde 
    // si no tiene 7 errores pero no encontró la respuesta se acobardo y 
    // de lo contrario ha ganado el juego 
    cout << "Round " << round << endl; 
    if(wrong  >= 7){ cout << "You lose." << endl;}
    else if( (wrong < 7) && (revealed < (int)answer.size())){ 
      cout << "You chickened out." << endl; } 
    else {  cout << "You win." << endl;  } 
  } 
  return 0;
} 
