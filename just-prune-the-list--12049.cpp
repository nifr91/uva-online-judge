// Ricardo Nieto Fuentes
// nifr91@gmail.com
//
// ============================================================================
//
// Just Prune The list 
// 1.000 seconds 
//
// You are given two list of integers. You can remove any number of elements 
// from any of them. You have to ensure that after removing some elements both
// of the list will contain same elements, but not necessarily in same order. 
// For example consider the following two lists: 
//
// |1 2 3 2 1
// |1 2 5 2 3
//
// After removing 1 from first list and 5 from second list, both list will 
// contain the same elements. We will find the following list after removing
// two elements
//
// |1 2 3 2
// |1 2 2 3
//
// What is the minimum number of elements to be removed to obtain two list 
// having same elements? 
//
// Input: 
//
// The first line of the input file contains an integer 'T' (T <= 100) which 
// denotes the total number of test cases. The description of each test case
// is given below: 
//
// First line will contain two integers 'N' (1 <= N <= 10_000), the number of 
// elements in the first list and 'M' (1 <= M <= 10_000) the number of 
// elements in the second list. The next line will contain 'N' integers 
// representing the first list followed by another line having 'M' elements
// representing the second list. Each integers in the input is 32 bit signed
// integer.
//
// Output: 
//
// For each test case output a single line containing the number of elements
// to be removed. See sample output for exact format. 
//
// ============================================================================
//
// Sample Input: 
//
// |1
// |5 5
// |1 2 3 2 1
// |1 2 5 2 3
// 
// Sample Output: 
//
// |2
//
// ============================================================================


// Solution -------------------------------------------------------------------
//
// Notemos que para hacer iguales las dos listas solo es necesario eliminar 
// la cantidad de números en las que difieren. 
// 
//  1  2  3  4  5
// [2][2][1][0][0] # 1 2 3 2 1
// [1][2][1][0][1] # 1 2 5 2 3
// ---------------
//  1  0  0  0  1  # El número mínimo de elementos a borrar es 2
//
// Para hacer eficiente el conteo, se usan mapas: 
//
//               K    =>   V 
// dos mapas : número => conteo  , uno para cada lista 
// dos mapas : número => número  , para pode acceder de forma eficiente a 
//                                 las llaves del mapa de conteo 
//
// Entonces solo consiste en para cada número sumar la diferencia entre 
// el conteo de ambas listas
// ----------------------------------------------------------------------------

#include<iostream> 
#define loop while(true)

// Mapa K => V
template <class K, class V> struct nmap{ 

  // Estructura de nodo 
  struct node{ 
    int id; K k; V v; node * l; node * r; node * p;
    node(K _k,V _v,node * _l = nullptr, node *_r = nullptr, node * _p = nullptr):
      k(_k),v(_v),l(_l),r(_r),p(_p){ id = std::rand(); }
  };

  // Estructura para regresar dos nodos en el mapa
  struct node_pair{ 
    node * l; node * r; node_pair(node * _l, node * _r):l(_l),r(_r){}
  };


  // Raíz 
  node * root = nullptr; 

  // Dividir el árbol por un valor ( los valores < o <= en un árbol 
  // (l) y los demás en otro (r). Ambos árboles mantienen la propiedad del 
  // treap
  node_pair split(node * root, K & key, bool leq = true){
    node * tl = nullptr; 
    node * tr = nullptr; 
    if(!root){ return node_pair(tl,tr);} 
    bool comp = (leq) ? (key <= root->k) : (key < root->k); 
    if (comp){
      tr = root; 
      auto tpair = split(tr->l,key,leq); 
      tr->l = tpair.r;
      tl = tpair.l;
    }
    else{ 
      tl = root;
      auto tpair = split(tl->r,key,leq); 
      tl->r = tpair.l;
      tr = tpair.r;
    } 
    return node_pair(tl,tr);
  } 

  // Combinar dos árboles (l y r) que tienen la propiedad del treap en 
  // un árbol, se cumple que para el árbol l los valores son < que los 
  // valores r
  node * merge(node * tl, node * tr){ 
    if(!tl || !tr){ return ((tl) ? tl : tr);}
    if(tl->id > tr->id){ 
      tl->r = merge(tl->r,tr);
      return tl; 
    } 
    else { 
      tr->l = merge(tl,tr->l); 
      return tr; 
    } 
  }

  // Encontrar en O(lg n) un valor en el árbol 
  node * find(K & key,node * root){ 
    if(!root || (root->k == key)){return root;} 
    if(key < root->k){return find(key,root->l); } 
    return find(key,root->r);
  } 

  // Agregar un elemento al árbol  en O(lg n)
  void insert(K key, V val){ 
    node * n = new node(key,val);
    auto npair = split(this->root,key); 
    auto l = npair.l;
    auto m = npair.r;
    npair = split(m,key,false);
    m = npair.l; 
    auto r = npair.r;
    this->root = merge(merge(l,n),r);
  } 

  // Eliminar un elemento al árbol  en O(lg n)
  void remove(K key){ 
    auto npair = split(this->root,key); 
    auto l = npair.l; 
    auto m = npair.r; 
    npair = split(m,key,false); 
    m = npair.l; 
    auto r = npair.r; 
    this->root = merge(l,r); 
    free_tree(m); 
  } 

  // Liberar la memoria de un árbol
  void free_tree(node * n ){ 
    if (n){ 
      free_tree(n->l); 
      free_tree(n->r); 
      delete n; 
    } 
  } 
};

// ============================================================================

int main(int num_args, char ** args){ 

  // Leer el número de casos
  int test_cases = 0; 
  std::cin >> test_cases; 

  for(int t=0; t < test_cases; t+=1){ 
    // Leer el tamaño de las listas 
    int n = 0; 
    int m = 0; 
    std::cin >> n >> m; 

    // Leer las listas, contando los números 
    nmap<int,int> amap; 
    nmap<int,int> akeys; 
    int alist[n] = {}; 
    for(int i=0; i < n; i+=1){
      std::cin >> alist[i];
      auto e = amap.find(alist[i],amap.root); 
      if(e){ e->v += 1;} 
      else{
        akeys.insert(alist[i],alist[i]); 
        amap.insert(alist[i],1);
      } 
    }

    nmap<int,int> bmap; 
    nmap<int,int> bkeys; 
    int blist[m] = {};
    for(int i=0; i < m; i+=1){ 
      std::cin >> blist[i];
      auto e = bmap.find(blist[i],bmap.root); 
      if(e){ e->v +=1 ; } 
      else{
        bkeys.insert(blist[i],blist[i]); 
        bmap.insert(blist[i],1);
      } 
    } 


   
    // Para cada número sumar las diferencias 
    int min_deletions = 0; 
    while(akeys.root){ 
      auto ak = akeys.root->v;
      int acnt = amap.find(ak,amap.root)->v;

      auto be = bmap.find(ak,bmap.root); 
      int bcnt = (be) ? be->v : 0; 

      min_deletions += abs(acnt - bcnt);
  
      akeys.remove(ak); 
      bkeys.remove(ak); 
      bmap.remove(ak);
      amap.remove(ak);
    }
    while(bkeys.root){ 
      auto bk = bkeys.root->v;
      int bcnt = bmap.find(bk,bmap.root)->v;

      auto ae = amap.find(bk,amap.root); 
      int acnt = (ae) ? ae->v : 0; 

      min_deletions += abs(acnt - bcnt);
  
      akeys.remove(bk); 
      bkeys.remove(bk); 
      bmap.remove(bk);
      amap.remove(bk);
    } 

    // Mostrar el número mínimo de elementos que deben eliminarse
    std::cout << min_deletions << std::endl; 
  } 

  return 0; 
} 

