// Ricardo Nieto Fuentes
// nifr91@gmail.com 
//
// ============================================================================
//
// Grocery store --- 11236
//
// A cashier in a grocery store seems to have difficulty in distinguishing the 
// multiplication symbol and the addition symbol. To make things easier for 
// him, you want to buy goods in such a way that the product of their prices
// is the same as the sum of their prices. 
//
// Of course, if you buy only one item, this is always true. With two items
// and three items, it still seems quite a boring task to you, so now 
// you are interested in finding possible prices of four items such that the
// sum of the four prices is equal to the product of the four prices. You 
// should consider the prices are in euros; with two digits after the 
// decimal point. Obviously, each product cost at least one cent. 
//
// Input
//
// This problem has no input 
//
// Output
//
// Print all solutions which have a sum of the four items of at most 20.00 euros
// For each solution, print one line with the prices of the four items in non 
// decreasing order, with one space character between them. You may print the 
// solution in any order, but make sure to print each solution only once. 
//
// ============================================================================
//
// Sample Input 
// - 
// 
// Sample Output 
//
// |0.50 1.00 2.50 16.00
// |1.25 1.60 1.75 1.84
// |1.25 1.40 1.86 2.00
// |...
//
// ============================================================================

#include <iostream>
using namespace std; 
#define ll long long

//-----------------------------------------------------------------------------
// Solución propuesta 
// 
// Una búsqueda del espacio de posibles combinaciones de valores e + f + g + h
// tales que sean iguales que e * f * g * h. 
//
// Note que para evitar errores de punto flotante podemos utilizar centavos de 
// forma que 
//
// a = e*100 
// b = f*100 
// c = g*100
// d = h*100
//
// Al comparar el producto y la suma tenemos la siguiente ecuación  :
//
// |a + b + c + d     a*b*c*d 
// |-------------  = ---------
// |    100           10**8
//
// simplificando vemos que tenemos que cumplir la siguiente restricción : 
//
// |10**6 (a+b+c+d) = a*b*c*d
//
// Se pueden realizar distintas optimizaciones para no revisar todo el espacio
// de búsqueda que es O(2000**4). 
//
// * Optimización 1 
//   Como solo se pide que se imprima una sola vez, se requieren las 
//   combinaciones no las permutaciones. De esta forma cada valor va a
//   ir empezando del anterior 
//   a = 1
//   b = a 
//   c = b
//   d = c
// 
// * Optimización 2 
//   La segunda restricción es que los valores deben sumar <= 2000, esto 
//   nos impone un límite superior 
//   a < 2000
//   b < 2000-(a)
//   c < 2000-(a+b)
//   d < 2000-(a+b+c)
//  
// * Optimización 3
//   La tercera restricción es el producto de los valores debe ser <= 2000
//   como 
//
//   | a < b < c < d 
//
//   tenemos que 
//
//   |a**4 < a*b*c*d  ==>   a** < a*b*c*d < 2*10**9 ==> a**4 < 2*10**9
//   |a*b**3 < 2*10**9
//   |a*b*c**2 < 2*10**9
//   |a*b*c*d < 2**10**9
//
// * Optimización 4
//   Notar que una vez se define a,b,c es posible calcular d. 
//
//   |let k = 10**6
//
//   de la ecuación k*(a+b+c+d) = a*b*c*d obtenemos 
//
//   |
//   |     (a+b+c)*k
//   |d = -------------
//   |    (a*b*c - k)
//   
//   lo que disminuye la complejidad a O(n**3)
//-----------------------------------------------------------------------------


// Diver ======================================================================

int main(int argc, char ** argv){ 

  ll k = 1000000; 
  for(    ll a=1; (a <=  2000)      && ((a*a*a*a) <=  2000000000);  a+=1){ 
    for(  ll b=a; (b <= (2000-a))   && ((a*b*b*b) <= (2000000000)); b+=1){ 
      for(ll c=b; (c <= (2000-a-b)) && ((a*b*c*c) <= (2000000000)); c+=1){ 

        ll p = a*b*c; 
        ll s = a+b+c;
        
        // Despejando de la ecuación
        //  d = (a+b+c)*k / (a*b*c - k )
        if ((p <= k )){continue;}
        ll d = (s*k)/(p-k);

        // Verificar que d tenga un valor aceptable
        if ((d < c) || d > 2000){ continue;} 

        // Sumar 
        s += d;
        if ((s > 2000)){continue;}

        // multiplicar 
        p *= d;
  
        // verificar que la suma y el producto sean iguales 
        // (a+b+c+d) * k  ==  a*b*c*d
        if (p==(s*k)){ 
          cout 
            << (a/100) << "." << ((a/10)%10) << (a%10) 
            << " "      
            << (b/100) << "." << ((b/10)%10) << (b%10) 
            << " "     
            << (c/100) << "." << ((c/10)%10) << (c%10) 
            << " "    
            << (d/100) << "." << ((d/10)%10) << (d%10) 
            << endl;
        } 
      } 
    } 
  } 
  return 0; 
} 
