// Ricardo Nieto Fuentes
// nifr91@gmail.com 
//
// ============================================================================
//
// Cancer or Scorpio  --- 11947
//
// Alice and Michael is a young couple who are planning on having their first
// child. Their wish their son Nelson was born on a special date for both of 
// them. 
//
// Alice has investigated in the internet and has found that the period of 
// gestation is forty weeks. These forty weeks begin to count on the first day
// of the las menstrual cycle. 
//
// Michael is passionate about astrology and even more about the zodiac signs, 
// he has asked Alice to investigate the range of dates that correspond to each
// sign. 
//
// sign        | begin  | end
// ------------+--------+-------
// aquarius    | jan 21 | feb 19
// pisces      | feb 20 | mar 20
// aries       | mar 21 | apr 20
// taurus      | apr 21 | may 21
// gemini      | may 22 | jun 21
// cancer      | jun 22 | jul 22
// leo         | jul 23 | aug 21
// virgo       | aug 22 | sep 23
// libra       | sep 24 | oct 23
// scorpio     | oct 24 | nov 22
// sagittarius | nov 23 | dec 22
// capricorn   | dec 23 | jan 20
// ------------+--------+-------
//
// Alice and Michael ask for help to calculate the date of birth of their son 
// Nelson and his zodiac sign. 
//
// Intput 
//
// The first line of input contains a single integer 'N' (1 <= N <= 1000) which 
// is the  number of datasets that follow. 
// Each dataset consists of a single line of input that contains only eight 
// digits that represents the date of the first day of the last 
// menstrual cycle in format MM DD YYYY.
//
// Output 
//
// For each dataset, you should generate one line of output with the following 
// values: The dataset number as a decimal integer (start counting at one), a
// space, the date of birth in format MM/DD/YY, a space, and the name 
// (in lowercase) of zodiac sign that correspond according tot he date of birth. 
//
// /!\ Note Consider leap years. 
//
// ===========================================================================
//
// Sample Input 
// 
// |2
// |01232009
// |01232008
//
// Sample Output
// 
// |1 10/30/2009 scorpio
// |2 10/29/2008 scorpio
//
// ===========================================================================
//

#include<iostream>
using namespace std; 

// Estructura para manejar la fecha 
struct date{ 
  int day; 
  int month; 
  int year; 
  int days_in_year = 365; 
  int days_in_month[13] = {0,31,28,31,30,31,30,31,31,30,31,30,31};

  // Constructor 
  date(int _day, int _month, int _year):day(_day),month(_month),year(_year){ 
    this->update_leap_year(); 
  } 

  // Actualizar el número de días dependiendo de si es año bisiesto 
  void update_leap_year(){ 
    if(is_leap_year()){ this->days_in_year = 366; days_in_month[2] = 29;}
    else{ days_in_year = 365; days_in_month[2] = 28;} 
  } 
 
  // Calcular el número de días que quedan en el año 
  int remainig_days_in_year(){ 
    int sum = 0; 
    for(int i=1; i < this->month; i+=1){ sum += days_in_month[i]; } 
    sum += this->day;
    return (this->days_in_year - sum); 
  } 

  // Calcular el número de días que quedan en el mes
  int remainig_days_in_month(){ 
    return(this->days_in_month[this->month] - this->day);
  }

  // Mover al siguiente año la fecha 
  int next_year(){ 
    int advanced = this->remainig_days_in_year() + 1;
    this->day = 1; 
    this->month = 1; 
    this->year += 1; 
    this->update_leap_year();
    return advanced; 
  } 

  // Mover al siguiente mes la fecha
  int next_month(){ 
    if (this->month == 12){ return this->next_year();}
    int advanced = this->remainig_days_in_month() + 1;
    this->day = 1;
    this->month +=1;
    return advanced; 
  } 

  // Recorrer 'n' días la fecha
  void advance_days(int n){ 
    while(n > 0) { 
      if( n > remainig_days_in_year()){ n -= this->next_year();}
      else if(n > remainig_days_in_month()){ n -= this->next_month();}
      else { 
        this->day += n; 
        n=0; 
      } 
    } 
  } 

  // Calcular el signo zodiacal dependiendo de la fecha 
  string zodiac(){ 
    int d = this->day; 
    int m = this->month; 
    if(((m==1  && d >= 21) || (m==2  && d <= 19))){return "aquarius";}
    if(((m==2  && d >= 20) || (m==3  && d <= 20))){return "pisces";}
    if(((m==3  && d >= 21) || (m==4  && d <= 20))){return "aries";}
    if(((m==4  && d >= 21) || (m==5  && d <= 21))){return "taurus";}
    if(((m==5  && d >= 22) || (m==6  && d <= 21))){return "gemini";}
    if(((m==6  && d >= 22) || (m==7  && d <= 22))){return "cancer";}
    if(((m==7  && d >= 23) || (m==8  && d <= 21))){return "leo";}
    if(((m==8  && d >= 22) || (m==9  && d <= 23))){return "virgo";}
    if(((m==9  && d >= 24) || (m==10 && d <= 23))){return "libra";}
    if(((m==10 && d >= 24) || (m==11 && d <= 22))){return "scorpio";}
    if(((m==11 && d >= 23) || (m==12 && d <= 22))){return "sagittarius";}
    if(((m==12 && d >= 23) || (m==1  && d <= 20))){return "capricorn";}
    return "-"; 
  } 


  // Determinar si es un año bisiesto 
  bool is_leap_year(){
    return ((((this->year % 4) == 0 ) && 
             ((this->year % 100) != 0)) ||
            ((this->year % 400) == 0)); 
  } 

  // Regresar una cadena de texto con el formato especificado 
  string to_s(){ 
    string str = ""; 
    str += (this->month < 10) ? "0" : ""; 
    str += to_string(this->month);
    str += "/"; 
    str += (this->day < 10) ? "0" : ""; 
    str += to_string(this->day);
    str += "/"; 
    str += to_string(this->year); 
    return str; 
  } 
} ;

// ============================================================================

int main(int argc, char ** argv){ 
 
  // Leer el número de casos
  int test; 
  cin >> test; 
  
  // Para cada caso 
  for(int i=0; i < test; i+=1){ 
    // Leer la fecha
    int num; 
    cin >> num; 

    // Generar el objeto 
    auto d = date((num/10000) % 100,(num/1000000) % 100,num % 10000); 

    // Avanzar 40 semanas 
    d.advance_days(280);

    // Imprimir el día 
    cout << (i+1) << " "  << d.to_s()  << " "  << d.zodiac() << endl;  
  } 

  return(0); 
} 
