// Ricardo Nieto Fuentes
// nifr91@gmail.com 
//
// ===========================================================================
//
// Minesweeper  --- 10189 
//
// Have you ever played Minesweeper? It's a cute little game which comes within
// a certain Operating System which name we can't really remember. Well, the 
// goal of the game is to find where are all the mines within a MxN field. To
// help you the game shows a number in a square which tells you how many mines
// there are adjacent to that square. For instance, suppose the following 
// 4x4 field with 2 mines ( which are represented by an '*' character): 
//
// *...
// ....
// .*..
// ....
//
// If we would represent the same field placing the hint numbers described avobe
// , we would end up with: 
//
// *100
// 2210
// 1*10
// 1110
//
// As you may have already noticed, each square may have at most 8 adjacent 
// squares. 
//
// Input 
//
// The input will consist of an arbitrary number of fields. The first line 
// contains two integers n and m (0 < n,m <= 100) which stands for the 
// number of lines and columns of the field respectively. The next 'n' lines
// contains exactly m character and represents the field. 
// 
// Each safe square is represented by an '.' character (without the quites)
// and each mine square is represented by an '*' character ( also without the 
// quotes). The first field line where n = m = 0 represents the end of the 
// input and should not be precessed. 
//
// Output 
//
// For each field, you must print the following message in a line alone: 
// Field #x: 
// Where x stands for the number of the field (starting from 1). The next n
// lines should contain the field with the '.' character replaced by the number
// of adjacent mines to that square. There must be an empty line between field 
// outputs. 
//
// ============================================================================
//
// Sample Input 
//
// |4 4
// |*...
// |....
// |.*..
// |....
// |3 5
// |**...
// |.....
// |.*...
// |0 0
//
// Sample Output
//
//
// |Field #1:
// |*100
// |2210
// |1*10
// |1110
// |
// |Field #2:
// |**100
// |33200
// |1*100
//
// ============================================================================

#include <iostream>
using namespace std; 

int main(int argc, char ** argv){ 

  // Matrices para almacenar los datos
  int  mat[100][100]; 
  char field[100][100]; 

 
  
  int nrows,ncols; 
  int field_num = 1; 
  while(cin >> nrows>> ncols){ 
    // Leer el campo 
    if(((nrows == 0) && (ncols == 0))){ break; } 
    for(int r=0; r < nrows; r+=1){ 
      for(int c=0; c < ncols; c+=1){ 
        mat[r][c] = 0; 
        cin >> field[r][c]; 
      } 
    } 

    if (field_num != 1){ cout << endl; }

    // calcular el número de minas adyacente a una celda, para ello nos 
    // colocamos en cada mina y sumamos 1 a los vecinos
    for(int r=0; r < nrows; r+=1){ 
      for (int c=0; c < ncols; c+=1){ 
        if (field[r][c] == '*'){
          for(int k=r-1; k <= r+1; k+=1){ 
            for(int l=c-1; l <= c+1; l+=1){ 
              if( (0 <= l) && (0 <= k) && (k < nrows) && (l < ncols)){ 
                mat[k][l]+=1;
              } 
            } 
          } 
        } 
      } 
    } 
    // Imprimir el resultado 
    cout << "Field #" << field_num <<":"<< endl; 
    for(int r=0; r < nrows; r+=1){ 
      for(int c=0; c < ncols; c+=1){ 
        if(field[r][c] == '*'){ cout << field[r][c]; } 
        else { cout << mat[r][c]; } 
      } 
      cout << endl; 
    } 

    field_num +=1; 
  } 

} 
