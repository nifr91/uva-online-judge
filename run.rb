#!/usr/bin/env ruby

FLAGS = "-std=c++11 -Wall -D_DEBUG -D_GLIBCXX_DEBUG -Wfatal-errors -O2"

file = ARGV[0] 
test_num = ARGV[1]

name = File
  .basename(file,File.extname(file))
  .sub(/\.+$/,"")

puts "Reviewing #{name} "

puts "Compile program"
puts `g++ #{name}.cpp #{FLAGS}`

puts "Collect tests"
tests = `find . -type 'f' -name '#{name}.input*'`
  .each_line
  .map{|s| s.strip}
  .to_a
  .sort!
puts "found #{tests.size} tests"

passed_tests = 0; 
test_summary = []; 
tests.each do |test| 
  next unless test =~ /\.in.*#{test_num}/
  puts `./a.out < #{test} > a.output`
  diff = `diff --report-identical-files #{test.sub('input','output')} a.output`

  test_name = test[/input.*/]
  if diff[/are identical/]
    puts diff
    str = "OK::#{test_name}"
    passed_tests += 1 
  else 
    puts `diff --side-by-side #{test.sub('input','output')} a.output`
    wrong_lines = `diff -y --suppress-common-lines #{test.sub('input','output')} a.output | wc -l`.to_i
    str = "FAILED::#{test_name} -- diff with ##{wrong_lines} lines"
  end 
  test_summary.push str; 

  `rm a.output`
end 

puts `rm a.out`
puts 
puts "Summary"
puts test_summary.join("\n");
puts "Passed #{passed_tests} of #{tests.size} tests"
