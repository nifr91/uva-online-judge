// Ricardo Nieto Fuentes
// nifr91@gmail.com 
//
// ============================================================================
//
// Uncompress --- 245
// 3.000 seconds 
//
// A simple scheme for creating a compressed version of a text file can be used 
// for files which contain no digit characters. The compression scheme requires
// making a list of the words in the uncompressed file. When a non-alphabetic
// character is encountered in the uncompressed file, it is copied directly 
// into the compressed file. When a word is encountered in the uncompressed 
// file it is copied directly into the compressed file only if is the first 
// occurrence of the word. In that case, the word is put at the front 
// of the list. If it is not the first occurrence, the word is not copied
// to the compressed file. Instead, its position in the list is copied into the
// compressed file and the word is moved tot he front of the list. The 
// numbering of list positions begins at 1.
//
// Write a program that takes a compressed file as input and generates a 
// reproduction of the original uncompressed file as output.
//
// Input: 
//
// You can assume that no word contains more than 50 characters and that the 
// original uncompressed file contains no digit characters. For the 
// purposes of this problem, a word is defined to be a maximal sequence of 
// upper- and lower- case letters. Words are case-sensitive -- the word 'abc'
// is not the same as the word 'Abc'. For example 
// 
// x-ray contains 2 words: x and ray
// Mary's contains 2 words: Mary and s
// It's a winner contains 4 words: IT and s and a and winner.
//
// There is no upper limit on the number of different words int the input 
// file. The end of the input file is signified by the number '0' on a line by
// itself. The terminating '0' merely indicates the end of the input and 
// should not be part of the output produced by your program.
// 
// Output: 
//
// See the sample output below for details of the output format.
//
// ===========================================================================
// 
// Sample Input
//
// |Dear Sally,
// |   Please, please do it--1 would 4
// |Mary very, 1 much. And 4 6
// |8 everything in 5's power to make
// |14 pay off for you.
// |
// |  -- Thank 2 18 18---
// |0
//
// Sample Output: 
//
// |Dear Sally,
// |   Please, please do it--it would please
// |Mary very, very much. And Mary would
// |do everything in Mary's power to make
// |it pay pay off for you.
// |
// |   -- Thank you very much --
//
//=============================================================================


// Solución -------------------------------------------------------------------
// 
//  Emplear una lista ligada simple para almacenar las palabras y realizar las
//  operaciones
// ----------------------------------------------------------------------------

#include<iostream>
#include <ctype.h>
using std::cout; 
using std::cin; 
using std::endl; 
using std::string;

struct nlist{ 
  // Estructura nodo 
  struct nnode {
    string word;
    nnode * next;
  };

  // Cabeza de la lista 
  nnode * root = nullptr;

  // Obtener una palabra actualizando su posición en la lista, 
  // cuando se imprime se agrega al inicio de la lista 
  string get_word(int num){ 
    
    // Recorrer la lista hasta el número de palabra especificado
    nnode *now = root
    nnode *prev = nullptr;
    for(int i = 1; i < num; i+=1) {
      prev = now;
      now = now->next;
    }

    // Se elimina el nodo de la lista
    if(prev != nullptr) {prev->next = now->next;}

    // Se agrega al inicio de la lista
    if(root != now) { now->next = root; }
    root = now;

    return now->word;
  } 

  // Se agrega una palabra a la lista 
  void insert_word(string word){ 
    // Recorrer la lista hasta el final de la lista o encontrar la palabra
    nnode *now = root
    nnode *prev = nullptr;
    while(now != nullptr) {
      if(now->word == word)
          break;
      prev = now;
      now = now->next;
    }

    // Agregar la palabra al inicio de la lista, si se encontró se elimina de la 
    // cadena y se agrega al inicio. Si no se encontró simplemente se agrega 
    // al inicio de la lista.
    if(now != nullptr) {
      if(prev != nullptr){prev->next = now->next;}
      now->next = root;
      root = now;
    } 
    else{
      nnode *now = new nnode();
      now->word = word;
      now->next = root;
      root = now;
    }
  } 
};

// Obtener un número
int get_num(string & line, int & pos){ 
  int num = 0; 
  while(isdigit(line[pos])){ 
    num = num*10 + line[pos]-'0'; 
    pos +=1;
  }
  pos -= 1; 
  return num; 
} 

// Obtener una palabra
string get_word(string & line, int & pos){ 
  string word = "";
  while(isalpha(line[pos])) {
    word += line[pos];
    pos += 1;
  }
  pos -= 1;
  return word;
} 

// ===========================================================================

int main() {
  std::string line;
  nlist list; 

  // Obtener cada línea de la entrada 
  while(getline(cin, line)) {
    // Se termina si el contenido de la línea es 0 solo
    if(line == "0"){ break; } 
    int i, len = line.length();

    // Para cada letra en la línea
    for(i = 0; i < len; i++) {
      // Si es un número o 
      if(!isalpha(line[i])) {
        // Si es el inicio de un número lo extraemos
        if(isdigit(line[i])) {
          // Obtener el número 
          int num = get_num(line,i); 
          // Obtener la palabra en la lista 
          // actualizando 
          string word = list.get_word(num);
          // Imprimir
          cout << word; 
        } 
        // Si es un símbolo o espacio simplemente se imprime
        else{ cout << line[i]; }
      }
      // Si es una palabra 
      else{
        // leer una palabra
        string word = get_word(line,i); 
        // mostrar la palabra 
        cout << word;
        // agregar la palabra a la lista
        list.insert_word(word);
      }    
    }
    cout << endl;
 }
  return 0;
}


