// Ricardo Nieto Fuentes 
// nifr91@gmail.com 
//
// ============================================================================
// 
// Dart A Mania --- 735
// 3.000 seconds
//
// The game of darts has many variations. One such variation is the game of 301.
// In the game of 301 each player starts with a score of 301 (hence the name). 
// Each player, in turn, throws three darts to score points which are 
// subtracted from the player's current score. For instance, if a player has a
// current score of 272 and scores 55 points with the three darts, the new 
// score would be 217. Each dart that is tossed may strike regions on the 
// dartboard that are numbered between 1 and 20. ( A value of zero indicates 
// that the player either missed the dartboard altogether or elected to not 
// throw the dart.) A dart that strikes on one of these regions will either 
// score the number printed on the dartboard, double the number printed, or 
// triple the number printed. For example a player may score 17,34 or 51 points
// with a toss of one dart that hits one of the regions marked with a 17. A 
// third way to score points with one dart is to hit the BULLS EYE which is 
// worth 50 points. (There is no provision for doubling or tripling the 
// bull's eye score.) 
//
// The first player to reduce his score to exactly zero wins the game. If a
// player scores more points than his/her current score. The player is said to
// have 'busted' and the new score is returned to the last current score. 
//
// Given a player's current dart score, write a program to calculate all the
// possible combinations and permutations of scores on throwing three darts
// that would reduce the player's score to exactly zero (meaning the player won
// the game). The output of the program should contain the number of
// combinations and permutations found. 
//
// For example if the player's current score is 2, then there would be two 
// combinations and six permutations. The combinations would be: 
//
// * Obtain a score of 2 on any one dart and zero on the other two, and 
// * obtains a score of one on two different darts and zero on the third dart. 
//
// The order in which this is accomplished is not important. 
//
// With permutations the order is significant; therefore the six permutations 
// would be as follows: 
//
// 2 0 0
// 0 2 0
// 0 0 2
// 1 1 0
// 1 0 1
// 0 1 1
//
// Note the program doesn't print out the actual permutations and combinations,
// just the total number of each. 
//
//
// Input:
//
// The input file contains a list of integers (each <= 999), one per line, that 
// represents several player's current scores. A value of zero or less will 
// signify the end of the input file. 
//
// Output: 
//
// For each positive integer in the input file, 2 or 3 lines will be written to
// the output file. 
//
// If the score can be reduced to zero, your program should write the lines: 
//
// |NUMBER OF COMBINATIONS THAT SCORES x IS c.
// |NUMBER OF PERMUTATIONS THAT SCORES x IS P.
//
// where 'x' is the value of the player's score while 'c' and 'p' are the total
// number of combinations and permutations possible, respectively. 
//
// If it is impossible to reduce the player's score to zero, write the line: 
//
// |THE SCORE OF x CANNOT BE MADE WITH THREE DARTS. 
//
// After the lines above are printed, your program should write a line of 70 
// asteriks to separate output for different scores.  The message 
//
// |END OF OUTPUT
//
// should appear at the end of the output file. 
//
// ============================================================================
//
// Sample Input
//
// |162
// |175
// |2
// |68
// |211
// |114
// |-100
//
// Sample Output 
//
// |NUMBER OF COMBINATIONS THAT SCORES 162 IS 7.
// |NUMBER OF PERMUTATIONS THAT SCORES 162 IS 28.
// |**********************************************************************
// |THE SCORE OF 175 CANNOT BE MADE WITH THREE DARTS.
// |**********************************************************************
// |NUMBER OF COMBINATIONS THAT SCORES 2 IS 2.
// |NUMBER OF PERMUTATIONS THAT SCORES 2 IS 6.
// |**********************************************************************
// |NUMBER OF COMBINATIONS THAT SCORES 68 IS 187.
// |NUMBER OF PERMUTATIONS THAT SCORES 68 IS 1056.
// |**********************************************************************
// |THE SCORE OF 211 CANNOT BE MADE WITH THREE DARTS.
// |**********************************************************************
// |NUMBER OF COMBINATIONS THAT SCORES 114 IS 82.
// |NUMBER OF PERMUTATIONS THAT SCORES 114 IS 445.
// |**********************************************************************
// |END OF OUTPUT
//
// ============================================================================

#include<iostream> 
using namespace std; 


//------------------------------------------------------------------
// Solución propuesta 
//
// Generar todas las posibles sumas para las permutaciones y 
// a partir de esa matriz contar el número de permutaciones y 
// combinaciones.
// -----------------------------------------------------------------

// Función para intercambiar dos valores
void swap(int * a , int *b){ 
  *a ^= *b; 
  *b ^= *a; 
  *a ^= *b;
} 

int main(int argc, char ** argv){ 

  // Generamos los posibles valores 
  int NSCORES = (20*3) + 1 + 1; 
  int SCORES[NSCORES];
  for(int i=0; i < NSCORES; i+=1){SCORES[i] = 1 << 20; } 
  for(int i=1; i <= 20; i+=1){ 
    SCORES[i] = i; 
    SCORES[i*2] = i*2;
    SCORES[i*3] = i*3;
  }
  SCORES[0] = 0; 
  SCORES[NSCORES-1] = 50;

  // Generamos todas las posibles sumas de las permutaciones en los dardos
  int sum[NSCORES][NSCORES][NSCORES]; 
  for(int k=0; k < NSCORES; k+=1){ 
    for(int l=0; l < NSCORES; l+=1){ 
      for(int m=0; m < NSCORES; m+=1){ 
        sum[k][l][m] = SCORES[k] + SCORES[l] + SCORES[m]; 
      } 
    } 
  } 


  int score; 
  bool first = true; 
  while (cin >> score){ 
    // Imprimir si es fin de caso
    if(first){ first = false; } 
    else { 
      for(int i=0; i < 70; i+=1){ cout << "*" ; } 
      cout << endl; 
    }

    // Imprimir si es fin de instancia 
    if (score <= 0){ 
      cout << "END OF OUTPUT" << endl; 
      break; 
    } 

    // Limpiar el número de combinaciones
    int combinations[NSCORES][NSCORES][NSCORES];
    for(int k=0; k < NSCORES; k+=1){ 
       for(int l=k; l < NSCORES; l+=1){ 
         for(int m=l; m < NSCORES; m+=1){ 
           combinations[k][l][m] = 0; 
         } 
       } 
    } 

    // Contar el número de permutaciones, ordenamos a < b < c para 
    // obtener las permutaciones
    int perm = 0; 
    for(int k=0; k < NSCORES; k+=1){ 
      for(int l=0; l < NSCORES; l+=1){ 
        for(int m=0; m < NSCORES; m+=1){ 
          if (score == sum[k][l][m]){
            int a = k; 
            int b = l; 
            int c = m; 
            if (b < a){swap(&a,&b);} 
            if (c < a){swap(&a,&c);} 
            if (c < b){swap(&b,&c);}
            combinations[a][b][c] = 1;
            perm +=1;
          } 
        } 
      } 
    }

    // Contar el número de combinaciones
    int comb = 0; 
    for(int k=0; k < NSCORES; k+=1){ 
      for(int l=k; l < NSCORES; l+=1){ 
        for(int m=l; m < NSCORES; m+=1){ 
          if(combinations[k][l][m]){ 
            comb +=1; 
          }
        } 
      } 
    } 


    // Imprimir el resultado 
    if(comb){ 
      cout << "NUMBER OF COMBINATIONS THAT SCORES " << score << " IS " << comb << "." << endl; 
      cout << "NUMBER OF PERMUTATIONS THAT SCORES " << score << " IS " << perm << "." << endl;
    }else{ 
      cout << "THE SCORE OF " << score << " CANNOT BE MADE WITH THREE DARTS." << endl;
    } 
  } 
  
  return 0; 
} 
