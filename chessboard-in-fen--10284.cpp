// Ricardo Nieto Fuentes 
// nifr91@gmail.com 
//
// ============================================================================
//
// Chessboard in FEN --- 10284
//
// In the FEN ( Forsyth-Edwards Notation), a chessboard is described as follows:
//
// * The board-content is specified starting with the top row and ending with 
// the bottom row.
//
// * Character '/' is used to separated data of adjacent rows.
//
// * Each row is specified from lef to right.
//
// * White pieces are identified by uppercase piece letters: PNBRQK
//
// * Black Pieces are identified by lowercase piece letters: pnbrqk
//
// * Empty squares are represented by the number one through eight. 
//
// * A number used represents the count of contiguous empty squares along a 
// row. 
// 
// * Each row's sum of numbers and characters must be equal 8. 
//
// As for example: 
//
// 5k1r/2q3p1/p3p2p/1B3p1Q/n4P2/6P1/bbP2N1P/1K1RR3
//
// is the FEN notation description of the following chessboard: 
//
// 1 [ ][ ][ ][ ][ ][k][ ][r]
// 2 [ ][ ][q][ ][ ][ ][p][ ]
// 3 [p][ ][ ][ ][p][ ][ ][p]
// 4 [ ][B][ ][ ][ ][p][ ][Q]
// 5 [n][ ][ ][ ][ ][P][ ][ ]
// 6 [ ][ ][ ][ ][ ][ ][P][ ]
// 7 [b][b][P][ ][ ][N][ ][P]
// 8 [ ][K][ ][R][R][ ][ ][ ]
//    1  2  3  4  5  6  7  8
//
// The chessboard of the beggining of a chess game is described in FEN as: 
//
// rnbqkbnr/ppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR
//
// Your task is simple given a chessboard description in a FEN notation you are
// asked to compute the number of unnoccupied squares on the board which are 
// not attacked by any pieces. 
//
// Input
//
// Input is a sequence of lines, each line containing a FEN description of a 
// chessboard. Note that the description does not necessarily give a legal 
// chess position. Input lines do not contain whitespace. 
//
// Output
//
// For each line of input, output one line containing an integer which gives 
// the number of unoccupied squares which are not attacked. 
//
// ============================================================================
//
// Sample Input 
//
// |5k1r/2q3p1/p3p2p/1B3p1Q/n4P2/6P1/bbP2N1P/1K1RR3
// |rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR
// 
// Sample Output 
//
// |3
// |16
//
// ============================================================================

#include<iostream>
using namespace std; 

// Solo se puede reemplazan las casillas que estén vacías 
void attack_cell(char * cell){ 
  if(cell[0] == '.'){ cell[0] = 'x'; } 
} 

// Revisamos que este dentro de los límites
bool in_bounds(int r , int c){ 
  return( (0 <= r) && (r < 8) && (0 <= c) && ( c < 8)); 
} 

// Función para atacar las piezas, importante notar que para la reina, torre y
// alfil debe detenerse el ataque si las casillas están bloqueadas por una 
// pieza.
void attack(char piece, int r, int c, char **board){
 
  // Rey ataca todas las casillas a distancia 1
  if (piece == 'k' || piece == 'K'){
    if(in_bounds(r-1,c-1)){attack_cell(&board[r-1][c-1]);} 
    if(in_bounds(r-1,c)){attack_cell(&board[r-1][c]); }  
    if(in_bounds(r-1,c+1)){attack_cell(&board[r-1][c+1]);}  
    if(in_bounds(r,c-1)){attack_cell(&board[r][c-1]);}  
    if(in_bounds(r,c+1)){attack_cell(&board[r][c+1]); }  
    if(in_bounds(r+1,c-1)){attack_cell(&board[r+1][c-1]);}  
    if(in_bounds(r+1,c)){attack_cell(&board[r+1][c]); }  
    if(in_bounds(r+1,c+1)){attack_cell(&board[r+1][c+1]);}  
  }
 
  // Peones atacan las dos casillas enfrente de ellos
  if(piece == 'p'){ 
    if(in_bounds(r+1,c-1)){attack_cell(&board[r+1][c-1]);} 
    if(in_bounds(r+1,c+1)){attack_cell(&board[r+1][c+1]); } 
  }
  if(piece == 'P'){ 
    if(in_bounds(r-1,c-1)){attack_cell(&board[r-1][c-1]);} 
    if(in_bounds(r-1,c+1)){attack_cell(&board[r-1][c+1]); } 
  }

  // La torre y la reina atacan horizontalmente y verticalmente
  if(piece == 'r' || piece == 'R' || piece == 'Q' || piece == 'q'){ 
    for(int k =c-1; (k >= 0); k-=1){ 
      if(!((board[r][k] == '.')||(board[r][k] == 'x'))){ break;} 
      attack_cell(&board[r][k]); 
    } 
    for(int k =c+1; (k <  8); k+=1){ 
      if(!((board[r][k] == '.')||(board[r][k] == 'x'))){ break;} 
      attack_cell(&board[r][k]); 
    } 
    for(int k =r-1; (k >= 0); k-=1){ 
      if(!((board[k][c] == '.')||(board[k][c] == 'x'))){ break;} 
      attack_cell(&board[k][c]); 
    } 
    for(int k =r+1; (k <  8); k+=1){ 
      if(!((board[k][c] == '.')||(board[k][c] == 'x'))){ break;} 
      attack_cell(&board[k][c]); 
    } 
  } 

  // El caballo ataca en L 
  if(piece == 'n' || piece == 'N'){ 
    if(in_bounds(r-1,c-2)){attack_cell(&board[r-1][c-2]);} 
    if(in_bounds(r-2,c-1)){attack_cell(&board[r-2][c-1]); }  
    if(in_bounds(r-1,c+2)){attack_cell(&board[r-1][c+2]);} 
    if(in_bounds(r-2,c+1)){attack_cell(&board[r-2][c+1]); }  

    if(in_bounds(r+1,c-2)){attack_cell(&board[r+1][c-2]);} 
    if(in_bounds(r+2,c-1)){attack_cell(&board[r+2][c-1]); }  
    if(in_bounds(r+1,c+2)){attack_cell(&board[r+1][c+2]);} 
    if(in_bounds(r+2,c+1)){attack_cell(&board[r+2][c+1]); }  
  }

  // El alfil y la reina atacan en diagonal 
  if(piece == 'b' || piece == 'B' || piece == 'Q' || piece == 'q'){ 
    for(int k =c-1, l=r-1; in_bounds(l,k); k-=1,l-=1){ 
      if(!((board[l][k] == '.')||(board[l][k] == 'x'))){ break;} 
      attack_cell(&board[l][k]); 
    } 
    for(int k =c+1, l=r+1; in_bounds(l,k); k+=1,l+=1){ 
      if(!((board[l][k] == '.')||(board[l][k] == 'x'))){ break;} 
      attack_cell(&board[l][k]); 
    } 
    for(int k =c-1, l=r+1; in_bounds(l,k); k-=1,l+=1){ 
      if(!((board[l][k] == '.')||(board[l][k] == 'x'))){ break;} 
      attack_cell(&board[l][k]); 
    } 
    for(int k =c+1, l=r-1; in_bounds(l,k); k+=1,l-=1){ 
      if(!((board[l][k] == '.')||(board[l][k] == 'x'))){ break;} 
      attack_cell(&board[l][k]); 
    }
  }
} 

// Main ========================================================================

int main(int argc, char ** argv){ 

  // Generamos el tablero
  char ** board = new char*[8];
  for(int i=0; i < 8; i+=1){ 
    board[i] = new char[8]; 
    for(int j=0; j < 8; j+=1){ board[i][j] = '.'; } 
  } 

  // Leer las distintas configuraciones
  string line; 
  while (cin >> line){ 

    // Leer la configuración del tablero  '/' salto de linea '0-9' casillas 
    // vacías
    int r = 0; 
    int c = 0; 
    for(int i=0; i < (int)line.size(); i+=1){ 
      char val = line[i]; 
      if (val == '/'){ 
        r += 1; 
        c = 0;
      }else if (val-'0' < 9){ 
        for(int j=0; j < val-'0'; j+=1){ 
          board[r][c] = '.'; 
          c += 1; 
        } 
      }else { 
        board[r][c] = val; 
        c+=1; 
      } 
    }

    // Atacar las casillas
    for(int r=0; r < 8; r+=1){ 
      for(int c=0; c < 8; c+=1){ 
        attack(board[r][c],r,c,board);
      } 
    } 

    // Contar el número de casillas vacías
    int cnt = 0; 
    for(int r=0; r < 8; r+=1){ 
      for(int c=0; c < 8; c+=1){ 
        cnt += (board[r][c] == '.') ? 1 : 0; 
      } 
    } 

    // Mostrar el resultado 
    cout << cnt << endl;

  } 

  return 0; 
} 

