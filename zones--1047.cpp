// Ricardo Nieto Fuentes
// nifr91@gmail.com 
//
// Telephone poles are part of an outdated technology. Cell phones nowadays 
// allow us to call whoever we want, wherever we want, independent of any wire.
// There is one problem -without a service tower nearby a cell phone is useless.
//
// In the absence of hills and mountains, a service tower will provide coverage 
// for a circular area. Instead of planning where to place the wires, a wireless
// telephone company has to plan where to build its service towers. Building the
// towers too far apart causes holes in the coverage and increases complaints. 
// Building the towers too close to each other results in large areas covered 
// by more than one service tower, which is redundant and inefficient. 
//
// International Cell Phone Company is developing a network strategy to
// determine the optimal placement of services towers. Since most customers 
// have replaced their old wired home phones by cell phones, the strategy for 
// planning service towers is therefore to cover as many homes of customers as
// possible. 
//
// The figure below shows the service area for the five towers ICPC's strategic
// department planned to build this year. Tower 5 will serve 24 customers,
// 6 of which are also served by tower 4. Towers 1,2 and 4 have a common 
// service area containing 3 customers. 
//
//
//
//
// |        tower 2      tower 3     tower 4               |
// |          20           25          30                  |
// |           ,-----.  ,-----.     ,-----.                |
// |        ,-'      ,-'.      `-,-'       `-.             |
// |       /        /    \  15  / \           \            |
// |      /   8    /      \    /   \   19  ,-----.         |
// |     ;        ;   2    :  ;     :   ,-'     : `-.      |
// |     |  ,-----.        |  |  5  |  /        |    \     |
// |     ,-'      :`-.     ;  :     ; /         ;     \    |
// |    / \        \  \   /    \   / ;     6   /       :   |
// |   /   \   7    \3 \ /      \ /  |        /        |   |
// |  ;     `-.      `-.:      ,-`-. :     ,-'         ;   |
// |  |        `-----'  |-----'     `-\---'     18    /    |
// |  :                 ;              \             /     |
// |   \      5        /                `-.       ,-'      |
// |    \             /                    `-----'         |
// |     `-.       ,-'                                     |
// |        `-----'                         tower 5        |
// |        tower 1                            24                              
// |          15                                                               
//
//
// Shortly after the plans for these five towers had been published, higher
// management issued a stop on al tower building. Protesting customers forced
// them to weaken this decree, and allow the building of three towers out of
// the five planned. These three towers should serve as many customers as
// possible, of courser. Finding the best possible choice for the towers to
// build is not trivial (the best choice in this case is tower 2,4 and 5,
// serving 68 customers). 
//
// You must write a program to help ICPC choose which towers to build in cases
// like these. If several choices of towers serve the same number of customers,
// choices including tower 1 are preferable. If this rule still leaves room for
// more than one solution, solutions including tower 2 are preferable, and so
// on.
//
// Input : 
//
// The input file contains several test cases. The first line of each test case
// contains two positive integers: then number 'n' (n <= 20) of towers planned,
// and the number of towers to be actually built. The next line contains 'n'
// numbers, each giving the number of customers served by a planned tower. (The
// first number refers to the first tower and so on). No tower serves more than
// a million people. The next line contains the number 'm' (m <= 10) of common
// service areas. Each of the next 'm' lines contains the description of a
// common service area. Such a line starts with the number 't' (t > 1) of
// towers for which this is a common service area, followed by the 't' numbers
// of the towers. The last number on the line is the number of customers in the
// common service area. The last line of the input consists of the two integers
// '0 0'. 
//
// Output : 
//
// For each test case, display the number of customers served and the best 
// choice for the location of the towers. Follow the format of the sample
// output.  Print a blank line after each test case. 
//
// ============================================================================
//
// Sample Input
//
// |5 3
// |15 20 25 30 24
// |5
// |2 1 2 7
// |3 1 2 3 3
// |2 2 3 2
// |2 3 4 5
// |2 4 5 6
// |5 3
// |25 25 25 25 25
// |4
// |2 1 2 5
// |2 2 3 5
// |2 3 4 5
// |2 4 5 5
// |5 3
// |25 25 25 25
// |0
// |0 0
//
// Sample Output : 
//
// |Case Number 1
// |Number of Customers: 68
// |Locations recommended: 2 4 5
// |
// |Case Number 2
// |Number of Customers: 75
// |Locations recommended: 1 3 5
// |
// |Case Number 3
// |Number of Customers: 75
// |Locations recommended: 1 2 3
// |
//
//=============================================================================

#include<iostream> 
// #include<vector>
using namespace std; 

// Solución -------------------------------------------------------------------
// 
// La principal idea es generar todas las posibles combinaciones de las torres
// en orden lexicográfico. 
// Contar el número de clientes que abarcan
// Regresar el máximo 
//
// Se debe considerar las intersecciones para evitar contribuciones duplicadas
// de las regiones. 
//
// ----------------------------------------------------------------------------

// Vector con funciones tipo pila para la implementación de las combinaciones
template <class T> struct  vect{ 
  int size = 0; 
  // Buffer
  T buff[30] = {};
  // Agregar un elemento al final 
  T push(T val){
    this->buff[this->size] = val;
    this->size += 1; 
    return val; 
  } 
  // Eliminar el último elemento 
  T pop(void){ 
    this->size -= 1; 
    if(this->size < 0){this->size = 0;}
    return this->buff[this->size]; 
  } 
  // Obtener un elemento 
  T at(int index){ 
    if(index < 0){index = this->size+index;}
    return this->buff[index];
  } 

  // Regresa true si no hay elementos
  bool isempty(void){return this->size == 0;} 
}; 


// Driver ---------------------------------------------------------------------


int main(int argc,char ** argv){ 


  int num_towers_planned = 0;
  int num_towers = 0; 
  int case_cnt = 1;

  // Leer el número de torres 
  while (cin >> num_towers_planned >> num_towers){ 
    // Terminamos si tenemos la línea 0 0
    if((num_towers_planned == 0) && (num_towers==0)){ break;} 

    // Leer los clientes que abarcan cada torre
    int customers[num_towers_planned]; 
    for(int t=0; t < num_towers_planned; t+=1){ cin >> customers[t]; }

    // Leer las áreas, el vector common tendrá un vector que contendrá 
    // 0 si la torre no tiene área en común y el número de clientes compartidos
    // si comparte el área.
    int num_common_area = 0;
    cin >> num_common_area; 
    int common[num_common_area][num_towers_planned] = {};
    for(int a=0; a < num_common_area; a+=1){

      // Inicializar las torres
      for(int t=0; t < num_towers_planned; t+=1){common[a][t] = 0;}
        
      // Leer que torres comparten área
      int area_num_towers; 
      cin >> area_num_towers;
      for(int t=0; t < area_num_towers; t+=1){ 
        int tower; 
        cin >> tower; 
        common[a][tower-1] = 1;
      } 

      // Asignar la población 
      int pop; 
      cin >> pop;
      for(int t=0; t < num_towers_planned; t+=1){ 
        if(common[a][t] != 0){common[a][t] = pop;}
      } 
    }
    
    // Combinaciones de torres
    int number_of_customers = 0;
    int locations_recommended[num_towers] = {};
    for(int i=0; i < num_towers; i+=1){ locations_recommended[i] = 0; } 

    auto stack = vect<int>(); 
    stack.push(-1);
    while(!stack.isempty()){
      while(stack.at(-1) < num_towers_planned-1){
        int val = stack.pop(); 
        stack.push(val+1);
        if(stack.size < num_towers){ stack.push(val+1); }
        else { 
          // Si hemos completado una combinación, se obtiene el 
          // número de clientes que cubre.

          int cover_pop = 0;
          // Sumar el total de clientes dado por las torres
          for(int t=0; t < num_towers; t+=1){ 
            cover_pop += customers[stack.at(t)];
          } 
          // Restar el excedente de las intersecciones 
          for(int a=0; a < num_common_area; a+=1){ 
            int shared = 0; 
            for(int i=0; i < num_towers; i+=1){ 
              if(common[a][stack.at(i)] != 0){
                if(shared != 0){cover_pop -= common[a][stack.at(i)];} 
                shared += 1;
              } 
            } 
          } 

          // Si mejora guardamos la combinación y el número de clientes
          if(cover_pop > number_of_customers){ 
            number_of_customers = cover_pop; 
            for(int t=0; t < num_towers; t+=1){ 
              locations_recommended[t] = stack.at(t);
            } 
          } 
        }
      }
      stack.pop(); 
    } 
 
    // Imprimir el resultado 
    cout << "Case Number  " << case_cnt << endl; 
    cout << "Number of Customers: " << number_of_customers << endl; 
    cout << "Locations recommended:" ; 
    for(int t=0; t < num_towers; t+=1){
      cout << " " << (locations_recommended[t]+1); 
    } 
    cout << endl; 
    cout << endl; 
    cout.flush();
    case_cnt += 1; 
  } 

  return 0; 
} 
