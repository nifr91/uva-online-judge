// Ricardo Nieto Fuentes
// nifr91@gmail.com
// ======================================================================
//
// Y2K Accounting Bug
// 3.000 seconds 
//
// Accounting for computer Machinists (ACM) has suffered from the Y2K bug and
// lost some vital data for preparing annual report for MS Inc. 
//
// All what they remember is that MS Inc. posted a surplus or a deficit each
// month of 1999 and each month when MS Inc. posted surplus, the amount of
// surplus was 's' and each month when MS Inc. posted deficit, the deficit
// was 'd'. They do not remember which or how many months posted surplus or
// deficit. MS Inc., unlike other companies, post their earnings for each 
// consecutive 5 months during a year. ACM knows that each of these 8 postings
// reported a deficit but they do not know how much. 
// 
// The chief accountant is almost sure that MS Inc. was about to post surplus
// for the entire year of 1999. Almost but not quite.
//
// Write a program, which decides whether MS Inc. suffered a deficit during 
// 1999, or if a surplus for 1999 was possible, what is the maximum amount of
// surplus that they can post. 
//
// Input: 
//
// Input is a sequence of lines, each containing two positive integers 's' and
// 'd'. 
//
// Output: 
//
// For each line of input, output one line containing either a single integer
// giving the amount of surplus for the entire year, or output 'Deficit' if it
// is impossible.
//
// ======================================================================
//
// Sample Input
//
// |59 237
// |375 743
// |200000 849694
// |2500000 8000000
//
// Sample Output
// 
// |116
// |28
// |300612
// |Deficit
//
// ======================================================================

#include<iostream>
using namespace std;

// Solución propuesta -----------------------------------------------------
// 
// Notar que lo que se pide es que combinación de s y d en los 12 meses 
// maximiza la suma de los meses, tal que durante los últimos 8 meses 
// la suma de los 5 meses anteriores (4+actual) es negativa. 
//
// Ejemplo 
//
// s: 50 d: -237
//
// month |    profit   |               last_5_month_reports
//     1 |       59    |                                  _
//     2 |       59    |                                  _
//     3 |       59    |                                  _
//     4 |       59    |                                  _
//     5 |     -237    |   59 +  59 +  59 +  59 - 237 =  -1
//     6 |       59    |   59 +  59 +  59 - 237 +  59 =  -1
//     7 |       59    |   59 +  59 - 237 +  59 +  59 =  -1
//     8 |       59    |   59 - 238 +  59 +  59 +  59 =  -1
//     9 |       59    | -237 +  59 +  59 +  59 +  59 =  -1
//    10 |     -237    |   59 +  59 +  59 +  59 - 237 =  -1
//    11 |       59    |   59 +  59 +  59 - 237 +  59 =  -1
//    12 |       59    |   59 +  59 - 237 +  59 +  59 =  -1
//-------+-------------+------------------------------------
//       |             | Como todos los reportes fueron negativos 
//       |             | en los ocho meses se reporto deficit
//-------+-------------+------------------------------------
//  total|    116      |
//       | Maximizar   |
//
// La manera de realizar esto es ir probando las posibles combinaciones 
// de tales que se maximice la suma y se cumpla que los reportes 
// sean negativos.
// ------------------------------------------------------------------------


// Vector tipo stack para almacenar los valores
struct vect{ 
  int size; 
  int buff[12]; 
  // Agregar elemento al final 
  int push(int val){ 
    this->buff[this->size] = val; 
    this->size +=1; 
    return val;
  } 
  // Eliminar último elemento 
  int pop(void){ 
    this->size -= 1; 
    return this->buff[this->size]; 
  } 
  // Regresar el elemento en el índice 'index'
  int at(int index){ return buff[index]; } 
};


// Búsqueda completa, que poda cuando el reporte de los últimos cinco meses es
// positivo. 
//
int search(vect & v,int s, int d){ 

  // Verificar que los últimos cinco meses den un deficit
  if(v.size >= 5){ 
    int sum =0; 
    for(int i=v.size-1; i >= v.size-5; i-=1){ sum += v.at(i); } 
    if(sum >= 0){return 0;} 
  } 

  // Caso base, al tener los doce meses regresamos el valor
  if(v.size == 12){ 
    int sum  = 0; 
    for(int i=0; i < v.size; i+=1){ sum += v.at(i);} 
    return sum;
  } 

  int sum = -1;

  // Elegir mes con deficit 
  v.push(-d); 
  sum = max(sum,search(v,s,d));
  v.pop();

  // Elegir mes con surplus 
  v.push(s); 
  sum = max(sum,search(v,s,d));
  v.pop();  

  return sum; 
} 

// Driver -----------------------------------------------------------------

int main(int argc, char ** argv) {
  int s = 0; 
  int d = 0;
  while(cin >> s >> d){
    auto v = vect();
    int sol = search(v,s,d); 
    cout << ((sol <= 0) ?  "Deficit" : to_string(sol) )<< endl; 
  } 
  return 0; 
} 
