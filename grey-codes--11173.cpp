// Ricardo Nieto Fuentes
// nifr91@gmail.com 
//
// ===========================================================================
// 
// Grey Codes --- 11173 
// 3.000 seconds 
//
// We are going to generate a sequence of integers in binary. Start with the
// sequence 
// 
// |0
// |1
//
// Reflect it in the horizontal line, prepend a zero to the numbers in the top
// half and a one to the numbers on the bottom and you will get
//
// |00
// |01
// |11
// |10
//
// Repeat this and you will have 8 numbers 
//
// 0|000
// 1|001
// 3|011
// 2|010
// 6|110
// 7|111
// 5|101
// 4|100
// 
// The corresponding decimal values are shown on the right. 
//
// These sequences are called Reflected gray codes for 1,2 and 3 bits 
// respectively. A gray code for 'n' bits is a sequence of 2**n different n-bit
// integers with the property that every two neighbouring integers differ
// in exactly one bit. A reflected gray code is a gray code constructed in the
// same way shown above. 
//
// Input: 
// 
// The first line of input gives the number of test cases, 'N' (at most 
// 250_000). N test cases follow. Each one is a line with 2 integers: 
// 'n' (1 <= n <= 30) and 'k' (0 <= k < 2**n). 
//
// Output: 
//
// For each test case, output the integer that appears in position 'k' of the
// n-bit reflected Gray Code. 
//
// ===========================================================================
//
// Sample Input: 
//
// |14
// |1 0
// |1 1
// |2 0
// |2 1
// |2 2
// |2 3
// |3 0
// |3 1
// |3 2
// |3 3
// |3 4
// |3 5
// |3 6
// |3 7
//
// |0
// |1
// |0
// |1
// |3
// |2
// |0
// |1
// |3
// |2
// |6
// |7
// |5
// |4
//
// ===========================================================================


// Solution -------------------------------------------------------------------
// 
// Código Gray : código binario reflejado, fue diseñado originalmente para 
// prevenir señales (falsas o viciadas) en la representación de los switches
// electromecánicos y actualmente es usado para facilitar la corrección de 
// errores en los sistemas de comunicación.
//
//
// Para convertir número k en su representación de número gray se realiza : 
//
// k xor (k // 2)
//
// Para convertir un número g en representación gray a número binario. 
//
// mask = g // 2; 
// while (mask != )
//   g = g xor mask 
//   mask = mask // 2
// end 
//
// Nos piden convertir el número 'k' en su representación gray, simplemente 
// aplicamos la función anterior.
// ----------------------------------------------------------------------------

#include<iostream>
#include<bits/stdc++.h>
using namespace std; 


int main(int num_args, char ** args){ 
  ios_base::sync_with_stdio(false);
  cin.tie(NULL);

  // Leer el número de casos
  int test_cases = 0; 
  cin >> test_cases; 

  // Para cada caso 
  for(int t=0; t < test_cases; t+=1){ 
    // Leer los dígitos n y el número k.
    int n = 0; 
    int k = 0; 
    cin >> n >> k; 

    // Calcular su codificación gray
    int sol = k ^ (k >> 1); 
    cout << sol << endl;
  } 

  return 0; 
} 
