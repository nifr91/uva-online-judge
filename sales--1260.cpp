// Ricardo Nieto Fuentes
// nifr91@gmail.com
//
// ============================================================================
//
// Sales --- 1260
//
// Mr. Cooper, the CEO of CozyWalk Co., receives a report of daily sales every
// day since the company has been established. Starting from the second day 
// since its establishment, on receiving the report, he compares it with 
// each of the previous reports in order to calculate the number of precious 
// whose sales amounts are less than or equal to it. After obtaining the 
// number of such days, he writes it in a list. 
//
// This problem can be stated more formally as follows. Let A=(a_1,a_2,...,a_n)
// denote the list of daily sales amounts. And let B=(b_1,b_2,...,b_n-1) be 
// another integer list maintained by Mr. Cooper, each value representing the 
// number of such previous days. On the i-th day (2 <= i <= n), he calculates
// b_(i-1), the number of a_k's such that a_k <= a_i (1 <= k < i). 
//
// For example, suppose that A=(20,43,57,43,20). For the fourth day's sales 
// amount a_4 = 43, the number of previous days whose sales amounts are less
// than or equal to it is 2 since a_1 <= a_4, a_2 <= a_4 and a_3 > a_4. 
// Therefore, b_3 = 2. Similarly, b_1,b_2, and b_4 can be obtained and its 
// results in B = (1,2,2,1). 
//
// Given an array of size 'n' for the list of daily sales amounts, write a 
// program that prints the sum of the n-1 integers in the list B. 
//
// Input 
//
// Your program is to read the input from standard input. The input consists of 
// T test cases. The number of test cases 'T' is given in the first line of the
// input. Each test case starts with a line containing an integer n 
// (2 <= n <= 1_000), which represents the size of the list A. In the following 
// line, 'n' integers are given, each represents the daily sales amounts
// a_i (1 <= a_i <= 5_000) and (1 <= i <= n) for the test case. 
//
// Output 
//
// Your program is to write to standard output. For each test case, print the 
// sum of the n-1 integers in the list B which is obtained from the list A.
//
// The following shows sample input and output for two test cases. 
//
// ===========================================================================
//
// Sample Input
//
// |2
// |5
// |38 111 102 111 177
// |8
// |276 284 103 439 452 276 452 398
//
// Sample Output
//
// |9
// |20
//
// ===========================================================================

#include<iostream>
using namespace std; 

int main(int arg, char ** argv){ 
  
  int test_cases; 
  cin >> test_cases; 

  int a[1000]; 
  for(int t=0; t < test_cases; t+=1){

    // Leer los datos de entrada
    int n; 
    cin >> n;
    for(int i=0; i < n; i+=1){ cin >> a[i]; }

    // Calcular el número total de entradas que son <= que la i-ésima en a[i]
    int sum = 0;
    for(int i=0; i < n; i+=1){ 
      for(int j=0; j < i ; j+=1){ 
        if(a[j] <= a[i]){ sum += 1;} 
      } 
    } 
    
    cout << sum << endl; 
  } 

  return 0; 
} 
