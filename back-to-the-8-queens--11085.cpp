// Ricardo Nieto Fuentes
// nifr91@gmail.com 
//
// ===========================================================================
//
// Back to the 8 Queens --- 11085
// 3.000 seconds
//
// You are given a chess board (of 8x8 dimension) and there are 8 queens placed
// randomly on the board. 
//
// Each of the 8 queens is in different columns and that means that no two 
// queens  are attacking each other vertically. But some queens are attacking
// each other horizontally and/or diagonally. You have to move the queens
// so that no two queens are attacking each other form any direction. You 
// are allowed to move the queens vertically an this you can only change row
// positions of each queen and not the column. 
//
// A move consists of moving a queen from (R1,C) to (R2,C) where
//  1<= R1,R2<=8 and R1!=R2. 
//
// You have to find the minimum number of moves required to complete the task. 
//
// Input: 
//
// There will be multiple test cases. Each case consists of a line containing
// 8 integers. All these integers will be in the range [1,8]. The ith integer
// indicates the row position of a queen in the ith column. 
//
// Output:
//
// For each case, output the case number followed by the required output. 
//
// Constraints: 
//
// Total number of test cases will be less than 1_000. 
//
// ============================================================================
//
// Sample Input
//
// |1 2 3 4 5 6 7 8
// |1 1 1 1 1 1 1 1
//
// Sample Output
//
// |Case 1: 7
// |Case 2: 7
//
// ============================================================================

#include<iostream>
using namespace std; 

// Solución propuesta --------------------------------------------------------
// 
// Note que es posible ir construyendo las posibles soluciones e ir eliminando 
// cuando no se cumplen las condiciones. 
//
// Es decir, se puede probar colocar todas las posiciones y ver cual 
// permutación válida es la que requiere menos movimientos, si se va 
// construyendo la solución podemos podar un conjunto de permutaciones que
// sean inválidas.
//
// ---------------------------------------------------------------------------

// Vector tipo stack para almacenar los valores
struct vect{ 
  int size; 
  int buff[8]; 
  // Agregar elemento al final 
  int push(int val){ 
    this->buff[this->size] = val; 
    this->size +=1; 
    return val;
  } 
  // Eliminar último elemento 
  int pop(void){ 
    this->size -= 1; 
    return this->buff[this->size]; 
  } 
  int & operator[](int index){ return this->buff[index]; } 
};


// Notemos que en la representación para que no se ataquen dos reinas, se debe
// verificar la diagonal, si la diferencia entre la posición perm[i] de la
// columna i  y la posición perm[j] de la columna j es exactamente cnt (la
// diferencia de sus columnas) la solución es invalida pues significa que la
// reina de la columna i y la de la columna j se están atacando.  De igual 
// forma se debe verificar que las filas no tienen valores iguales. 
//
bool is_sol(vect & s){ 
  bool valid = true; 
  for(int i=0; (i < s.size) && valid ; i+=1){ 
    for(int j=i+1,cnt=1; (j<s.size) && valid; j+=1){ 
      if((abs(s[j] - s[i]) == cnt) || (s[j] == s[i])){valid = false;} 
      cnt+=1; 
    } 
  } 
  return valid;
} 

// Función generativa para contar el número mínimo de movimientos 
//
int solve(vect & v,vect & s){ 
  // Poda, si no es una solución válida. 
  if (!is_sol(s)){return 1<<30;} 
  
  // Caso base, si llegamos a una permutación válida, entonces contamos 
  // la cantidad de movimientos requeridos 
  if(v.size == s.size){ 
    int cnt_moves = 0; 
    for(int i=0; i < v.size; i+=1){
      if(v[i] != s[i]) {cnt_moves += 1;}
    }
    return cnt_moves; 
  } 


  // Probar mover la reina incluyendo dejarla en la posición
  int cnt_moves = 1<<30; 
  for(int i=0; i < 8; i+=1){ 
    s.push(i); 
    cnt_moves = min(cnt_moves,solve(v,s)); 
    s.pop();
  }

  // Regresar el mínimo de movimientos necesarios 
  return cnt_moves ; 
} 

// Driver --------------------------------------------------------------

int main(int argc, char ** argv){ 
  int row;
  int case_cnt = 1;
  while (cin >> row){ 
    auto v = vect(); 
    v.push(row-1);
    for(int i=1; i < 8; i+=1){ 
      cin >> row; 
      v.push(row-1); 
    } 

    auto sol = vect(); 
    cout << "Case " << case_cnt << ": " << solve(v,sol)  << endl; 
    case_cnt += 1;
  } 
  return 0; 
} 
