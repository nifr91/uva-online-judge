// Ricardo Nieto Fuentes
// nifr91@gmail.com 
//
// ============================================================================
//
// Interval Product --- 12532 
// 2.000 seconds 
//
// Its normal to feel worried and tense the day before a programming contests.
// To relax, you went out for a drink with some friends in a nearby pub. To 
// keep your mind sharp for the next day, you decided to play the following 
// game. To start, your friends will give you a sequence of 'N' integers
// x1,x2,....,Xn. Then there will be K rounds; at each round, your friends will
// issue a command, which can be: 
//
// . a change command, when your friends want to change one of the 
//   values in the sequence
// . a product command, when your friends five you two values I, J and ask you 
//   if the product Xl*xl+1*xj-1*xj is postive, negative or zero. 
//
// Since you are at a pub, it was decided that the penalty for a wrong answer is
// to drink a pint of beer. You are worried this could affect you negatively at 
// the next day's contest, and you dont want to check if ballmer's peak theory 
// is correct. Fortunately, your friends fave you the right to use your 
// notebook. Since you trust more your coding skills than your math, you 
// decided to write a program to help you in the game. 
//
// Input: 
//
// Each test case is described using several lines. The first line contains two
// integers 'N' and 'K', indicating respectively the number of elements in the
// sequence and the number of rounds of the game ( 1 <= N,K <= 10**5). The
// second line contains 'N' integers 'xi' that represent the initial values of
// the sequence (-100 <= xi <= 100 for i = 1,2,..n). Each of the next K lines
// describes a command and starts with an uppercase letter that is either 'C'
// or 'P'. If the letter is 'C', the line describes a *change* command, and the
// letter is followed by two integers 'i' and 'v' indicating that x_i must
// receive the value v (1 <= i <= N) and (-100 <= v <= 100). If the letter is
// 'P', the line describes a product command, and the letter is followed by two
// integers i and j, indicating that the product from xi to xj inclusive must
// be calculated (1<= i <= j <= n). Within each test case there is at least one
// product command. 
//
// Output: 
//
// For each test case output a line with a string representing the result of
// all the *product* commands in the testcase. The i-th character of the string
// represents the result of the ith *product* command. If the result of the
// command os positive the character must be '+'(plus); if the result is
// negative the character must be '-'(minues); if the result is zero the
// character must be '0'(zero). 
//
// ============================================================================
//
// Sample Input 
//
// |4 6
// |-1 6 0 -1
// |C 1 10
// |P 1 4
// |C 3 7
// |P 2 2
// |C 4 -5
// |P 1 4
// |5 9
// |1 5 -2 4 3
// |P 1 2
// |P 1 5
// |C 4 -5
// |P 1 5
// |P 4 5
// |C 3 0
// |P 1 5
// |C 4 -5
// |C 4 -5
//
// Sample Output:
//
// |0+-
// |+-+-0
// 
// ============================================================================


// Solution -------------------------------------------------------------------
//
// Implementar un árbol de segmentos con actualización, donde se almacena 
// -1 1 o 0 (el resultado del producto)
//
// ----------------------------------------------------------------------------

#include<iostream>

// Implementación del árbol de segmentos 
struct nstree{ 
  
  // Intervalo inclusivo l..r
  struct range{ 
    int l = 0; 
    int r = 0; 
    range(){};
    range(int _l, int _r): l(_l),r(_r){}
    bool contains(range o){return ((this->l <= o.l) && (o.r <= this->r));} 
    bool intersects(range o){return ((this->l <= o.r) && (o.l <= this->r));}
  };

  // Estructura que contienen el valor y el rango (representa un nodo del arbol)
  struct node{ 
    int val = 0; 
    range rng;
    node(){}
    node(int _val,range _rng):val(_val),rng(_rng){}
  };

  // arreglo para procesar los datos debe ser 2**(log(n)-1) nodos
  node st[500000]; 
  int size = 0; 

  // Crear un árbol de segmentos de forma recursiva iniciando del nodo raíz
  int construct_st(int i, range rng, int * a){

    // Caso base si es un nodo hoja el segmento solo tiene un elemento
    if(rng.l == rng.r){ 
      int v = (a[rng.l] == 0) ? 0 : ((a[rng.l] < 0) ? -1 : 1); 
      this->st[i] = node(v,rng);
      return this->st[i].val; 
    }

    // Caso recursivo se divide en dos el segmento 
    int m = (rng.l + rng.r)/2; 
    auto vl = construct_st(this->ln(i),range(rng.l,m),a); 
    auto vr = construct_st(this->rn(i),range(m+1,rng.r),a);

    // Combinación de los valores 
    this->st[i] = node(vl*vr,rng); 
    return this->st[i].val; 
  }

  // Ver el valor en un segmento a..b
  int operator[](range rng){return at(rng,0);}
  int at(range rng, int index){ 
    auto node = this->st[index]; 
    if(rng.contains(node.rng)){ 
      return this->st[index].val; 
    }else if(rng.intersects(node.rng)){ 
      auto vl = this->at(rng,this->ln(index)); 
      auto vr = this->at(rng,this->rn(index));
      return (vl*vr);
    }else{return 1;} 
  }

  // Actualizar un valor en el arreglo, propagando la actualización hacia
  // arriba de los nodos afectados
  int update(int index, int val, int node_index ){ 
    auto node = this->st[node_index]; 
    auto rng = node.rng; 

    // caso base
    if(rng.l == rng.r){ 
      int v = (val == 0) ? 0 : ((val < 0) ? -1 : 1);
      this->st[node_index] = nstree::node(v,rng); 
      return v;
    } 

    // caso recursivo 
    int m = (rng.r + rng.l)/2; 
    auto vl = 1;
    auto vr = 1;
    // El valor esta a la derecha o a la izquierda 
    if(index <= m){ 
      vl = this->update(index,val,this->ln(node_index));
      vr = this->st[this->rn(node_index)].val;
    }else{ 
      vl = this->st[this->ln(node_index)].val;
      vr = this->update(index,val,this->rn(node_index));
    } 
    // actualizar el nodo actual
    this->st[node_index] = nstree::node(vl*vr,rng);
    return this->st[node_index].val; 
  } 

  // Operaciones para mover 
  int ln(int i){return (i<<1)+1; } 
  int rn(int i){return (i<<1)+2; } 
}; 

int main(int num_args, char ** args){ 

  // Leer el tamaño del arreglo y el número de preguntas 
  int array_len = 0; 
  int rounds = 0; 
  while(std::cin >> array_len >> rounds){  

    // Leer los datos originales
    int array[array_len] = {}; 
    for(int i=0; i < array_len; i+=1){std::cin >> array[i];} 

    // Generar el árbol de segmentos 
    nstree st; 
    st.construct_st(0,nstree::range(0,array_len-1),array); 

    // Leer las preguntas 
    for(int i=0; i < rounds; i+=1){ 
      char command = ' ';
      int a = 0; 
      int b = 0; 
      std::cin >> command >> a >> b; 

      // Modificar un elemento en el arreglo 
      if(command == 'C'){st.update(a-1,b,0);} 
      // Obtener el valor 
      else if(command == 'P'){
        int val = st[nstree::range(a-1,b-1)]; 
        std::cout << ((val == 0) ? '0' : ((val < 0) ? '-' : '+')); 
      }
    } 

    // Mostrar la información
    std::cout << std::endl;
  }

  return 0; 
} 
