template <typename T> 
struct nvect{ 
  T buffer[20000] = {}; 
  int size = 0; 
  // Agregar elemento al final 
  T push(T val){ 
    this->buffer[this->size] = val; 
    this->size +=1; 
    return val; 
  } 
  // Eliminar último elemento añadido 
  T pop( ){ 
    this->size -= 1; 
    return this->buffer[this->size]; 
  } 
  // Acceder a un elemento 
  T & operator[](int index){
    if(index < 0){ index = this->size + index; } 
    return this->buffer[index];
  } 
}; 

// Mapa K => V
template <class K, class V> 
struct nmap{ 

  // Estructura de nodo 
  struct node{ 
    int id; K k; V v; node * l; node * r; node * p;
    node(K _k,V _v,node * _l = nullptr, node *_r = nullptr, node * _p = nullptr):
      k(_k),v(_v),l(_l),r(_r),p(_p){ id = std::rand(); }
  };

  // Estructura para regresar dos nodos en el mapa
  struct node_pair{ 
    node * l; node * r; node_pair(node * _l, node * _r):l(_l),r(_r){}
  };


  // Raíz 
  node * root = nullptr; 
  int size = 0; 

  // Dividir el árbol por un valor ( los valores < o <= en un árbol 
  // (l) y los demás en otro (r). Ambos árboles mantienen la propiedad del 
  // treap
  node_pair split(node * root, K & key, bool leq = true){
    node * tl = nullptr; 
    node * tr = nullptr; 
    if(!root){ return node_pair(tl,tr);} 
    bool comp = (leq) ? (key <= root->k) : (key < root->k); 
    if (comp){
      tr = root; 
      auto tpair = split(tr->l,key,leq); 
      tr->l = tpair.r;
      tl = tpair.l;
    }
    else{ 
      tl = root;
      auto tpair = split(tl->r,key,leq); 
      tl->r = tpair.l;
      tr = tpair.r;
    } 
    return node_pair(tl,tr);
  } 

  // Combinar dos árboles (l y r) que tienen la propiedad del treap en 
  // un árbol, se cumple que para el árbol l los valores son < que los 
  // valores r
  node * merge(node * tl, node * tr){ 
    if(!tl || !tr){ return ((tl) ? tl : tr);}
    if(tl->id > tr->id){ 
      tl->r = merge(tl->r,tr);
      return tl; 
    } 
    else { 
      tr->l = merge(tl,tr->l); 
      return tr; 
    } 
  }

  // Encontrar en O(lg n) un valor en el árbol 
  node * find(K & key,node * root){ 
    if(!root || (root->k == key)){return root;} 
    if(key < root->k){return find(key,root->l); } 
    return find(key,root->r);
  } 

  // Agregar un elemento al árbol  en O(lg n)
  void insert(K key, V val){ 
    node * n = new node(key,val);
    auto npair = split(this->root,key); 
    auto l = npair.l;
    auto m = npair.r;
    npair = split(m,key,false);
    m = npair.l; 
    auto r = npair.r;
    this->root = merge(merge(l,n),r);
    this->size += 1; 
  } 

  // Eliminar un elemento al árbol  en O(lg n)
  void remove(K key){ 
    auto npair = split(this->root,key); 
    auto l = npair.l; 
    auto m = npair.r; 
    npair = split(m,key,false); 
    m = npair.l; 
    auto r = npair.r; 
    this->root = merge(l,r); 
    free_tree(m); 
    this->size -= 1; 
  } 

  // Liberar la memoria de un árbol
  void free_tree(node * n ){ 
    if (n){ 
      free_tree(n->l); 
      free_tree(n->r); 
      delete n; 
    } 
  } 

  // Obtener un arreglo con las llaves 
  nvect<K> keys(){ 
    nvect<K> keysvect; 
    this->get_keys(keysvect,this->root);  
    return keysvect; 
  } 

  // Obtener las llaves
  void get_keys(nvect<K> & a,node * n){ 
    if(!n){ return ; } 
    get_keys(a,n->l);
    a.push(n->k); 
    get_keys(a,n->r); 
  }


  // Acceder al elemento con llave key
  V & operator[](K key) {return this->find(key,this->root)->v;} 

};





template <typename T> 
struct ufds{ 

  // Clase nodo, contiene un valor, apuntador al padre, el rango del nodo y 
  // el número de nodos hijos.
  struct node{ 
    T val; 
    node * parent; 
    int cnt; 
    int rnk; 
    node(T val){ 
      this->val = val; 
      this->parent = this; 
      this->cnt = 1; 
      this->rnk = 0; 
    } 
  }; 

  // Utilizamos un mapa para almacenar los nodos de forma eficiente
  nmap<T,node*>kmap; 

  // Insertar un nuevo nodo
  void insert(T val){ 
    node * n = new node(val); 
    this->kmap.insert(val,n); 
  } 
 
  // Regresa true si esta en el conjunto y false si no esta
  bool inset(T val){ return(!!this->kmap.find(val,this->kmap.root)); }

  // Regresa el nodo padre del conjunto (el representante), aplica la 
  // compresión de camino, donde se reduce la altura del árbol a 1.
  node * find(T val){ 
    auto node = this->kmap[val];
    if(node->parent != node){ node->parent = find(node->parent->val);} 
    return (node->parent); 
  } 

  int merge(T v1, T v2){ 
    auto n1 = this->find(v1); 
    auto n2 = this->find(v2);
    
    // Los dos valores pertenecen al mismo conjunto si tienen el mismo 
    // representante.
    if(n1 == n2){ return n1->cnt;} 

    // Si los dos conjuntos tienen el mismo rango se combinan y se aumenta el 
    // rango , si no, se elije el que tenga menor rango como padre.
    node * p = nullptr; 
    node * c = nullptr; 
    if(n1->rnk == n2->rnk){ 
      p = n1; 
      c = n2; 
      n1->rnk += 1;
    }else if (n1->rnk > n2->rnk){ 
      p = n1; 
      c = n2;
    }else{ 
      p = n2; 
      c = n1; 
    } 

    // El hijo (menor rango) tiene com padre al de mayor rango (para 
    // no aumentar la altura). También se actualiza el número de nodos en el 
    // conjunto. 
    c->parent = p; 
    p->cnt += c->cnt; 

    return p->cnt; 
  } 

} ;


// Implementación del árbol de segmentos 
struct nstree{ 
  
  // Intervalo inclusivo l..r
  struct range{ 
    int l = 0; 
    int r = 0; 
    range(){};
    range(int _l, int _r): l(_l),r(_r){}
    bool contains(range o){return ((this->l <= o.l) && (o.r <= this->r));} 
    bool intersects(range o){return ((this->l <= o.r) && (o.l <= this->r));}
  };

  // Estructura que contienen el valor y el rango (representa un nodo del arbol)
  struct node{ 
    int val = 0; 
    range rng;
    node(){}
    node(int _val,range _rng):val(_val),rng(_rng){}
  };

  // arreglo para procesar los datos debe ser 2**(log(n)-1) nodos
  node st[500000]; 
  int size = 0; 

  // Crear un árbol de segmentos de forma recursiva iniciando del nodo raíz
  int construct_st(int i, range rng, int * a){

    // Caso base si es un nodo hoja el segmento solo tiene un elemento
    if(rng.l == rng.r){ 
      int v = (a[rng.l] == 0) ? 0 : ((a[rng.l] < 0) ? -1 : 1); 
      this->st[i] = node(v,rng);
      return this->st[i].val; 
    }

    // Caso recursivo se divide en dos el segmento 
    int m = (rng.l + rng.r)/2; 
    auto vl = construct_st(this->ln(i),range(rng.l,m),a); 
    auto vr = construct_st(this->rn(i),range(m+1,rng.r),a);

    // Combinación de los valores 
    this->st[i] = node(vl*vr,rng); 
    return this->st[i].val; 
  }

  // Ver el valor en un segmento a..b
  int operator[](range rng){return at(rng,0);}
  int at(range rng, int index){ 
    auto node = this->st[index]; 
    if(rng.contains(node.rng)){ 
      return this->st[index].val; 
    }else if(rng.intersects(node.rng)){ 
      auto vl = this->at(rng,this->ln(index)); 
      auto vr = this->at(rng,this->rn(index));
      return (vl*vr);
    }else{return 1;} 
  }

  // Actualizar un valor en el arreglo, propagando la actualización hacia
  // arriba de los nodos afectados
  int update(int index, int val, int node_index ){ 
    auto node = this->st[node_index]; 
    auto rng = node.rng; 

    // caso base
    if(rng.l == rng.r){ 
      int v = (val == 0) ? 0 : ((val < 0) ? -1 : 1);
      this->st[node_index] = nstree::node(v,rng); 
      return v;
    } 

    // caso recursivo 
    int m = (rng.r + rng.l)/2; 
    auto vl = 1;
    auto vr = 1;
    // El valor esta a la derecha o a la izquierda 
    if(index <= m){ 
      vl = this->update(index,val,this->ln(node_index));
      vr = this->st[this->rn(node_index)].val;
    }else{ 
      vl = this->st[this->ln(node_index)].val;
      vr = this->update(index,val,this->rn(node_index));
    } 
    // actualizar el nodo actual
    this->st[node_index] = nstree::node(vl*vr,rng);
    return this->st[node_index].val; 
  } 

  // Operaciones para mover 
  int ln(int i){return (i<<1)+1; } 
  int rn(int i){return (i<<1)+2; } 
}; 
