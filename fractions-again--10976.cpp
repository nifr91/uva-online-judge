// Ricardo Nieto Fuentes
// nifr91@gmail.com 
//
// ============================================================================
//
// Fractions Again? --- 10976
// 3.000 seconds 
//
// Its easy to see that for every fraction in the form 1/k (k > 0), we 
// can always find two positive integers x and y , x >= y , such that: 
//
//  1      1     1
// ---  = --- + ---
//  k      x     y
//
//  Now our question is: can you write a program that counts how many such 
//  pairs of x and y there are for any given k?
//
//  Input 
//
//  Input contains no more than 100 lines, each giving a value of k (0 < k <=
//  10_000). 
//
//  Output 
//
//  For each k, output the number of corresponding (x,y) pairs, followed by a 
//  sorted list of values of x and y, as shown in the sample output. 
//
//  ==========================================================================
//
//  Sample Input 
//
//  |2
//  |12
//
//  Sample Output 
//  
//  |2
//  |1/2 = 1/6 + 1/3
//  |1/2 = 1/4 + 1/4
//  |8
//  |1/12 = 1/156 + 1/13
//  |1/12 = 1/84 + 1/14
//  |1/12 = 1/60 + 1/15
//  |1/12 = 1/48 + 1/16
//  |1/12 = 1/36 + 1/18
//  |1/12 = 1/30 + 1/20
//  |1/12 = 1/28 + 1/21
//  |1/12 = 1/24 + 1/24
//
//  ===========================================================================

// Propuesta solución 
//
// Notemos que la igualdad se puede expresar como :
//     k*y
// x = ----
//     y-k
// de manera que podemos recorrer los 10_000 números y ver si se
// cumple la igualdad
//
// notemos que para que se cumpla 
//
// 1/k  - 1/y = 1/x 
//
// entonces y > k


#include<iostream> 
using namespace std; 
int main(int argc, char ** argv){ 

  // Leer la entrada 
  int k ; 
  long long xy[10000][2]; 
  while (cin >> k){ 
    int cnt = 0;

    // Recorrer todas las posibilidades 
    for(int i=0; i < 10000; i+=1){  
      long long * x = xy[cnt]; 
      long long * y = xy[cnt]+1;
      *y = k+1+i;
      *x = (k*(*y)/(*y-k));

      // Comprobar que se cumple la igualdad
      if(k == ((*x)*(*y) /(*x+*y))){ 
        cnt +=1; 
      } 
      if (*x < *y){break;}
    }

    // Imprimir el resultado 
    cout << cnt << endl;
    for(int i=0; i < cnt; i+=1){ 
      cout 
        <<  "1/" << k 
        <<  " = " 
        <<  "1/" << xy[i][0]
        <<  " + "
        <<  "1/" << xy[i][1]
        << endl; 
    } 
  } 
  return 0; 
} 
