// Ricardo Nieto Fuentes
// nifr91@gmail.com 
//
// ============================================================================
//
// Longest Match --- 10100
// 3.000 seconds
//
// A newly opened detective agency is struggling with their limited intelligence
// to find out a secret information passing technique among its detectives. Since
// they are new in this profession, they know well that their messages will
// easily be trapped and hence modified by other groups. They want to guess the
// intentions of other groups by checking the changed sections of messages. 
// First they have to get the length of the longest match. You are going to 
// help them. 
//
// Input: 
//
// The input file may contain multiple test cases. Each case will contain two 
// successive lines of string. Blank lines and non-letter printable punctuation
// characters may appear. Each line of string will be no longer than 1_000 
// characters. Length of each word will be less than 20 characters.
//
// Output: 
//
// For each case of input, you will have to output a line starting with 
// the case number right justified in a field with of two, followed by the
// longest match as shown in the sample output. In case of at least one 
// blank line for each input, output "Blank!". Consider the non-letter 
// punctuation characters as white-spaces.
//
// ============================================================================
//
// Sample Input: 
//
// |This is a test.
// |test
// |Hello!
// |
// |The document provides late-breaking information
// |late breaking.
// 
// Sample Output: 
//
// | 1. Length of longest match: 1
// | 2. Blank!
// | 3. Length of longest match: 2
//
// ============================================================================

// Solution -------------------------------------------------------------------
//
// Emplear el algoritmo de Longest Common Substring, empleando las 
// palabras en lugar de los caracteres. 
//
// ----------------------------------------------------------------------------


#include<iostream>
#include <ctype.h>
#include<iomanip>
using std::cin; 
using std::cout; 
using std::endl; 
using std::endl; 
using std::string;
using std::setw;
using std::right;

// Obtener una palabra o número 
string get_word(string & line, int & pos){ 
  string word = "";
  while(isalnum(line[pos])) {
    word += line[pos];
    pos += 1;
  }
  pos -= 1;
  return word;
}
// ======================================================================

int main(int num_args, char ** args){ 

  // Leer los datos 
  string line = "";
  int case_cnt = 1; 

  // Leer la primer linea 
  while(getline(cin,line)){ 
    string s1[1001]; 
    int s1len = 0; 
    string s2[1001]; 
    int s2len = 0; 

    for(int i=0; i < (int)line.size(); i+=1){ 
      if (isalnum(line[i])){ 
        string word = get_word(line,i); 
        s1[s1len] = word; 
        s1len += 1;
      } 
    }

    // Leer las segunda linea 
    getline(cin,line);
    for(int i=0; i < (int)line.size(); i+=1){ 
      if (isalnum(line[i])){ 
        string word = get_word(line,i); 
        s2[s2len] = word; 
        s2len += 1;
      } 
    }

    // DP para encontrar la cadena común más larga
    int dp[s1len+1][s2len+1] = {}; 
    for(int i=1; i <= s1len; i+=1){ 
      for(int j=1; j <= s2len; j+=1){ 
        if(s1[i-1] == s2[j-1]){ dp[i][j] = dp[i-1][j-1]+1; } 
        else{ 
          dp[i][j] = 
            (dp[i][j-1] > dp[i-1][j]) ? dp[i][j-1] : dp[i-1][j]; 
        } 
      } 
    }
   
   
    // Desplegar la información
    cout << setw(2) << right << case_cnt << ". "; 
    if(!s1len || !s2len ){ cout << "Blank!" << endl; }
    else{ cout << "Length of longest match: " << dp[s1len][s2len] << endl;} 
    case_cnt += 1;
  } 

  return 0; 
} 
