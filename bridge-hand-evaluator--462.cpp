// Ricardo Nieto Fuentes
// nifr91@gmail.com 
//
// ============================================================================
//
// In the card game "Bridge", players must assess the strength of their hands 
// prior to bidding against one another. Most players use a point-count scheme
// which employs the following rules: 
//
// 1. Each ace counts 4 points. Each king counts 3 points. Each queen counts 2
//    points. Each jack counts one point. 
// 2. Substract a point for any king of a suit in which the hand holds no 
//    other cards. 
// 3. Substract a point for any queen in a suit in which the hand holds only 
//    zero or one other cards.
// 4. Substract a point for any jack in a suit in which the hand holds only 
//    zero, one, or  two other cards. 
// 5. Add a point for each suit in which the hand contains exactly two cards.
// 6. Add two points for each suit in which the hand contains exactly one card.
// 7. Add two points for each suit in which the hand contains no cards. 
//
// A suit is _stoppped_ if it contains an ace, or if it contains a king and at
// least one other card, or if it contains a queen and at least two other cards.
// 
// During the opening assessment, the three most common possibilities are: 
//
// * If the hand evaluates to fewer than 14 points, then the player must pass. 
// * One may open bidding in a suit if the hand evaluates to 14 or more points. 
//   Bidding is always opened in one one of the suits with the most cards.
// * One may open bidding in "no trump" if the hand evaluates to 16 or more 
//   points _ignoring rules 5,6 and 7_ and if all four suits are stopped. A no 
//   trump bid is always preferred over a suit bid when both are possible. 
//
// Input 
//
// Input to your program consists of aseries of lines, each line containing 13 
// cards. Each card consists of two characters. The first represents the rank 
// of the card: 'A','2','3','4','5','6','7','8','9','T','J','Q','K'. The second
// represents the suit of the card: 'S','H','D','C', standing for "spades", 
// "hearts", "diamonds" and "clubs", respectively. 
//
// Output
//
// For each line of the input, print one line containing the recommended bid,
// either 'PASS', the first 'BID suit', where suit is 'S','H','D' or 'C'
// (sorted in this natural way if two or more are possible), or 'BID NO-TRUMP'. 
//
// Note: For the first example below, the evaluation starts with 6 points for
// the two kings, 4 for the ace, 6 for the three queens, and one for the jack.
// To this tally of 17 points we add 1 point for having only two cards in
// spades, and substract 1 for having a queen in spades with only one other
// card in spades.  The resulting 17 points is enough to justify opening in a
// suit. 
//
// Te evaluation for _no-trump_ is 16 points, since we cannot count the one
// point for having only two spades. We cannot open in no-trump, however,
// because the hearts suit is not stopped.  Hence we must open bidding in a
// suit. The two longest suits are clubs and diamonds, with four cards each, so
// the possible outputs are 'BID C' or 'BID D'. 
//
// ============================================================================
//
// Sample Input 
// 
// |KS  QS  TH  8H  4H  AC  QC  TC  5C  KD  QD  JD  8D
// |AC  3C  4C  AS  7S  4S  AD  TD  7D  5D  AH  7H  5H
//
// Sample Output 
//
// |BID D
// |BID NO-TRUMP
//


#include<iostream> 
using namespace std; 

// Representar como mascara de bits las cartas en cada suit
//  K  Q  J  T 9 8 7 6 5 4 3 2 A -  carta 
// 13 12 11 10 9 8 7 6 5 4 3 2 1 0  bits
// 
// para ello definimos los siguientes macros para la mascara de bits

// Poner el bit especificado de num a 1
#define set_bit(num,bit)    num = (num | (1 << bit))
// Poner el bit especificado de num a 0
#define clear_bit(num,bit)  num = (num & (~(1 << x)))
// Invertir el bit especificado de num 
#define toggle_bit(num,bit) num = (num ^ (1<<bit))
// Verificar si el bit especificado es 1
#define is_bit_on(num,bit)  ((num & (1 << bit))!=0)

// Contar el número de bits a 1 en O(1)
int num_to_bits[16] = {0,1,1,2,1,2,2,3,1,2,2,3,2,3,3,4}; 
int count_set_bits_rec(int num){ 
  if(0 == num){ return num_to_bits[0]; } 
  int nibble = num & 0xf; 
  return(num_to_bits[nibble] + count_set_bits_rec(num>>4)); 
} 

// Verificar si el bit especificado por 'card' es 1
#define card_in_hand(num,card)  ((num & (1<<card))!=0)

// Constantes para las cartas
int K = 13; 
int Q = 12;
int J = 11;
int T = 10; 
int A = 1;
int S = 0; 
int H = 1; 
int D = 2; 
int C = 3; 

// Main =======================================================================

int main(int argc, char ** argv){

  // Número de cartas
  int NCARDS = 13; 
  string cards[NCARDS];
  // Por cada caso de prueba se leen las cartas
  while (cin >> cards[0]){
    for(int i=1; i < NCARDS;i+=1){cin >> cards[i];}

    // Por cada carta se almacenan como mascara de bits en el suit
    int suits[4] = {0,0,0,0};
    for(int i=0; i < NCARDS; i+=1){ 
      int suit = 0; 
      int card = 0; 
      // Ver que carta es
      if     (cards[i][0] == 'K'){card = K;}
      else if(cards[i][0] == 'Q'){card = Q;} 
      else if(cards[i][0] == 'J'){card = J;} 
      else if(cards[i][0] == 'T'){card = T;} 
      else if(cards[i][0] == '9'){card = 9;} 
      else if(cards[i][0] == '8'){card = 8;} 
      else if(cards[i][0] == '7'){card = 7;} 
      else if(cards[i][0] == '6'){card = 6;} 
      else if(cards[i][0] == '5'){card = 5;} 
      else if(cards[i][0] == '4'){card = 4;} 
      else if(cards[i][0] == '3'){card = 3;} 
      else if(cards[i][0] == '2'){card = 2;} 
      else if(cards[i][0] == 'A'){card = A;} 
     
      // Ver el suit
      if (cards[i][1] == 'S'){suit = S;} 
      if (cards[i][1] == 'H'){suit = H;} 
      if (cards[i][1] == 'D'){suit = D;} 
      if (cards[i][1] == 'C'){suit = C;} 

      // Asignar el bit de la carta en su suit
      set_bit(suits[suit],card); 
    } 

    // Obtener los puntos 
    int akgj_points = 0; 
    int extra_points = 0; 
    bool no_trump = true; 
    // Por cada palo 
    for(int i=0; i < 4; i+=1){
      // Contar el número de cartas en el palo
      int cards_in_suit = (count_set_bits_rec(suits[i]));
 
      // Regla 1
      if(card_in_hand(suits[i],A)){akgj_points += 4;}
      if(card_in_hand(suits[i],K)){akgj_points += 3;}
      if(card_in_hand(suits[i],Q)){akgj_points += 2;}
      if(card_in_hand(suits[i],J)){akgj_points += 1;} 
      // Regla 2
      if(card_in_hand(suits[i],K) && (cards_in_suit == 1)){akgj_points -= 1;}
      // Regla 3
      if(card_in_hand(suits[i],Q) && (cards_in_suit <= 2)){akgj_points -= 1;} 
      // Regla 4
      if(card_in_hand(suits[i],J) && (cards_in_suit <= 3)){akgj_points -= 1;} 
      // Regla 5
      if(cards_in_suit == 2){extra_points += 1;}
      // Regla 6
      if(cards_in_suit == 1){extra_points += 2;}
      // Regla 7
      if(cards_in_suit == 0){extra_points += 2;}

      // Para poder hacer una apuesta no-trump todos los palos deben estar 
      // congelados
      no_trump= no_trump && 
        ((card_in_hand(suits[i],A)) || 
        (card_in_hand(suits[i],K) && (cards_in_suit >= 2)) ||
        (card_in_hand(suits[i],Q) && (cards_in_suit >= 3)));
    }
    // Para poder hacer una apuesta ademas de que todos los palos estén 
    // congelados debemos contar con 16 o más puntos sin contar las reglas 
    // 4 6 y 7
    no_trump = no_trump && (akgj_points >= 16); 

    // Número total de puntos
    int points = (akgj_points + extra_points);
    
    // Si tenemos menos de 14 puntos no podemos hacer apuesta
    if(points < 14){cout << "PASS" << endl;} 
    // Si podemos hacer una apuesta no-trump la realizamos
    else if (no_trump){ cout << "BID NO-TRUMP"  << endl; }
    // Obtenemos el máximo valor de cartas en un palo, y apostamos en 
    // el primer palo en orden SHDC que tenga ese valor. 
    else { 

      // Obtener valor maximo 
      int max_cards_in_suit = 0; 
      for(int i=0; i < 4; i+=1){ 
        int cards_in_suit = (count_set_bits_rec(suits[i])); 
        if(cards_in_suit >= max_cards_in_suit){
          max_cards_in_suit = cards_in_suit; 
        }
      } 
      // Encontrar primer palo con el valor maximo 
      int suit = 0; 
      for(int i=0; i < 4; i+=1){ 
        int cards_in_suit = (count_set_bits_rec(suits[i])); 
        if (cards_in_suit == max_cards_in_suit){ 
          suit = i;
          break;
        } 
      } 

      // Imprimir la apuesta al palo
      cout << "BID" << " "; 
      switch(suit){ 
        case 0: cout << "S"; break; 
        case 1: cout << "H"; break; 
        case 2: cout << "D"; break; 
        case 3: cout << "C"; break; 
      } 
      cout << endl; 
    } 

  }  
  return(0); 
} 
